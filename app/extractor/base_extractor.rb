# frozen_string_literal: true

module Extractor
  class BaseExtractor
    def initialize(logger)
      @logger = logger
    end

    def call
      raise NotImplementedError
    end
  end
end
