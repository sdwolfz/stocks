# frozen_string_literal: true

module Extractor
  class PriceExtractor < BaseExtractor
    def call(exchange, box)
      case exchange
      when 'BVB'
        call_bvb(box)
      when 'LSE'
        call_lse(box)
      when 'NYSE'
        call_nyse(box)
      else
        raise "Unknown exchange: #{exchange}"
      end
    end

    private

    BVB_SELECTORS = {
      price:  '.horizontal-box strong',
      shares: '#ctl00_body_ctl02_IssueInfoControl_dvInfo > tr:nth-child(1) > td:nth-child(2)'
    }.freeze

    def call_bvb(box)
      data   = box.input
      result = []
      client = HTTPClient.new

      data.each do |element|
        value = element['value']
        id    = value['symbol']

        if box.stash.key?(id)
          result << box.stash[id]
        else
          url = value['url']

          @logger.info("Extracting: #{url}")
          response = client.get(url)

          if response.code == '404'
            @logger.debug("Price for #{id} not found!")

            price  = '0'
            shares = '0'
          else
            raise "Request error: #{response.code}" unless response.code == '200'

            page   = Nokogiri::HTML.parse(response.body)
            price  = page.css(BVB_SELECTORS[:price]).text.strip
            shares = page.css(BVB_SELECTORS[:shares]).text.strip
          end

          payload = {
            'id'    => id,
            'value' => {
              'symbol' => id,
              'price'  => price,
              'shares' => shares
            }
          }

          box.stash[id] = payload
          result << payload

          sleep 0.5
        end
      end

      result
    end

    LSE_SELECTORS = {
      price:  'div.sp-main-info__primary-data:nth-child(1) > span:nth-child(2)',
      shares: 'div.sp-info-columns__column:nth-child(1) table:nth-child(1) tr:nth-child(3) td:nth-child(2)'
    }.freeze

    def call_lse(box)
      data   = box.input
      result = []
      client = HTTPClient.new

      data.each do |element|
        value = element['value']
        id    = value['symbol']

        if box.stash.key?(id)
          result << box.stash[id]
        else
          url = "https://www.lse.co.uk/SharePrice.asp?shareprice=#{id}"

          @logger.info("Extracting: #{url}")
          response = client.get(url)
          raise "Request error: #{response.code}" unless response.code == '200'

          page   = Nokogiri::HTML.parse(response.body)
          price  = page.css(LSE_SELECTORS[:price]).text.strip
          shares = page.css(LSE_SELECTORS[:shares]).text.strip

          payload = {
            'id'    => id,
            'value' => {
              'symbol' => id,
              'price'  => price,
              'shares' => shares
            }
          }

          box.stash[id] = payload
          result << payload

          sleep 0.5
        end
      end

      result
    end

    NYSE_SELECTORS = {
      price:  '.price-section__current-value',
      shares: 'div.snapshot__data-item:nth-child(4)'
    }.freeze

    def call_nyse(box)
      data   = box.input
      result = []
      client = HTTPClient.new

      data.each do |element|
        value = element['value']
        id    = value['symbol']
        mic   = value['mic']

        if box.stash.key?(id)
          result << box.stash[id]
        else
          symbol = id.downcase.gsub('.', '-')
          url    = "https://markets.businessinsider.com/stocks/#{symbol}-stock"
          @logger.info("Extracting: #{url}")

          response = client.get(url)
          if response.code == '404' || response.code == '301'
            @logger.debug("Price for #{id} not found!")

            price  = '0'
            shares = '0B'
          else
            raise "Request error: #{response.code}" unless response.code == '200'

            page   = Nokogiri::HTML.parse(response.body)
            price  = page.css(NYSE_SELECTORS[:price]).text.strip
            shares = page.css(NYSE_SELECTORS[:shares]).text.gsub('Number of Shares', '').strip
          end

          payload = {
            'id'    => id,
            'value' => {
              'symbol' => id,
              'price'  => price,
              'shares' => shares
            }
          }
          box.stash[id] = payload
          result << payload

          sleep 0.5
        end
      end

      result
    end
  end
end
