# frozen_string_literal: true

module Generator
  class Data
    GEOGRAPHIES = {
      'RO' => 'Romania',
      'UK' => 'United Kingdom',
      'US' => 'United States'
    }.freeze

    def call
      generate_data_files('RO')
      generate_data_files('UK')
      generate_data_files('US')
    end

    private

    def generate_data_files(id)
      artifacts = [
        ['dividend', "#{id}.json"],
        ['metadata', "#{id}.json"],
        ['symbol',   "#{id}.json"]
      ]
      registry = DataJoiner.new.call(artifacts)

      registry.each do |symbol, value|
        geography = value['geography']
        next unless geography

        generate_dividend_files(symbol, value)
        generate_metadata_files(symbol, value)
      end
    end

    def generate_dividend_files(symbol, value)
      geography = value['geography']

      dividends = value['dividends']
      return unless dividends

      dividends.each do |dividend|
        year  = dividend['year']
        value = dividend['value']

        path = File.join(Dir.pwd, 'data', 'registry', geography, symbol, year.to_s)
        FileUtils.mkdir_p(path)

        file = File.join(path, 'dividend.yml')
        text = <<~YML
          ---

          symbol: #{symbol}
          dividend: #{value}
        YML
        File.write(file, text)
      end
    end

    def generate_metadata_files(symbol, value)
      geography = value['geography']
      name      = value['name']
      url       = value['url']
      mic       = value['mic']

      path = File.join(Dir.pwd, 'data', 'registry', geography, symbol)
      FileUtils.mkdir_p(path)

      file = File.join(path, 'metadata.yml')
      text = <<~YML
        ---

        symbol: #{symbol}
        geography: #{GEOGRAPHIES[geography]}
        name: #{name}
        url: #{url}
        mic: #{mic}
      YML
      File.write(file, text)
    end
  end
end
