# frozen_string_literal: true

module Job
  class CalendarJob
    def initialize(logger)
      @logger = logger
      @driver = WebDriver.new
    end

    def call(geo)
      run_bvb_chain  if geo.include?('RO') || geo.empty?
      run_lse_chain  if geo.include?('UK') || geo.empty?
      run_nyse_chain if geo.include?('US') || geo.empty?
    end

    private

    def run_bvb_chain
      chain = CacheBox::Chain.new('calendar/BVB')

      chain.add(:seeder) do
        schema = SchemaValidator.new(
          @logger,
          output: 'schemas/transformer/symbol-transformer-schema.json'
        )

        path   = 'artifacts/symbol/RO.json'
        result = Extractor::JSONExtractor.new(@logger).call(path)
        schema.validate_output!(result)

        result
      end
      chain.add(:extractor) do |box|
        input  = 'schemas/transformer/symbol-transformer-schema.json'
        output = 'schemas/extractor/text-list-extractor-schema.json'
        schema = SchemaValidator.new(@logger, input: input, output: output)

        schema.validate_input!(box.input)
        result = Extractor::CalendarExtractor.new(@logger, @driver).call('BVB', box)
        schema.validate_output!(result)

        result
      end
      chain.add(:transformer) do |box|
        input  = 'schemas/extractor/text-list-extractor-schema.json'
        output = 'schemas/transformer/calendar-transformer-schema.json'
        schema = SchemaValidator.new(@logger, input: input, output: output)

        schema.validate_input!(box.input)
        result = Transformer::CalendarTransformer.new(@logger).call('BVB', box)
        schema.validate_output!(result)

        result
      end
      chain.add(:loader) do |box|
        input  = 'schemas/transformer/calendar-transformer-schema.json'
        schema = SchemaValidator.new(@logger, input: input)

        schema.validate_input!(box.input)
        path = 'artifacts/calendar/RO.json'
        Loader::JSONLoader.new(@logger).call(path, box.input)

        nil
      end

      chain.run!
    end

    def run_lse_chain
      chain = CacheBox::Chain.new('calendar/LSE')

      chain.add(:seeder) do
        schema = SchemaValidator.new(
          @logger,
          output: 'schemas/transformer/symbol-transformer-schema.json'
        )

        path   = 'artifacts/symbol/UK.json'
        result = Extractor::YAMLExtractor.new(@logger).call(path)
        schema.validate_output!(result)

        result
      end
      chain.add(:extractor) do |box|
        input  = 'schemas/transformer/symbol-transformer-schema.json'
        output = 'schemas/extractor/text-extractor-schema.json'
        schema = SchemaValidator.new(@logger, input: input, output: output)

        schema.validate_input!(box.input)
        result = Extractor::CalendarExtractor.new(@logger, @driver).call('LSE', box)
        schema.validate_output!(result)

        result
      end
      chain.add(:transformer) do |box|
        input  = 'schemas/extractor/text-extractor-schema.json'
        output = 'schemas/transformer/calendar-transformer-schema.json'
        schema = SchemaValidator.new(@logger, input: input, output: output)

        schema.validate_input!(box.input)
        result = Transformer::CalendarTransformer.new(@logger).call('LSE', box)
        schema.validate_output!(result)

        result
      end
      chain.add(:loader) do |box|
        input  = 'schemas/transformer/calendar-transformer-schema.json'
        schema = SchemaValidator.new(@logger, input: input)

        schema.validate_input!(box.input)
        path = 'artifacts/calendar/UK.json'
        Loader::JSONLoader.new(@logger).call(path, box.input)

        nil
      end

      chain.run!
    end

    def run_nyse_chain; end
  end
end
