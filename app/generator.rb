# frozen_string_literal: true

module Generator
end

require_relative './generator/calendar'
require_relative './generator/data'
require_relative './generator/page'

require_relative './generator/renderer'
