# frozen_string_literal: true

require 'fileutils'
require 'json'

module Loader
  class JSONLoader < BaseLoader
    def call(path, data)
      path = File.join(Dir.pwd, path)
      dir  = File.dirname(path)
      FileUtils.mkdir_p(dir)

      @logger.info("Loading: #{path}")

      output = ::JSON.dump(data)
      File.write(path, output)

      self
    end
  end
end
