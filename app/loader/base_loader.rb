# frozen_string_literal: true

require 'logger'

module Loader
  class BaseLoader
    def initialize(logger)
      @logger = logger || Logger.new($stdout)
    end

    def call
      raise NotImplementedError
    end
  end
end
