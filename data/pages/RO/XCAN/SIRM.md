---
title: "SINTEROM SA Cluj (SIRM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>SINTEROM SA Cluj</td></tr>
    <tr><td>Symbol</td><td>SIRM</td></tr>
    <tr><td>Web</td><td><a href="https://www.sinterom.ro">www.sinterom.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
