---
title: "ROMNAV SA BRAILA (BRNA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>ROMNAV SA BRAILA</td></tr>
    <tr><td>Symbol</td><td>BRNA</td></tr>
    <tr><td>Web</td><td><a href="https://www.romnav.ro">www.romnav.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.6186 |
| 2019 | 1.2949 |
| 2018 | 0.9618 |
| 2017 | 0.8671 |
| 2015 | 1.78 |
| 2014 | 0.9646 |
| 2012 | 0.3708 |
| 2009 | 0.929 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
