---
title: "GRUPUL EDITORILOR SI DIFUZORILOR DE PRESA S.A. (GDP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>GRUPUL EDITORILOR SI DIFUZORILOR DE PRESA S.A.</td></tr>
    <tr><td>Symbol</td><td>GDP</td></tr>
    <tr><td>Web</td><td><a href="https://www.accespress.ro">www.accespress.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
