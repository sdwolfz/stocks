---
title: "MERCUR SA CRAIOVA (MRDO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>MERCUR SA CRAIOVA</td></tr>
    <tr><td>Symbol</td><td>MRDO</td></tr>
    <tr><td>Web</td><td><a href="https://www.mercurcraiova.ro">www.mercurcraiova.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.124 |
| 2018 | 0.11 |
| 2017 | 0.1377 |
| 2016 | 0.04 |
| 2013 | 0.6 |
| 2012 | 2.3 |
| 2010 | 1.76 |
| 2009 | 3.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
