---
title: "CONSTRUCTII BIHOR SA ORADEA (COBJ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>CONSTRUCTII BIHOR SA ORADEA</td></tr>
    <tr><td>Symbol</td><td>COBJ</td></tr>
    <tr><td>Web</td><td><a href="https://www.constructiibihor.ro">www.constructiibihor.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2003 | 0.0285 |
| 2002 | 0.0465 |
| 2001 | 0.0408 |
| 2000 | 0.0285 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
