---
title: "SEMROM OLTENIA SA CRAIOVA (SEOL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>SEMROM OLTENIA SA CRAIOVA</td></tr>
    <tr><td>Symbol</td><td>SEOL</td></tr>
    <tr><td>Web</td><td><a href="https://www.semromoltenia.ro">www.semromoltenia.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.032 |
| 2003 | 0.0777 |
| 2002 | 0.0894 |
| 2001 | 0.0912 |
| 2000 | 0.0655 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
