---
title: "BAD SA SUCEAVA (BADE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>BAD SA SUCEAVA</td></tr>
    <tr><td>Symbol</td><td>BADE</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.0217 |
| 2015 | 0.017 |
| 2011 | 0.014514 |
| 2010 | 0.0384 |
| 2009 | 0.0508 |
| 2008 | 0.4427 |
| 2007 | 0.2317 |
| 2006 | 0.21 |
| 2005 | 0.1219 |
| 2004 | 0.1568 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
