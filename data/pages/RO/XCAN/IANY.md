---
title: "24 IANUARIE SA PLOIESTI (IANY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>24 IANUARIE SA PLOIESTI</td></tr>
    <tr><td>Symbol</td><td>IANY</td></tr>
    <tr><td>Web</td><td><a href="https://www.24january.ro">www.24january.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 1.0026 |
| 2017 | 0.927207 |
| 2016 | 1.0 |
| 2015 | 0.6275 |
| 2014 | 0.44338 |
| 2004 | 0.3631 |
| 2002 | 1.452 |
| 2001 | 1.676 |
| 2000 | 0.911 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
