---
title: "ASCENDIA SA (ASC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>ASCENDIA SA</td></tr>
    <tr><td>Symbol</td><td>ASC</td></tr>
    <tr><td>Web</td><td><a href="https://www.ascendia.ro">www.ascendia.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
