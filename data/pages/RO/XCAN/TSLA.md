---
title: "TRANSILVANIA LEASING SI CREDIT IFN SA BRASOV (TSLA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>TRANSILVANIA LEASING SI CREDIT IFN SA BRASOV</td></tr>
    <tr><td>Symbol</td><td>TSLA</td></tr>
    <tr><td>Web</td><td><a href="https://www.transilvanialeasingsicredit.ro">www.transilvanialeasingsicredit.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.001 |
| 2017 | 0.001 |
| 2016 | 0.001 |
| 2015 | 0.0012 |
| 2014 | 0.0012 |
| 2013 | 0.0018 |
| 2010 | 0.0023 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
