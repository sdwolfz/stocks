---
title: "COMCEREAL SA TULCEA (CTUL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>COMCEREAL SA TULCEA</td></tr>
    <tr><td>Symbol</td><td>CTUL</td></tr>
    <tr><td>Web</td><td><a href="https://www.comcerealtulcea.ro">www.comcerealtulcea.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.31 |
| 2018 | 0.4 |
| 2017 | 0.31 |
| 2015 | 0.43 |
| 2009 | 0.2563 |
| 2008 | 0.5614 |
| 2007 | 3.9999 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
