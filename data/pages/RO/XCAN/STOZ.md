---
title: "STICLOVAL SA VALENII DE MUNTE (STOZ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>STICLOVAL SA VALENII DE MUNTE</td></tr>
    <tr><td>Symbol</td><td>STOZ</td></tr>
    <tr><td>Web</td><td><a href="https://www.sticloval.ro">www.sticloval.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.4786 |
| 2015 | 2.1639 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
