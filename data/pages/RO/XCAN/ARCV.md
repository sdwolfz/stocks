---
title: "IMOTRUST SA ARAD (ARCV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>IMOTRUST SA ARAD</td></tr>
    <tr><td>Symbol</td><td>ARCV</td></tr>
    <tr><td>Web</td><td><a href="https://www.imotrustarad.ro">www.imotrustarad.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2007 | 0.0238 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
