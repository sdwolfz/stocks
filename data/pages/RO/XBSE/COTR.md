---
title: "TRANSILVANIA CONSTRUCTII SA (COTR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/bvb/">BVB</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/bvb/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>TRANSILVANIA CONSTRUCTII SA</td></tr>
    <tr><td>Symbol</td><td>COTR</td></tr>
    <tr><td>Web</td><td><a href="http://www.transilvaniaconstructii.ro">www.transilvaniaconstructii.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 4.25 |
| 2019 | 5.0 |
| 2018 | 1.9 |
| 2018 | 3.0 |
| 2017 | 2.8 |
| 2016 | 1.65 |
| 2016 | 1.2 |
| 2012 | 1.2 |
| 2007 | 2.5933 |
| 2005 | 2.47 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
