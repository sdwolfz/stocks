---
title: "BRD - GROUPE SOCIETE GENERALE S.A. (BRD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>BRD - GROUPE SOCIETE GENERALE S.A.</td></tr>
    <tr><td>Symbol</td><td>BRD</td></tr>
    <tr><td>Web</td><td><a href="https://www.brd.ro">www.brd.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.0749 |
| 2018 | 1.64 |
| 2017 | 1.64 |
| 2016 | 0.73 |
| 2015 | 0.32 |
| 2011 | 0.1669 |
| 2010 | 0.1796 |
| 2009 | 0.2795 |
| 2008 | 0.7283 |
| 2007 | 0.5921 |

### Reports

<style>
  .reports-table {
    width: 100%;
    display: block;
    overflow-x: auto;
    overflow-y: scroll;
    scrollbar-width: none;
    max-height: 50rem;
    border: 0.5rem solid #D8D8D8;
  }
  .reports-table table {
    border: none;
    padding: 0;
  }
  .reports-table th {
    border: none;
    padding: 0;
  }
  .reports-table td {
    border: none
    padding: 0;
  }

  table.reports {
    border-collapse: collapse;
  }

  table.reports thead th, table.reports tbody td {
    padding: 3px 5px;
  }

  table.reports td {
    border: 0px solid #FFFFFF;
  }

  table.reports thead .header-top {
    background-color: #000000;
    color: #FFFFFF;
  }

  table.reports thead .header-assets {
    background-color: #3D85C6;
    color: #FFFFFF;
  }

  table.reports thead .header-assets-non {
    background-color: #A2C4C9;
  }

  table.reports thead .header-assets-cur {
    background-color: #9FC5E8;
  }

  table.reports thead .header-assets-tot {
    background-color: #3D85C6;
    color: #FFFFFF;
  }

  table.reports thead .header-equity {
    background-color: #6AA84F;
    color: #FFFFFF;
  }

  table.reports thead .header-lia-non {
    background-color: #D5A6BD;
  }

  table.reports thead .header-lia-cur {
    background-color: #EA9999;
  }

  table.reports thead .header-lia-tot {
    background-color: #A64D79;
    color: #FFFFFF;
  }

  table.reports thead .header-equity {
    background-color: #6AA84F;
    color: #FFFFFF;
  }

  table.reports thead .header-liabilities {
    background-color: #A64D79;
    color: #FFFFFF;
  }

  table.reports thead .header-revenue {
    background-color: #674EA6;
    color: #FFFFFF;
  }

  table.reports thead .header-cashflow {
    background-color: #E69138;
    color: #FFFFFF;
  }

  table.reports tbody td.number {
    text-align: right;
  }

  table.reports tbody td.year-even, table.reports tbody td.quarter-even {
    text-align: center;
    background-color: #B7B7B7;
  }

  table.reports tbody td.year-odd, table.reports tbody td.quarter-odd {
    text-align: center;
    background-color: #D9D9D9;
  }

  table.reports tbody td.assets-non-h2 {
    background-color: #D0E0E3;
  }
  table.reports tbody td.assets-cur-h2 {
    background-color: #CFE2F3;
  }
  table.reports tbody td.assets-tot-h2 {
    background-color: #CFE2F3;
  }
  table.reports tbody td.assets-non-t3 {
    background-color: #A2C4C9;
  }
  table.reports tbody td.assets-cur-t3 {
    background-color: #9FC5E8;
  }
  table.reports tbody td.assets-tot-t3 {
    background-color: #9FC5E8;
  }
  table.reports tbody td.assets-non-a4 {
    background-color: #76A5AF;
  }
  table.reports tbody td.assets-cur-a4 {
    background-color: #6FA8DC;
  }
  table.reports tbody td.assets-tot-a4 {
    background-color: #6FA8DC;
  }

  table.reports tbody td.equity-h2 {
    background-color: #D9EAD3;
  }
  table.reports tbody td.equity-t3 {
    background-color: #B6D7A8;
  }
  table.reports tbody td.equity-a4 {
    background-color: #93C47D;
  }

  table.reports tbody td.liabilities-non-h2 {
    background-color: #EAD1DC;
  }
  table.reports tbody td.liabilities-cur-h2 {
    background-color: #F4CCCC;
  }
  table.reports tbody td.liabilities-tot-h2 {
    background-color: #F4CCCC;
  }
  table.reports tbody td.liabilities-non-t3 {
    background-color: #D5A6BD;
  }
  table.reports tbody td.liabilities-cur-t3 {
    background-color: #EA9999;
  }
  table.reports tbody td.liabilities-tot-t3 {
    background-color: #EA9999;
  }
  table.reports tbody td.liabilities-non-a4 {
    background-color: #C27BA0;
  }
  table.reports tbody td.liabilities-cur-a4 {
    background-color: #E06666;
  }
  table.reports tbody td.liabilities-tot-a4 {
    background-color: #E06666;
  }

  table.reports tbody td.revenue-h2 {
    background-color: #D9D2E9;
  }
  table.reports tbody td.revenue-t3 {
    background-color: #B4A7D6;
  }
  table.reports tbody td.revenue-a4 {
    background-color: #8E7CC3;
  }

  table.reports tbody td.cashflow-h2 {
    background-color: #FCE5CD;
  }
  table.reports tbody td.cashflow-t3 {
    background-color: #F9CB9C;
  }
  table.reports tbody td.cashflow-a4 {
    background-color: #F6B26B;
  }
</style>

<div class="reports-table">
  <table class="reports">
    <thead>
      <tr>
        <th rowspan="3" class="header-top">Year</th>
        <th rowspan="3" class="header-top">Quarter</th>
        <th colspan="7" class="header-top">Balance</th>
        <th colspan="9" class="header-top">Revenue</th>
        <th colspan="8" class="header-top">Cashflow</th>
      </tr>
      <tr>
        <th colspan="3" class="header-assets">Assets</th>
        <th             class="header-equity">Equity</th>
        <th colspan="3" class="header-liabilities">Liabilities</th>
        <th rowspan="2" class="header-revenue">Sales</th>
        <th rowspan="2" class="header-revenue">Operating Expenses</th>
        <th rowspan="2" class="header-revenue">Operating Profit</th>
        <th rowspan="2" class="header-revenue">Financial Resut</th>
        <th rowspan="2" class="header-revenue">Profit Before Taxes</th>
        <th rowspan="2" class="header-revenue">Tax On Gains</th>
        <th rowspan="2" class="header-revenue">Net Profit</th>
        <th rowspan="2" class="header-revenue">Other</th>
        <th rowspan="2" class="header-revenue">Total Result</th>
        <th rowspan="2" class="header-cashflow">Operating</th>
        <th rowspan="2" class="header-cashflow">Investment</th>
        <th rowspan="2" class="header-cashflow">Financing</th>
        <th rowspan="2" class="header-cashflow">Total</th>
        <th rowspan="2" class="header-cashflow">Cash at Start</th>
        <th rowspan="2" class="header-cashflow">Cash at End</th>
        <th rowspan="2" class="header-cashflow">Capex</th>
        <th rowspan="2" class="header-cashflow">Free Cash Flow</th>
      </tr>
      <tr>
        <th class="header-assets-non">Noncurrent</th>
        <th class="header-assets-cur">Current</th>
        <th class="header-assets-tot">Total</th>
        <th class="header-equity">Total</th>
        <th class="header-lia-non">Noncurrent</th>
        <th class="header-lia-cur">Current</th>
        <th class="header-lia-tot">Total</th>
      <tr>
    </thead>
    <tbody>
      <tr class="row-a4">
        <td rowspan="4" class="year-even">2020</td>
        <td class="quarter-even">A4</td>
        <td class="number assets-non-a4">0</td>
        <td class="number assets-cur-a4">0</td>
        <td class="number assets-tot-a4">0</td>
        <td class="number equity-a4">0</td>
        <td class="number liabilities-non-a4">0</td>
        <td class="number liabilities-cur-a4">0</td>
        <td class="number liabilities-tot-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-even">T3</td>
        <td class="number assets-non-t3">0</td>
        <td class="number assets-cur-t3">0</td>
        <td class="number assets-tot-t3">0</td>
        <td class="number equity-t3">0</td>
        <td class="number liabilities-non-t3">0</td>
        <td class="number liabilities-cur-t3">0</td>
        <td class="number liabilities-tot-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-even">H2</td>
        <td class="number assets-non-h2">0</td>
        <td class="number assets-cur-h2">60,307,488,000</td>
        <td class="number assets-tot-h2">60,307,488,000</td>
        <td class="number equity-h2">8,682,887,000</td>
        <td class="number liabilities-non-h2">0</td>
        <td class="number liabilities-cur-h2">51,624,601,000</td>
        <td class="number liabilities-tot-h2">51,624,601,000</td>
        <td class="number revenue-h2">1,512,118,000</td>
        <td class="number revenue-h2">-799,608,000</td>
        <td class="number revenue-h2">712,510,000</td>
        <td class="number revenue-h2">-225,113,000</td>
        <td class="number revenue-h2">487,397,000</td>
        <td class="number revenue-h2">-72,719,000</td>
        <td class="number revenue-h2">414,678,000</td>
        <td class="number revenue-h2">84,287,000</td>
        <td class="number revenue-h2">498,965,000</td>
        <td class="number cashflow-h2">2,078,438,000</td>
        <td class="number cashflow-h2">-92,603,000</td>
        <td class="number cashflow-h2">-45,429,000</td>
        <td class="number cashflow-h2">1,940,405,000</td>
        <td class="number cashflow-h2">5,337,054,000</td>
        <td class="number cashflow-h2">7,277,459,000</td>
        <td class="number cashflow-h2">-80,563,000</td>
        <td class="number cashflow-h2">1,997,875,000</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-even">Q1</td>
        <td class="number assets-non-q1">0</td>
        <td class="number assets-cur-q1">58,935,383,000</td>
        <td class="number assets-tot-q1">58,935,383,000</td>
        <td class="number equity-q1">8,112,239,000</td>
        <td class="number liabilities-non-q1">0</td>
        <td class="number liabilities-cur-q1">50,823,144,000</td>
        <td class="number liabilities-tot-q1">50,823,144,000</td>
        <td class="number revenue-q1">766,728,000</td>
        <td class="number revenue-q1">-425,155,000</td>
        <td class="number revenue-q1">341,573,000</td>
        <td class="number revenue-q1">-59,892,000</td>
        <td class="number revenue-q1">281,681,000</td>
        <td class="number revenue-q1">-40,922,000</td>
        <td class="number revenue-q1">240,759,000</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">240,759,000</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-odd">2019</td>
        <td class="quarter-odd">A4</td>
        <td class="number assets-non-a4">0</td>
        <td class="number assets-cur-a4">57,770,504,000</td>
        <td class="number assets-tot-a4">57,770,504,000</td>
        <td class="number equity-a4">8,183,918,000</td>
        <td class="number liabilities-non-a4">0</td>
        <td class="number liabilities-cur-a4">49,586,586,000</td>
        <td class="number liabilities-tot-a4">49,586,586,000</td>
        <td class="number revenue-a4">3,269,908,000</td>
        <td class="number revenue-a4">-1,677,537,000</td>
        <td class="number revenue-a4">1,592,371,000</td>
        <td class="number revenue-a4">203,673,000</td>
        <td class="number revenue-a4">1,796,044,000</td>
        <td class="number revenue-a4">-296,889,000</td>
        <td class="number revenue-a4">1,499,155,000</td>
        <td class="number revenue-a4">176,100,000</td>
        <td class="number revenue-a4">1,675,255,000</td>
        <td class="number cashflow-a4">907,514,000</td>
        <td class="number cashflow-a4">-101,060,000</td>
        <td class="number cashflow-a4">-842,930,000</td>
        <td class="number cashflow-a4">-36,477,000</td>
        <td class="number cashflow-a4">5,373,530,000</td>
        <td class="number cashflow-a4">5,337,052,000</td>
        <td class="number cashflow-a4">-170,748,000</td>
        <td class="number cashflow-a4">736,766,000</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-odd">T3</td>
        <td class="number assets-non-t3">0</td>
        <td class="number assets-cur-t3">55,983,930,000</td>
        <td class="number assets-tot-t3">55,983,930,000</td>
        <td class="number equity-t3">8,053,662,000</td>
        <td class="number liabilities-non-t3">0</td>
        <td class="number liabilities-cur-t3">47,930,268,000</td>
        <td class="number liabilities-tot-t3">47,930,268,000</td>
        <td class="number revenue-t3">2,446,359,000</td>
        <td class="number revenue-t3">-1,186,066,000</td>
        <td class="number revenue-t3">1,260,293,000</td>
        <td class="number revenue-t3">206,724,000</td>
        <td class="number revenue-t3">1,467,017,000</td>
        <td class="number revenue-t3">-240,654,000</td>
        <td class="number revenue-t3">1,226,363,000</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">1,226,363,000</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-odd">H2</td>
        <td class="number assets-non-h2">0</td>
        <td class="number assets-cur-h2">56,743,633,000</td>
        <td class="number assets-tot-h2">56,743,633,000</td>
        <td class="number equity-h2">7,419,912,000</td>
        <td class="number liabilities-non-h2">0</td>
        <td class="number liabilities-cur-h2">49,323,721,000</td>
        <td class="number liabilities-tot-h2">49,323,721,000</td>
        <td class="number revenue-h2">1,608,583,000</td>
        <td class="number revenue-h2">-803,724,000</td>
        <td class="number revenue-h2">804,859,000</td>
        <td class="number revenue-h2">144,317,000</td>
        <td class="number revenue-h2">949,176,000</td>
        <td class="number revenue-h2">-162,273,000</td>
        <td class="number revenue-h2">786,903,000</td>
        <td class="number revenue-h2">124,343,000</td>
        <td class="number revenue-h2">911,246,000</td>
        <td class="number cashflow-h2">563,464,000</td>
        <td class="number cashflow-h2">-61,114,000</td>
        <td class="number cashflow-h2">-1,052,154,000</td>
        <td class="number cashflow-h2">-549,804,000</td>
        <td class="number cashflow-h2">5,373,530,000</td>
        <td class="number cashflow-h2">4,823,725,000</td>
        <td class="number cashflow-h2">-63,399,000</td>
        <td class="number cashflow-h2">500,065,000</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-odd">Q1</td>
        <td class="number assets-non-q1">0</td>
        <td class="number assets-cur-q1">56,034,275,000</td>
        <td class="number assets-tot-q1">56,034,275,000</td>
        <td class="number equity-q1">8,011,408,000</td>
        <td class="number liabilities-non-q1">0</td>
        <td class="number liabilities-cur-q1">48,022,867,000</td>
        <td class="number liabilities-tot-q1">48,022,867,000</td>
        <td class="number revenue-q1">784,295,000</td>
        <td class="number revenue-q1">-442,108,000</td>
        <td class="number revenue-q1">342,187,000</td>
        <td class="number revenue-q1">25,896,000</td>
        <td class="number revenue-q1">368,083,000</td>
        <td class="number revenue-q1">-67,001,000</td>
        <td class="number revenue-q1">301,082,000</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">301,082,000</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-even">2018</td>
        <td class="quarter-even">A4</td>
        <td class="number assets-non-a4">0</td>
        <td class="number assets-cur-a4">55,719,369,000</td>
        <td class="number assets-tot-a4">55,719,369,000</td>
        <td class="number equity-a4">7,657,427,000</td>
        <td class="number liabilities-non-a4">0</td>
        <td class="number liabilities-cur-a4">48,061,942,000</td>
        <td class="number liabilities-tot-a4">48,061,942,000</td>
        <td class="number revenue-a4">3,115,282,000</td>
        <td class="number revenue-a4">-1,489,665,000</td>
        <td class="number revenue-a4">1,625,617,000</td>
        <td class="number revenue-a4">230,388,000</td>
        <td class="number revenue-a4">1,856,005,000</td>
        <td class="number revenue-a4">-292,903,000</td>
        <td class="number revenue-a4">1,563,102,000</td>
        <td class="number revenue-a4">-45,624,000</td>
        <td class="number revenue-a4">1,517,478,000</td>
        <td class="number cashflow-a4">410,448,000</td>
        <td class="number cashflow-a4">-145,827,000</td>
        <td class="number cashflow-a4">-1,095,926,000</td>
        <td class="number cashflow-a4">-831,305,000</td>
        <td class="number cashflow-a4">6,204,834,000</td>
        <td class="number cashflow-a4">5,373,529,000</td>
        <td class="number cashflow-a4">-149,298,000</td>
        <td class="number cashflow-a4">261,150,000</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-even">T3</td>
        <td class="number assets-non-t3">0</td>
        <td class="number assets-cur-t3">54,508,777,000</td>
        <td class="number assets-tot-t3">54,508,777,000</td>
        <td class="number equity-t3">7,163,007,000</td>
        <td class="number liabilities-non-t3">0</td>
        <td class="number liabilities-cur-t3">47,345,770,000</td>
        <td class="number liabilities-tot-t3">47,345,770,000</td>
        <td class="number revenue-t3">2,289,437,000</td>
        <td class="number revenue-t3">-1,104,490,000</td>
        <td class="number revenue-t3">1,184,947,000</td>
        <td class="number revenue-t3">169,654,000</td>
        <td class="number revenue-t3">1,354,601,000</td>
        <td class="number revenue-t3">-212,990,000</td>
        <td class="number revenue-t3">1,141,611,000</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">1,141,611,000</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-even">H2</td>
        <td class="number assets-non-h2">0</td>
        <td class="number assets-cur-h2">53,909,265,000</td>
        <td class="number assets-tot-h2">53,909,265,000</td>
        <td class="number equity-h2">6,635,169,000</td>
        <td class="number liabilities-non-h2">0</td>
        <td class="number liabilities-cur-h2">47,274,096,000</td>
        <td class="number liabilities-tot-h2">47,274,096,000</td>
        <td class="number revenue-h2">1,479,871,000</td>
        <td class="number revenue-h2">-738,289,000</td>
        <td class="number revenue-h2">741,582,000</td>
        <td class="number revenue-h2">154,384,000</td>
        <td class="number revenue-h2">895,966,000</td>
        <td class="number revenue-h2">-139,411,000</td>
        <td class="number revenue-h2">756,555,000</td>
        <td class="number revenue-h2">-235,729,000</td>
        <td class="number revenue-h2">520,826,000</td>
        <td class="number cashflow-h2">835,051,000</td>
        <td class="number cashflow-h2">-52,900,000</td>
        <td class="number cashflow-h2">-1,213,173,000</td>
        <td class="number cashflow-h2">-431,021,000</td>
        <td class="number cashflow-h2">6,204,834,000</td>
        <td class="number cashflow-h2">5,773,813,000</td>
        <td class="number cashflow-h2">-56,151,000</td>
        <td class="number cashflow-h2">778,900,000</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-even">Q1</td>
        <td class="number assets-non-q1">0</td>
        <td class="number assets-cur-q1">55,957,154,000</td>
        <td class="number assets-tot-q1">55,957,154,000</td>
        <td class="number equity-q1">7,672,528,000</td>
        <td class="number liabilities-non-q1">0</td>
        <td class="number liabilities-cur-q1">48,284,626,000</td>
        <td class="number liabilities-tot-q1">48,284,626,000</td>
        <td class="number revenue-q1">724,191,000</td>
        <td class="number revenue-q1">-384,939,000</td>
        <td class="number revenue-q1">339,251,000</td>
        <td class="number revenue-q1">152,803,000</td>
        <td class="number revenue-q1">492,055,000</td>
        <td class="number revenue-q1">-77,773,000</td>
        <td class="number revenue-q1">414,282,000</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">414,282,000</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-odd">2017</td>
        <td class="quarter-odd">A4</td>
        <td class="number assets-non-a4">0</td>
        <td class="number assets-cur-a4">54,927,391,000</td>
        <td class="number assets-tot-a4">54,927,391,000</td>
        <td class="number equity-a4">7,368,751,000</td>
        <td class="number liabilities-non-a4">0</td>
        <td class="number liabilities-cur-a4">47,558,640,000</td>
        <td class="number liabilities-tot-a4">47,558,640,000</td>
        <td class="number revenue-a4">2,785,874,000</td>
        <td class="number revenue-a4">-1,473,218,000</td>
        <td class="number revenue-a4">1,312,656,000</td>
        <td class="number revenue-a4">359,517,000</td>
        <td class="number revenue-a4">1,672,173,000</td>
        <td class="number revenue-a4">-257,599,000</td>
        <td class="number revenue-a4">1,414,574,000</td>
        <td class="number revenue-a4">-205,745,000</td>
        <td class="number revenue-a4">1,208,830,000</td>
        <td class="number cashflow-a4">1,707,303,000</td>
        <td class="number cashflow-a4">-150,712,000</td>
        <td class="number cashflow-a4">-363,016,000</td>
        <td class="number cashflow-a4">1,193,575,000</td>
        <td class="number cashflow-a4">5,011,258,000</td>
        <td class="number cashflow-a4">6,204,833,000</td>
        <td class="number cashflow-a4">-151,220,000</td>
        <td class="number cashflow-a4">1,556,083,000</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-odd">T3</td>
        <td class="number assets-non-t3">0</td>
        <td class="number assets-cur-t3">52,955,436,000</td>
        <td class="number assets-tot-t3">52,955,436,000</td>
        <td class="number equity-t3">7,168,297,000</td>
        <td class="number liabilities-non-t3">0</td>
        <td class="number liabilities-cur-t3">45,787,139,000</td>
        <td class="number liabilities-tot-t3">45,787,139,000</td>
        <td class="number revenue-t3">2,059,849,000</td>
        <td class="number revenue-t3">-1,063,241,000</td>
        <td class="number revenue-t3">996,609,000</td>
        <td class="number revenue-t3">271,866,000</td>
        <td class="number revenue-t3">1,268,474,000</td>
        <td class="number revenue-t3">-202,970,000</td>
        <td class="number revenue-t3">1,065,505,000</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">1,065,505,000</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-odd">H2</td>
        <td class="number assets-non-h2">0</td>
        <td class="number assets-cur-h2">52,795,764,000</td>
        <td class="number assets-tot-h2">52,795,764,000</td>
        <td class="number equity-h2">6,919,972,000</td>
        <td class="number liabilities-non-h2">0</td>
        <td class="number liabilities-cur-h2">45,875,792,000</td>
        <td class="number liabilities-tot-h2">45,875,792,000</td>
        <td class="number revenue-h2">1,339,229,000</td>
        <td class="number revenue-h2">-718,076,000</td>
        <td class="number revenue-h2">621,153,000</td>
        <td class="number revenue-h2">269,863,000</td>
        <td class="number revenue-h2">891,016,000</td>
        <td class="number revenue-h2">-141,035,000</td>
        <td class="number revenue-h2">749,981,000</td>
        <td class="number revenue-h2">10,067,000</td>
        <td class="number revenue-h2">760,048,000</td>
        <td class="number cashflow-h2">1,385,047,000</td>
        <td class="number cashflow-h2">-62,277,000</td>
        <td class="number cashflow-h2">-568,088,000</td>
        <td class="number cashflow-h2">754,681,000</td>
        <td class="number cashflow-h2">3,511,237,000</td>
        <td class="number cashflow-h2">4,265,918,000</td>
        <td class="number cashflow-h2">-62,291,000</td>
        <td class="number cashflow-h2">1,322,756,000</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-odd">Q1</td>
        <td class="number assets-non-q1">0</td>
        <td class="number assets-cur-q1">51,363,255,000</td>
        <td class="number assets-tot-q1">51,363,255,000</td>
        <td class="number equity-q1">6,988,551,000</td>
        <td class="number liabilities-non-q1">0</td>
        <td class="number liabilities-cur-q1">44,374,704,000</td>
        <td class="number liabilities-tot-q1">44,374,704,000</td>
        <td class="number revenue-q1">650,124,000</td>
        <td class="number revenue-q1">-384,132,000</td>
        <td class="number revenue-q1">265,992,000</td>
        <td class="number revenue-q1">123,827,000</td>
        <td class="number revenue-q1">389,819,000</td>
        <td class="number revenue-q1">-59,961,000</td>
        <td class="number revenue-q1">329,858,000</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">329,858,000</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-even">2016</td>
        <td class="quarter-even">A4</td>
        <td class="number assets-non-a4">0</td>
        <td class="number assets-cur-a4">51,881,492,000</td>
        <td class="number assets-tot-a4">51,881,492,000</td>
        <td class="number equity-a4">6,673,833,000</td>
        <td class="number liabilities-non-a4">0</td>
        <td class="number liabilities-cur-a4">45,207,659,000</td>
        <td class="number liabilities-tot-a4">45,207,659,000</td>
        <td class="number revenue-a4">2,777,649,000</td>
        <td class="number revenue-a4">-1,387,572,000</td>
        <td class="number revenue-a4">1,390,077,000</td>
        <td class="number revenue-a4">-483,508,000</td>
        <td class="number revenue-a4">906,569,000</td>
        <td class="number revenue-a4">-143,070,000</td>
        <td class="number revenue-a4">763,499,000</td>
        <td class="number revenue-a4">-120,703,000</td>
        <td class="number revenue-a4">642,796,000</td>
        <td class="number cashflow-a4">-1,110,004,000</td>
        <td class="number cashflow-a4">-118,561,000</td>
        <td class="number cashflow-a4">-226,092,000</td>
        <td class="number cashflow-a4">-1,454,658,000</td>
        <td class="number cashflow-a4">6,465,915,000</td>
        <td class="number cashflow-a4">5,011,258,000</td>
        <td class="number cashflow-a4">-117,102,000</td>
        <td class="number cashflow-a4">-1,227,106,000</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-even">T3</td>
        <td class="number assets-non-t3">0</td>
        <td class="number assets-cur-t3">50,170,924,000</td>
        <td class="number assets-tot-t3">50,170,924,000</td>
        <td class="number equity-t3">6,707,310,000</td>
        <td class="number liabilities-non-t3">0</td>
        <td class="number liabilities-cur-t3">43,463,614,000</td>
        <td class="number liabilities-tot-t3">43,463,614,000</td>
        <td class="number revenue-t3">2,110,679,000</td>
        <td class="number revenue-t3">-1,031,807,000</td>
        <td class="number revenue-t3">1,078,872,000</td>
        <td class="number revenue-t3">-361,849,000</td>
        <td class="number revenue-t3">717,023,000</td>
        <td class="number revenue-t3">-110,970,000</td>
        <td class="number revenue-t3">606,053,000</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">606,053,000</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-even">H2</td>
        <td class="number assets-non-h2">0</td>
        <td class="number assets-cur-h2">49,484,853,000</td>
        <td class="number assets-tot-h2">49,484,853,000</td>
        <td class="number equity-h2">6,362,566,000</td>
        <td class="number liabilities-non-h2">0</td>
        <td class="number liabilities-cur-h2">43,122,287,000</td>
        <td class="number liabilities-tot-h2">43,122,287,000</td>
        <td class="number revenue-h2">1,433,873,000</td>
        <td class="number revenue-h2">-705,450,000</td>
        <td class="number revenue-h2">728,423,000</td>
        <td class="number revenue-h2">-282,404,000</td>
        <td class="number revenue-h2">446,019,000</td>
        <td class="number revenue-h2">-64,992,000</td>
        <td class="number revenue-h2">381,027,000</td>
        <td class="number revenue-h2">-49,498,000</td>
        <td class="number revenue-h2">331,529,000</td>
        <td class="number cashflow-h2">1,620,543,000</td>
        <td class="number cashflow-h2">-33,162,000</td>
        <td class="number cashflow-h2">-266,398,000</td>
        <td class="number cashflow-h2">1,320,982,000</td>
        <td class="number cashflow-h2">3,265,893,000</td>
        <td class="number cashflow-h2">4,586,875,000</td>
        <td class="number cashflow-h2">-31,699,000</td>
        <td class="number cashflow-h2">1,588,844,000</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-even">Q1</td>
        <td class="number assets-non-q1">0</td>
        <td class="number assets-cur-q1">47,515,408,000</td>
        <td class="number assets-tot-q1">47,515,408,000</td>
        <td class="number equity-q1">6,386,117,000</td>
        <td class="number liabilities-non-q1">0</td>
        <td class="number liabilities-cur-q1">41,129,291,000</td>
        <td class="number liabilities-tot-q1">41,129,291,000</td>
        <td class="number revenue-q1">647,329,000</td>
        <td class="number revenue-q1">-404,620,000</td>
        <td class="number revenue-q1">242,709,000</td>
        <td class="number revenue-q1">-151,515,000</td>
        <td class="number revenue-q1">91,194,000</td>
        <td class="number revenue-q1">-18,317,000</td>
        <td class="number revenue-q1">72,878,000</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">72,878,000</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
      </tr>
    </tbody>
  </table>
</div>

## Valuation

**Coming soon...**
