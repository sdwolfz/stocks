---
title: "SIF MOLDOVA S.A. (SIF2)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/bvb/">BVB</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/bvb/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>SIF MOLDOVA S.A.</td></tr>
    <tr><td>Symbol</td><td>SIF2</td></tr>
    <tr><td>Web</td><td><a href="http://www.sifm.ro">www.sifm.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.06 |
| 2018 | 0.0304 |
| 2017 | 0.05 |
| 2016 | 0.044 |
| 2015 | 0.045 |
| 2014 | 0.1012 |
| 2013 | 0.066 |
| 2012 | 0.24 |
| 2011 | 0.22 |
| 2010 | 0.09 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
