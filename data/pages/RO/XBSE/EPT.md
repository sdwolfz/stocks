---
title: "ELECTROPUTERE S.A. (EPT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>ELECTROPUTERE S.A.</td></tr>
    <tr><td>Symbol</td><td>EPT</td></tr>
    <tr><td>Web</td><td><a href="https://www.electroputere.ro">www.electroputere.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2000 | 0.0253 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
