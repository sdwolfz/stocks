---
title: " (ASML)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ASML</td></tr>
    <tr><td>Web</td><td><a href="https://www.asml.com">www.asml.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.87 |
| 2020 | 2.853 |
| 2019 | 3.513 |
| 2018 | 1.69 |
| 2017 | 1.307 |
| 2016 | 1.209 |
| 2015 | 0.76 |
| 2014 | 0.844 |
| 2013 | 0.69 |
| 2012 | 12.471 |
| 2011 | 0.585 |
| 2010 | 0.241 |
| 2009 | 0.23 |
| 2008 | 0.315 |
| 2007 | 2.84 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
