---
title: "UNIQUE FABRICATING (UFAB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>UNIQUE FABRICATING</td></tr>
    <tr><td>Symbol</td><td>UFAB</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.05 |
| 2018 | 0.6 |
| 2017 | 0.6 |
| 2016 | 0.6 |
| 2015 | 0.3 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
