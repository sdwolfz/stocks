---
title: "OLD NATIONAL BANCORP(INDIANA) (ONB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>OLD NATIONAL BANCORP(INDIANA)</td></tr>
    <tr><td>Symbol</td><td>ONB</td></tr>
    <tr><td>Web</td><td><a href="https://www.oldnational.com">www.oldnational.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.28 |
| 2020 | 0.56 |
| 2019 | 0.52 |
| 2018 | 0.52 |
| 2017 | 0.52 |
| 2016 | 0.52 |
| 2015 | 0.48 |
| 2014 | 0.44 |
| 2013 | 0.4 |
| 2012 | 0.36 |
| 2011 | 0.28 |
| 2010 | 0.28 |
| 2009 | 0.44 |
| 2008 | 0.92 |
| 2007 | 0.88 |
| 2006 | 0.84 |
| 2005 | 0.76 |
| 2004 | 0.76 |
| 2003 | 0.76 |
| 2002 | 0.72 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
