---
title: "CLEARONE INC (CLRO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CLEARONE INC</td></tr>
    <tr><td>Symbol</td><td>CLRO</td></tr>
    <tr><td>Web</td><td><a href="https://www.clearone.com">www.clearone.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.07 |
| 2017 | 0.26 |
| 2016 | 0.2 |
| 2015 | 0.155 |
| 2014 | 0.1 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
