---
title: "ROCKY MOUNTAIN CHOCOLATE FACTORY IN (RMCF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ROCKY MOUNTAIN CHOCOLATE FACTORY IN</td></tr>
    <tr><td>Symbol</td><td>RMCF</td></tr>
    <tr><td>Web</td><td><a href="https://www.rmcf.com">www.rmcf.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.12 |
| 2019 | 0.48 |
| 2018 | 0.48 |
| 2017 | 0.48 |
| 2016 | 0.48 |
| 2015 | 0.48 |
| 2014 | 0.44 |
| 2013 | 0.44 |
| 2012 | 0.43 |
| 2011 | 0.4 |
| 2010 | 0.4 |
| 2009 | 0.4 |
| 2008 | 0.4 |
| 2007 | 0.385 |
| 2006 | 0.33 |
| 2005 | 0.318 |
| 2004 | 0.26 |
| 2003 | 0.156 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
