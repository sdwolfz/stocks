---
title: "PEPSICO INC (PEP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PEPSICO INC</td></tr>
    <tr><td>Symbol</td><td>PEP</td></tr>
    <tr><td>Web</td><td><a href="https://www.pepsico.com">www.pepsico.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.098 |
| 2020 | 4.023 |
| 2019 | 3.792 |
| 2018 | 3.588 |
| 2017 | 3.168 |
| 2016 | 2.96 |
| 2015 | 2.763 |
| 2014 | 2.533 |
| 2013 | 2.24 |
| 2012 | 2.128 |
| 2011 | 2.025 |
| 2010 | 1.89 |
| 2009 | 1.35 |
| 2008 | 1.65 |
| 2007 | 1.425 |
| 2006 | 1.16 |
| 2005 | 1.01 |
| 2004 | 0.85 |
| 2003 | 0.63 |
| 2002 | 0.595 |
| 2001 | 0.72 |
| 2000 | 0.28 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
