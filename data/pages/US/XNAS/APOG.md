---
title: "APOGEE ENTERPRISES INC (APOG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>APOGEE ENTERPRISES INC</td></tr>
    <tr><td>Symbol</td><td>APOG</td></tr>
    <tr><td>Web</td><td><a href="https://www.apog.com">www.apog.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.4 |
| 2020 | 0.75 |
| 2019 | 0.7 |
| 2018 | 0.63 |
| 2017 | 0.56 |
| 2016 | 0.5 |
| 2015 | 0.44 |
| 2014 | 0.4 |
| 2013 | 0.36 |
| 2012 | 0.352 |
| 2011 | 0.326 |
| 2010 | 0.326 |
| 2009 | 0.326 |
| 2008 | 0.304 |
| 2007 | 0.277 |
| 2006 | 0.263 |
| 2005 | 0.253 |
| 2004 | 0.243 |
| 2003 | 0.232 |
| 2002 | 0.223 |
| 2001 | 0.213 |
| 2000 | 0.21 |
| 1999 | 0.21 |
| 1998 | 0.203 |
| 1997 | 0.185 |
| 1996 | 0.345 |
| 1995 | 0.325 |
| 1994 | 0.305 |
| 1993 | 0.285 |
| 1992 | 0.265 |
| 1991 | 0.255 |
| 1990 | 0.23 |
| 1989 | 0.19 |
| 1988 | 0.04 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
