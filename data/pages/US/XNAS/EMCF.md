---
title: "EMCLAIRE FINANCIAL CORP (EMCF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>EMCLAIRE FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>EMCF</td></tr>
    <tr><td>Web</td><td><a href="https://www.emclairefinancial.com">www.emclairefinancial.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |
| 2020 | 1.2 |
| 2019 | 1.16 |
| 2018 | 1.12 |
| 2017 | 1.08 |
| 2016 | 1.04 |
| 2015 | 0.96 |
| 2014 | 0.88 |
| 2013 | 0.8 |
| 2012 | 0.82 |
| 2011 | 0.64 |
| 2010 | 0.56 |
| 2009 | 0.74 |
| 2008 | 1.3 |
| 2007 | 1.54 |
| 2006 | 1.1 |
| 2005 | 1.02 |
| 2004 | 0.94 |
| 2003 | 1.11 |
| 2002 | 1.03 |
| 2001 | 0.7 |
| 2000 | 0.62 |
| 1999 | 0.57 |
| 1998 | 0.14 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
