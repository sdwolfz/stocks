---
title: "FLIR SYSTEMS INC (FLIR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FLIR SYSTEMS INC</td></tr>
    <tr><td>Symbol</td><td>FLIR</td></tr>
    <tr><td>Web</td><td><a href="https://www.flir.com">www.flir.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.34 |
| 2020 | 0.68 |
| 2019 | 0.68 |
| 2018 | 0.64 |
| 2017 | 0.6 |
| 2016 | 0.48 |
| 2015 | 0.44 |
| 2014 | 0.4 |
| 2013 | 0.36 |
| 2012 | 0.28 |
| 2011 | 0.24 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
