---
title: "HAWAIIAN HOLDINGS INC (HA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HAWAIIAN HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>HA</td></tr>
    <tr><td>Web</td><td><a href="https://www.hawaiianairlines.com">www.hawaiianairlines.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.12 |
| 2019 | 0.48 |
| 2018 | 0.48 |
| 2017 | 0.12 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
