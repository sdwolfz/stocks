---
title: "BETTERWARE DE MEXICO S A B DE (BWMX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BETTERWARE DE MEXICO S A B DE</td></tr>
    <tr><td>Symbol</td><td>BWMX</td></tr>
    <tr><td>Web</td><td><a href="https://www.betterware.com.mx">www.betterware.com.mx</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.431 |
| 2020 | 0.942 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
