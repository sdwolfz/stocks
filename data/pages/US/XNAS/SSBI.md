---
title: "SUMMIT STATE BANK SANTA ROSA CALIF (SSBI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SUMMIT STATE BANK SANTA ROSA CALIF</td></tr>
    <tr><td>Symbol</td><td>SSBI</td></tr>
    <tr><td>Web</td><td><a href="https://www.summitstatebank.com">www.summitstatebank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.24 |
| 2020 | 0.48 |
| 2019 | 0.48 |
| 2018 | 0.48 |
| 2017 | 0.48 |
| 2016 | 0.48 |
| 2015 | 0.48 |
| 2014 | 0.44 |
| 2013 | 0.42 |
| 2012 | 0.36 |
| 2011 | 0.36 |
| 2010 | 0.36 |
| 2009 | 0.36 |
| 2008 | 0.36 |
| 2007 | 0.36 |
| 2006 | 0.18 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
