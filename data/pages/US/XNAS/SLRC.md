---
title: " (SLRC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SLRC</td></tr>
    <tr><td>Web</td><td><a href="https://www.slrinvestmentcorp.com">www.slrinvestmentcorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.82 |
| 2020 | 1.64 |
| 2019 | 1.64 |
| 2018 | 1.64 |
| 2017 | 1.6 |
| 2016 | 1.6 |
| 2015 | 1.6 |
| 2014 | 1.6 |
| 2013 | 2.0 |
| 2012 | 2.4 |
| 2011 | 2.4 |
| 2010 | 2.14 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
