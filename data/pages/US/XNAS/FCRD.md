---
title: "FIRST EAGLE ALTERNATIVE CAP BDC INC (FCRD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIRST EAGLE ALTERNATIVE CAP BDC INC</td></tr>
    <tr><td>Symbol</td><td>FCRD</td></tr>
    <tr><td>Web</td><td><a href="https://www.investor.feacbdc.com">www.investor.feacbdc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.51 |
| 2019 | 0.84 |
| 2018 | 1.08 |
| 2017 | 1.08 |
| 2016 | 1.29 |
| 2015 | 1.36 |
| 2014 | 1.36 |
| 2013 | 1.43 |
| 2012 | 1.34 |
| 2011 | 1.02 |
| 2010 | 0.3 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
