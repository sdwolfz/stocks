---
title: "COMMERCE BANCSHARES INC (CBSH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>COMMERCE BANCSHARES INC</td></tr>
    <tr><td>Symbol</td><td>CBSH</td></tr>
    <tr><td>Web</td><td><a href="https://www.commercebank.com">www.commercebank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.525 |
| 2020 | 0.81 |
| 2019 | 0.78 |
| 2018 | 0.705 |
| 2017 | 0.675 |
| 2016 | 0.675 |
| 2015 | 0.675 |
| 2014 | 0.675 |
| 2013 | 0.675 |
| 2012 | 0.69 |
| 2011 | 0.69 |
| 2010 | 0.705 |
| 2009 | 0.72 |
| 2008 | 0.75 |
| 2007 | 0.75 |
| 2006 | 0.735 |
| 2005 | 0.72 |
| 2004 | 0.69 |
| 2003 | 0.555 |
| 2002 | 0.488 |
| 2001 | 0.48 |
| 2000 | 0.465 |
| 1999 | 0.45 |
| 1998 | 0.508 |
| 1997 | 0.615 |
| 1996 | 0.57 |
| 1995 | 0.54 |
| 1994 | 0.66 |
| 1993 | 0.65 |
| 1992 | 0.79 |
| 1991 | 0.75 |
| 1990 | 0.7 |
| 1989 | 0.78 |
| 1988 | 0.3 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
