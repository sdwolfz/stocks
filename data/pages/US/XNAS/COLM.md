---
title: "COLUMBIA SPORTSWEAR CO (COLM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>COLUMBIA SPORTSWEAR CO</td></tr>
    <tr><td>Symbol</td><td>COLM</td></tr>
    <tr><td>Web</td><td><a href="https://www.columbia.com">www.columbia.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.52 |
| 2020 | 0.26 |
| 2019 | 0.96 |
| 2018 | 0.9 |
| 2017 | 0.73 |
| 2016 | 0.69 |
| 2015 | 0.62 |
| 2014 | 0.99 |
| 2013 | 0.91 |
| 2012 | 0.88 |
| 2011 | 0.86 |
| 2010 | 2.24 |
| 2009 | 0.66 |
| 2008 | 0.64 |
| 2007 | 0.58 |
| 2006 | 0.14 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
