---
title: "SPARK ENERGY INC (SPKE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SPARK ENERGY INC</td></tr>
    <tr><td>Symbol</td><td>SPKE</td></tr>
    <tr><td>Web</td><td><a href="https://www.sparkenergy.com">www.sparkenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.363 |
| 2020 | 0.725 |
| 2019 | 0.725 |
| 2018 | 0.725 |
| 2017 | 1.088 |
| 2016 | 1.45 |
| 2015 | 1.45 |
| 2014 | 0.24 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
