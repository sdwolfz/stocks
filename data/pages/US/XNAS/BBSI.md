---
title: "BARRETT BUSINESS SERVICES (BBSI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BARRETT BUSINESS SERVICES</td></tr>
    <tr><td>Symbol</td><td>BBSI</td></tr>
    <tr><td>Web</td><td><a href="https://www.bbsi.com">www.bbsi.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.6 |
| 2020 | 1.2 |
| 2019 | 1.1 |
| 2018 | 1.0 |
| 2017 | 1.0 |
| 2016 | 0.88 |
| 2015 | 0.88 |
| 2014 | 0.76 |
| 2013 | 0.57 |
| 2012 | 0.46 |
| 2011 | 0.38 |
| 2010 | 0.33 |
| 2009 | 0.32 |
| 2008 | 0.32 |
| 2007 | 0.29 |
| 2006 | 0.07 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
