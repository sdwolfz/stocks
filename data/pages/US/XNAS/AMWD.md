---
title: "AMERICAN WOODMARK CORP (AMWD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AMERICAN WOODMARK CORP</td></tr>
    <tr><td>Symbol</td><td>AMWD</td></tr>
    <tr><td>Web</td><td><a href="https://www.americanwoodmark.com">www.americanwoodmark.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2011 | 0.18 |
| 2010 | 0.45 |
| 2009 | 0.36 |
| 2008 | 0.36 |
| 2007 | 0.3 |
| 2006 | 0.18 |
| 2005 | 0.12 |
| 2004 | 0.19 |
| 2003 | 0.2 |
| 2002 | 0.2 |
| 2001 | 0.2 |
| 2000 | 0.2 |
| 1999 | 0.18 |
| 1998 | 0.14 |
| 1997 | 0.1 |
| 1996 | 0.04 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
