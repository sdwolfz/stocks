---
title: "UNIVEST FINANCIAL CORPORATION (UVSP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>UNIVEST FINANCIAL CORPORATION</td></tr>
    <tr><td>Symbol</td><td>UVSP</td></tr>
    <tr><td>Web</td><td><a href="https://www.univest.net">www.univest.net</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.4 |
| 2020 | 0.6 |
| 2019 | 0.8 |
| 2018 | 0.8 |
| 2017 | 0.8 |
| 2016 | 0.8 |
| 2015 | 0.8 |
| 2014 | 0.8 |
| 2013 | 0.8 |
| 2012 | 0.8 |
| 2011 | 0.8 |
| 2010 | 0.8 |
| 2009 | 0.8 |
| 2008 | 0.8 |
| 2007 | 0.8 |
| 2006 | 0.78 |
| 2005 | 0.8 |
| 2004 | 1.0 |
| 2003 | 0.8 |
| 2002 | 0.92 |
| 2001 | 0.82 |
| 2000 | 0.74 |
| 1999 | 0.66 |
| 1998 | 0.15 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
