---
title: " (ACGLP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ACGLP</td></tr>
    <tr><td>Web</td><td><a href="https://www.archcapgroup.com">www.archcapgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.328 |
| 2020 | 1.313 |
| 2019 | 1.313 |
| 2018 | 1.313 |
| 2017 | 1.305 |
| 2016 | 0.343 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
