---
title: "PATRICK INDUSTRIES INC (PATK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PATRICK INDUSTRIES INC</td></tr>
    <tr><td>Symbol</td><td>PATK</td></tr>
    <tr><td>Web</td><td><a href="https://www.patrickind.com">www.patrickind.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.28 |
| 2020 | 1.03 |
| 2019 | 0.25 |
| 2003 | 0.04 |
| 2002 | 0.16 |
| 2001 | 0.16 |
| 2000 | 0.16 |
| 1999 | 0.16 |
| 1998 | 0.16 |
| 1997 | 0.12 |
| 1996 | 0.16 |
| 1995 | 0.12 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
