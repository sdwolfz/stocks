---
title: "THE ODP CORPORATION (ODP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>THE ODP CORPORATION</td></tr>
    <tr><td>Symbol</td><td>ODP</td></tr>
    <tr><td>Web</td><td><a href="https://www.officedepot.com">www.officedepot.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.025 |
| 2019 | 0.1 |
| 2018 | 0.1 |
| 2017 | 0.1 |
| 2016 | 0.05 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
