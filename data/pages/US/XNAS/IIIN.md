---
title: "INSTEEL INDUSTRIES INC (IIIN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>INSTEEL INDUSTRIES INC</td></tr>
    <tr><td>Symbol</td><td>IIIN</td></tr>
    <tr><td>Web</td><td><a href="http://www.insteel.com">www.insteel.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.62 |
| 2019 | 0.12 |
| 2018 | 0.12 |
| 2017 | 1.12 |
| 2016 | 1.37 |
| 2015 | 1.12 |
| 2014 | 0.12 |
| 2013 | 0.12 |
| 2012 | 0.37 |
| 2011 | 0.12 |
| 2010 | 0.12 |
| 2009 | 0.12 |
| 2008 | 0.62 |
| 2007 | 0.12 |
| 2006 | 0.15 |
| 2005 | 0.18 |
| 2000 | 0.06 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
