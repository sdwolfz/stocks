---
title: "MAIDEN HOLDINGS LTD (MHLD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MAIDEN HOLDINGS LTD</td></tr>
    <tr><td>Symbol</td><td>MHLD</td></tr>
    <tr><td>Web</td><td><a href="https://www.maiden.bm">www.maiden.bm</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.35 |
| 2017 | 0.6 |
| 2016 | 0.57 |
| 2015 | 0.53 |
| 2014 | 0.46 |
| 2013 | 0.38 |
| 2012 | 0.33 |
| 2011 | 0.3 |
| 2010 | 0.265 |
| 2009 | 0.245 |
| 2008 | 0.16 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
