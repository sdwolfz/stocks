---
title: "BANKWELL FINL GROUP INC (BWFG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BANKWELL FINL GROUP INC</td></tr>
    <tr><td>Symbol</td><td>BWFG</td></tr>
    <tr><td>Web</td><td><a href="https://www.mybankwell.com">www.mybankwell.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.28 |
| 2020 | 0.56 |
| 2019 | 0.52 |
| 2018 | 0.48 |
| 2017 | 0.28 |
| 2016 | 0.22 |
| 2015 | 0.05 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
