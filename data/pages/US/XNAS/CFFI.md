---
title: "C & F FINANCIAL CORP (CFFI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>C & F FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>CFFI</td></tr>
    <tr><td>Web</td><td><a href="https://www.cffc.com">www.cffc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.38 |
| 2020 | 1.52 |
| 2019 | 1.49 |
| 2018 | 1.41 |
| 2017 | 1.33 |
| 2016 | 1.29 |
| 2015 | 1.22 |
| 2014 | 1.19 |
| 2013 | 1.16 |
| 2012 | 1.08 |
| 2011 | 1.01 |
| 2010 | 1.0 |
| 2009 | 1.06 |
| 2008 | 1.24 |
| 2007 | 1.24 |
| 2006 | 1.16 |
| 2005 | 1.0 |
| 2004 | 0.9 |
| 2003 | 0.72 |
| 2002 | 0.62 |
| 2001 | 0.58 |
| 2000 | 0.53 |
| 1999 | 0.49 |
| 1998 | 0.65 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
