---
title: "AIR T INC (AIRT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AIR T INC</td></tr>
    <tr><td>Symbol</td><td>AIRT</td></tr>
    <tr><td>Web</td><td><a href="https://www.airt.net">www.airt.net</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2013 | 0.3 |
| 2012 | 0.25 |
| 2011 | 0.25 |
| 2010 | 0.33 |
| 2009 | 0.33 |
| 2008 | 0.3 |
| 2007 | 0.25 |
| 2006 | 0.25 |
| 2005 | 0.25 |
| 2004 | 0.2 |
| 2002 | 0.12 |
| 2001 | 0.15 |
| 2000 | 0.1 |
| 1999 | 0.08 |
| 1998 | 0.14 |
| 1997 | 0.1 |
| 1996 | 0.08 |
| 1995 | 0.07 |
| 1993 | 0.01 |
| 1991 | 0.01 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
