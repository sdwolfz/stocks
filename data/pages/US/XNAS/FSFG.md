---
title: "FIRST SAVINGS FINANCIAL GROUP INC (FSFG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIRST SAVINGS FINANCIAL GROUP INC</td></tr>
    <tr><td>Symbol</td><td>FSFG</td></tr>
    <tr><td>Web</td><td><a href="https://www.fsbbank.net">www.fsbbank.net</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.18 |
| 2020 | 0.68 |
| 2019 | 0.64 |
| 2018 | 0.6 |
| 2017 | 0.56 |
| 2016 | 0.52 |
| 2015 | 0.48 |
| 2014 | 0.44 |
| 2013 | 0.4 |
| 2012 | 0.4 |
| 2009 | 0.08 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
