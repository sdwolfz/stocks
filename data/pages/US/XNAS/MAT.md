---
title: "MATTEL INC (MAT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MATTEL INC</td></tr>
    <tr><td>Symbol</td><td>MAT</td></tr>
    <tr><td>Web</td><td><a href="https://corporate.mattel.com">corporate.mattel.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 0.91 |
| 2016 | 1.52 |
| 2015 | 1.52 |
| 2014 | 1.52 |
| 2013 | 1.44 |
| 2012 | 1.24 |
| 2011 | 0.92 |
| 2010 | 0.83 |
| 2009 | 0.75 |
| 2008 | 0.75 |
| 2007 | 0.75 |
| 2006 | 0.65 |
| 2005 | 0.5 |
| 2004 | 0.45 |
| 2003 | 0.4 |
| 2002 | 0.05 |
| 2001 | 0.05 |
| 2000 | 0.09 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
