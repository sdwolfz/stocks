---
title: "HALLADOR ENERGY CO (HNRG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HALLADOR ENERGY CO</td></tr>
    <tr><td>Symbol</td><td>HNRG</td></tr>
    <tr><td>Web</td><td><a href="https://www.halladorenergy.com">www.halladorenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.04 |
| 2019 | 0.16 |
| 2018 | 0.16 |
| 2017 | 0.16 |
| 2016 | 0.16 |
| 2015 | 0.16 |
| 2014 | 0.16 |
| 2013 | 0.12 |
| 2012 | 0.8 |
| 2011 | 0.12 |
| 2010 | 0.1 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
