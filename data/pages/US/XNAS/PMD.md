---
title: "PSYCHEMEDICS CORP (PMD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PSYCHEMEDICS CORP</td></tr>
    <tr><td>Symbol</td><td>PMD</td></tr>
    <tr><td>Web</td><td><a href="https://www.psychemedics.com">www.psychemedics.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.18 |
| 2019 | 0.72 |
| 2018 | 0.69 |
| 2017 | 0.6 |
| 2016 | 0.6 |
| 2015 | 0.6 |
| 2014 | 0.6 |
| 2013 | 0.6 |
| 2012 | 0.6 |
| 2011 | 0.48 |
| 2010 | 0.48 |
| 2009 | 0.58 |
| 2008 | 1.16 |
| 2007 | 0.575 |
| 2006 | 0.475 |
| 2005 | 0.36 |
| 2004 | 0.32 |
| 2003 | 0.32 |
| 2002 | 0.18 |
| 2001 | 0.1 |
| 2000 | 0.16 |
| 1999 | 0.04 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
