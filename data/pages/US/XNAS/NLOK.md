---
title: "NORTONLIFELOCK INC (NLOK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NORTONLIFELOCK INC</td></tr>
    <tr><td>Symbol</td><td>NLOK</td></tr>
    <tr><td>Web</td><td><a href="https://www.nortonlifelock.com">www.nortonlifelock.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.125 |
| 2020 | 12.5 |
| 2019 | 0.35 |
| 2018 | 0.3 |
| 2017 | 0.3 |
| 2016 | 4.375 |
| 2015 | 0.6 |
| 2014 | 0.6 |
| 2013 | 0.45 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
