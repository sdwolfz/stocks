---
title: "VIRTUS INVESTMENT PARTNERS INC (VRTS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>VIRTUS INVESTMENT PARTNERS INC</td></tr>
    <tr><td>Symbol</td><td>VRTS</td></tr>
    <tr><td>Web</td><td><a href="https://www.virtus.com">www.virtus.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.64 |
| 2020 | 2.83 |
| 2019 | 2.32 |
| 2018 | 1.9 |
| 2017 | 1.8 |
| 2016 | 1.8 |
| 2015 | 1.8 |
| 2014 | 0.9 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
