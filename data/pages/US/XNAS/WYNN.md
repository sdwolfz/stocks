---
title: "WYNN RESORTS LTD (WYNN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>WYNN RESORTS LTD</td></tr>
    <tr><td>Symbol</td><td>WYNN</td></tr>
    <tr><td>Web</td><td><a href="https://www.wynnresorts.com">www.wynnresorts.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.0 |
| 2019 | 3.75 |
| 2018 | 2.75 |
| 2017 | 2.0 |
| 2016 | 2.0 |
| 2015 | 3.0 |
| 2014 | 6.25 |
| 2013 | 7.0 |
| 2012 | 9.5 |
| 2011 | 6.5 |
| 2010 | 8.5 |
| 2009 | 4.0 |
| 2007 | 6.0 |
| 2006 | 6.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
