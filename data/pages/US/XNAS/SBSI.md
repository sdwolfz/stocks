---
title: "SOUTHSIDE BANCSHARES INC (SBSI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SOUTHSIDE BANCSHARES INC</td></tr>
    <tr><td>Symbol</td><td>SBSI</td></tr>
    <tr><td>Web</td><td><a href="https://www.southside.com">www.southside.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.65 |
| 2020 | 1.3 |
| 2019 | 1.26 |
| 2018 | 1.2 |
| 2017 | 1.11 |
| 2016 | 1.01 |
| 2015 | 1.0 |
| 2014 | 0.96 |
| 2013 | 0.91 |
| 2012 | 1.11 |
| 2011 | 0.9 |
| 2010 | 0.85 |
| 2009 | 0.75 |
| 2008 | 0.6 |
| 2007 | 0.5 |
| 2006 | 0.36 |
| 2005 | 0.35 |
| 2004 | 0.32 |
| 2003 | 0.26 |
| 2002 | 0.26 |
| 2001 | 0.25 |
| 2000 | 0.275 |
| 1999 | 0.3 |
| 1998 | 0.2 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
