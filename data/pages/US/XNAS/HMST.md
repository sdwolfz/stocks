---
title: "HOMESTREET INC (HMST)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HOMESTREET INC</td></tr>
    <tr><td>Symbol</td><td>HMST</td></tr>
    <tr><td>Web</td><td><a href="https://www.homestreet.com">www.homestreet.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.5 |
| 2020 | 0.6 |
| 2014 | 0.11 |
| 2013 | 0.33 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
