---
title: " (ZIONO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ZIONO</td></tr>
    <tr><td>Web</td><td><a href="https://www.zionsbancorp.com">www.zionsbancorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.394 |
| 2020 | 1.575 |
| 2019 | 1.575 |
| 2018 | 1.575 |
| 2017 | 1.575 |
| 2016 | 1.575 |
| 2015 | 1.575 |
| 2014 | 1.575 |
| 2013 | 1.352 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
