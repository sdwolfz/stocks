---
title: " (IPLDP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>IPLDP</td></tr>
    <tr><td>Web</td><td><a href="https://www.alliantenergy.com">www.alliantenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.319 |
| 2020 | 1.275 |
| 2019 | 1.275 |
| 2018 | 1.275 |
| 2017 | 1.275 |
| 2016 | 1.275 |
| 2015 | 1.275 |
| 2014 | 1.275 |
| 2013 | 0.942 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
