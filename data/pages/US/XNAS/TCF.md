---
title: "TCF FINANCIAL CORPORATION (TCF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TCF FINANCIAL CORPORATION</td></tr>
    <tr><td>Symbol</td><td>TCF</td></tr>
    <tr><td>Web</td><td><a href="https://www.tcfbank.com">www.tcfbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.35 |
| 2020 | 1.4 |
| 2019 | 1.38 |
| 2018 | 1.24 |
| 2017 | 1.1 |
| 2016 | 1.06 |
| 2015 | 1.0 |
| 2014 | 0.94 |
| 2013 | 0.87 |
| 2012 | 0.82 |
| 2011 | 0.8 |
| 2010 | 0.8 |
| 2009 | 1.18 |
| 2008 | 1.18 |
| 2007 | 1.14 |
| 2006 | 1.1 |
| 2005 | 1.06 |
| 2004 | 1.06 |
| 2003 | 1.0 |
| 2002 | 0.96 |
| 2001 | 0.96 |
| 2000 | 0.88 |
| 1999 | 0.84 |
| 1998 | 0.96 |
| 1997 | 0.88 |
| 1996 | 0.8 |
| 1995 | 0.68 |
| 1994 | 0.84 |
| 1993 | 0.8 |
| 1992 | 0.88 |
| 1991 | 1.04 |
| 1990 | 1.0 |
| 1989 | 1.0 |
| 1988 | 0.25 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
