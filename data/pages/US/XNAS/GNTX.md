---
title: "GENTEX CORP (GNTX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GENTEX CORP</td></tr>
    <tr><td>Symbol</td><td>GNTX</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.24 |
| 2020 | 0.475 |
| 2019 | 0.455 |
| 2018 | 0.43 |
| 2017 | 0.38 |
| 2016 | 0.35 |
| 2015 | 0.33 |
| 2014 | 0.6 |
| 2013 | 0.55 |
| 2012 | 0.51 |
| 2011 | 0.47 |
| 2010 | 0.44 |
| 2009 | 0.44 |
| 2008 | 0.425 |
| 2007 | 0.39 |
| 2006 | 0.365 |
| 2005 | 0.515 |
| 2004 | 0.62 |
| 2003 | 0.15 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
