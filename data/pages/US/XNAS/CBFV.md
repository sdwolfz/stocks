---
title: "CB FINANCIAL SERVICES INC (CBFV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CB FINANCIAL SERVICES INC</td></tr>
    <tr><td>Symbol</td><td>CBFV</td></tr>
    <tr><td>Web</td><td><a href="https://www.communitybank.tv">www.communitybank.tv</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.48 |
| 2020 | 0.96 |
| 2019 | 0.96 |
| 2018 | 0.89 |
| 2017 | 0.88 |
| 2016 | 0.88 |
| 2015 | 0.85 |
| 2014 | 0.84 |
| 2013 | 0.84 |
| 2012 | 0.84 |
| 2011 | 0.83 |
| 2010 | 0.81 |
| 2009 | 0.77 |
| 2008 | 0.71 |
| 2007 | 0.64 |
| 2006 | 0.27 |
| 2005 | 0.45 |
| 2004 | 0.39 |
| 2003 | 0.38 |
| 2002 | 0.37 |
| 2001 | 0.33 |
| 2000 | 0.33 |
| 1999 | 0.25 |
| 1998 | 0.48 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
