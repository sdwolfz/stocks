---
title: " (PTNR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>PTNR</td></tr>
    <tr><td>Web</td><td><a href="https://www.partner.co.il">www.partner.co.il</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2012 | 0.26 |
| 2011 | 1.168 |
| 2010 | 3.76 |
| 2009 | 1.225 |
| 2008 | 1.27 |
| 2007 | 0.711 |
| 2006 | 0.318 |
| 2005 | 0.095 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
