---
title: "DIAMOND HILL INVESTMENT GROUP (DHIL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>DIAMOND HILL INVESTMENT GROUP</td></tr>
    <tr><td>Symbol</td><td>DHIL</td></tr>
    <tr><td>Web</td><td><a href="https://www.diamond-hill.com">www.diamond-hill.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.0 |
| 2020 | 12.0 |
| 2019 | 9.0 |
| 2018 | 8.0 |
| 2017 | 7.0 |
| 2016 | 6.0 |
| 2015 | 5.0 |
| 2014 | 4.0 |
| 2013 | 3.0 |
| 2012 | 8.0 |
| 2011 | 5.0 |
| 2010 | 13.0 |
| 2009 | 10.0 |
| 2008 | 10.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
