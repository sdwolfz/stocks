---
title: "KENTUCKY FIRST FEDERAL BANCORP (KFFB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>KENTUCKY FIRST FEDERAL BANCORP</td></tr>
    <tr><td>Symbol</td><td>KFFB</td></tr>
    <tr><td>Web</td><td><a href="https://www.ffsbky.com">www.ffsbky.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.4 |
| 2019 | 0.4 |
| 2018 | 0.4 |
| 2017 | 0.4 |
| 2016 | 0.4 |
| 2015 | 0.4 |
| 2014 | 0.4 |
| 2013 | 0.4 |
| 2012 | 0.4 |
| 2011 | 0.4 |
| 2010 | 0.4 |
| 2009 | 0.4 |
| 2008 | 0.4 |
| 2007 | 0.4 |
| 2006 | 0.4 |
| 2005 | 0.3 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
