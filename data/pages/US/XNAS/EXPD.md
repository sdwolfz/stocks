---
title: "EXPEDITORS INTERN OF WASHINGTON INC (EXPD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>EXPEDITORS INTERN OF WASHINGTON INC</td></tr>
    <tr><td>Symbol</td><td>EXPD</td></tr>
    <tr><td>Web</td><td><a href="https://www.expeditors.com">www.expeditors.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.58 |
| 2020 | 1.04 |
| 2019 | 1.0 |
| 2018 | 0.9 |
| 2017 | 0.84 |
| 2016 | 0.8 |
| 2015 | 0.72 |
| 2014 | 0.64 |
| 2013 | 0.6 |
| 2012 | 0.56 |
| 2011 | 0.5 |
| 2010 | 0.4 |
| 2009 | 0.38 |
| 2008 | 0.32 |
| 2007 | 0.28 |
| 2006 | 0.33 |
| 2005 | 0.3 |
| 2004 | 0.22 |
| 2003 | 0.16 |
| 2002 | 0.18 |
| 2001 | 0.2 |
| 2000 | 0.14 |
| 1999 | 0.1 |
| 1998 | 0.14 |
| 1997 | 0.1 |
| 1996 | 0.16 |
| 1995 | 0.12 |
| 1994 | 0.1 |
| 1993 | 0.15 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
