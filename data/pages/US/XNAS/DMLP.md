---
title: " (DMLP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>DMLP</td></tr>
    <tr><td>Web</td><td><a href="https://www.dmlp.net">www.dmlp.net</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.546 |
| 2020 | 1.391 |
| 2019 | 2.013 |
| 2018 | 1.737 |
| 2017 | 1.156 |
| 2016 | 0.857 |
| 2015 | 1.154 |
| 2014 | 1.903 |
| 2013 | 1.732 |
| 2012 | 1.79 |
| 2011 | 1.653 |
| 2010 | 1.654 |
| 2009 | 1.502 |
| 2008 | 2.805 |
| 2007 | 1.974 |
| 2006 | 2.83 |
| 2005 | 1.999 |
| 2004 | 1.698 |
| 2003 | 1.087 |
| 2002 | 0.81 |
| 2001 | 1.23 |
| 2000 | 0.9 |
| 1999 | 0.72 |
| 1998 | 0.72 |
| 1997 | 0.72 |
| 1996 | 0.68 |
| 1995 | 0.68 |
| 1994 | 0.68 |
| 1993 | 0.61 |
| 1992 | 0.23 |
| 1991 | 0.2 |
| 1990 | 0.2 |
| 1989 | 0.3 |
| 1988 | 0.08 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
