---
title: "NVE CORP (NVEC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NVE CORP</td></tr>
    <tr><td>Symbol</td><td>NVEC</td></tr>
    <tr><td>Web</td><td><a href="https://www.nve.com">www.nve.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.0 |
| 2020 | 4.0 |
| 2019 | 4.0 |
| 2018 | 4.0 |
| 2017 | 4.0 |
| 2016 | 4.0 |
| 2015 | 5.06 |
| 1999 | 1.127 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
