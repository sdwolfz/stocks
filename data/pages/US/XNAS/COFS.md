---
title: "CHOICEONE FINANCIAL SERVICES INC (COFS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CHOICEONE FINANCIAL SERVICES INC</td></tr>
    <tr><td>Symbol</td><td>COFS</td></tr>
    <tr><td>Web</td><td><a href="https://www.choiceone.com">www.choiceone.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.22 |
| 2020 | 0.82 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
