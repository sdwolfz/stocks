---
title: "COLLIERS INTL GROUP INC (CIGI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>COLLIERS INTL GROUP INC</td></tr>
    <tr><td>Symbol</td><td>CIGI</td></tr>
    <tr><td>Web</td><td><a href="https://www.colliers.com">www.colliers.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.1 |
| 2019 | 0.1 |
| 2018 | 0.1 |
| 2017 | 0.1 |
| 2016 | 0.1 |
| 2015 | 0.14 |
| 2014 | 0.4 |
| 2013 | 0.3 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
