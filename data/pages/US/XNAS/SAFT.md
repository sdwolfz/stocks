---
title: "SAFETY INSURANCE GROUP INC (SAFT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SAFETY INSURANCE GROUP INC</td></tr>
    <tr><td>Symbol</td><td>SAFT</td></tr>
    <tr><td>Web</td><td><a href="https://www.safetyinsurance.com">www.safetyinsurance.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.8 |
| 2020 | 3.6 |
| 2019 | 3.4 |
| 2018 | 3.2 |
| 2017 | 3.0 |
| 2016 | 2.8 |
| 2015 | 2.8 |
| 2014 | 2.6 |
| 2013 | 2.4 |
| 2012 | 2.2 |
| 2011 | 2.0 |
| 2010 | 1.8 |
| 2009 | 1.6 |
| 2008 | 1.6 |
| 2007 | 1.3 |
| 2006 | 0.86 |
| 2005 | 0.6 |
| 2004 | 0.44 |
| 2003 | 0.34 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
