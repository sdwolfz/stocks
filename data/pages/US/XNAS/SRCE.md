---
title: "FIRST SOURCE CORP (SRCE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIRST SOURCE CORP</td></tr>
    <tr><td>Symbol</td><td>SRCE</td></tr>
    <tr><td>Web</td><td><a href="https://www.1stsource.com">www.1stsource.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.59 |
| 2020 | 1.13 |
| 2019 | 1.1 |
| 2018 | 0.96 |
| 2017 | 0.76 |
| 2016 | 0.72 |
| 2015 | 0.72 |
| 2014 | 0.71 |
| 2013 | 0.68 |
| 2012 | 0.66 |
| 2011 | 0.64 |
| 2010 | 0.61 |
| 2009 | 0.59 |
| 2008 | 0.58 |
| 2007 | 0.56 |
| 2006 | 0.56 |
| 2005 | 0.49 |
| 2004 | 0.42 |
| 2003 | 0.37 |
| 2002 | 0.36 |
| 2001 | 0.27 |
| 2000 | 0.27 |
| 1999 | 0.24 |
| 1998 | 0.233 |
| 1997 | 0.245 |
| 1996 | 0.25 |
| 1995 | 0.3 |
| 1994 | 0.32 |
| 1993 | 0.425 |
| 1992 | 0.51 |
| 1991 | 0.41 |
| 1990 | 0.37 |
| 1989 | 0.45 |
| 1988 | 0.11 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
