---
title: "PEOPLE'S UNITED FINANCIAL INC (PBCT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PEOPLE'S UNITED FINANCIAL INC</td></tr>
    <tr><td>Symbol</td><td>PBCT</td></tr>
    <tr><td>Web</td><td><a href="https://www.peoples.com">www.peoples.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.363 |
| 2020 | 0.718 |
| 2019 | 0.708 |
| 2018 | 0.697 |
| 2017 | 0.688 |
| 2016 | 0.678 |
| 2015 | 0.668 |
| 2014 | 0.658 |
| 2013 | 0.648 |
| 2012 | 0.638 |
| 2011 | 0.628 |
| 2010 | 0.617 |
| 2009 | 0.608 |
| 2008 | 0.583 |
| 2007 | 0.65 |
| 2006 | 0.97 |
| 2005 | 1.06 |
| 2004 | 1.405 |
| 2003 | 1.53 |
| 2002 | 1.42 |
| 2001 | 1.34 |
| 2000 | 1.2 |
| 1999 | 1.03 |
| 1998 | 0.84 |
| 1997 | 0.83 |
| 1996 | 0.8 |
| 1995 | 0.64 |
| 1994 | 0.47 |
| 1993 | 0.06 |
| 1990 | 0.72 |
| 1989 | 0.7 |
| 1988 | 0.16 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
