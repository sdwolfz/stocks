---
title: "COCA-COLA CONSOLIDATED INC (COKE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>COCA-COLA CONSOLIDATED INC</td></tr>
    <tr><td>Symbol</td><td>COKE</td></tr>
    <tr><td>Web</td><td><a href="https://www.cokeconsolidated.com">www.cokeconsolidated.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.5 |
| 2020 | 1.0 |
| 2019 | 1.0 |
| 2018 | 1.0 |
| 2017 | 1.0 |
| 2016 | 1.0 |
| 2015 | 1.0 |
| 2014 | 1.0 |
| 2013 | 1.0 |
| 2012 | 1.0 |
| 2011 | 1.0 |
| 2010 | 1.0 |
| 2009 | 1.0 |
| 2008 | 1.0 |
| 2007 | 1.0 |
| 2006 | 1.0 |
| 2005 | 1.0 |
| 2004 | 1.0 |
| 2003 | 1.0 |
| 2002 | 1.0 |
| 2001 | 1.0 |
| 2000 | 1.0 |
| 1999 | 1.0 |
| 1998 | 1.0 |
| 1997 | 1.0 |
| 1996 | 1.0 |
| 1995 | 1.0 |
| 1994 | 1.0 |
| 1993 | 0.88 |
| 1992 | 0.88 |
| 1991 | 0.88 |
| 1990 | 0.88 |
| 1989 | 0.88 |
| 1988 | 0.22 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
