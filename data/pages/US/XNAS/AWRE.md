---
title: "AWARE INC MASSACHUSETTS (AWRE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AWARE INC MASSACHUSETTS</td></tr>
    <tr><td>Symbol</td><td>AWRE</td></tr>
    <tr><td>Web</td><td><a href="https://www.aware.com">www.aware.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2014 | 1.75 |
| 2012 | 2.95 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
