---
title: "TAITRON COMPONENTS INC (TAIT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TAITRON COMPONENTS INC</td></tr>
    <tr><td>Symbol</td><td>TAIT</td></tr>
    <tr><td>Web</td><td><a href="https://www.taitroncomponents.com">www.taitroncomponents.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.08 |
| 2020 | 0.145 |
| 2019 | 0.125 |
| 2018 | 0.105 |
| 2017 | 0.1 |
| 2016 | 0.075 |
| 2009 | 0.05 |
| 2008 | 0.05 |
| 2007 | 0.1 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
