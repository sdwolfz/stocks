---
title: "CYTOMX THERAPEUTICS INC (CTMX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CYTOMX THERAPEUTICS INC</td></tr>
    <tr><td>Symbol</td><td>CTMX</td></tr>
    <tr><td>Web</td><td><a href="https://www.cytomx.com">www.cytomx.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
