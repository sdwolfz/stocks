---
title: "UNIVERSAL LOGISTICS HOLDINGS INC (ULH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>UNIVERSAL LOGISTICS HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>ULH</td></tr>
    <tr><td>Web</td><td><a href="https://www.universallogistics.com">www.universallogistics.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.21 |
| 2020 | 0.21 |
| 2019 | 0.635 |
| 2018 | 0.385 |
| 2017 | 0.28 |
| 2016 | 0.28 |
| 2015 | 0.28 |
| 2014 | 0.28 |
| 2013 | 0.14 |
| 2012 | 1.0 |
| 2011 | 1.0 |
| 2009 | 1.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
