---
title: "INVESTAR HOLDING CORP (ISTR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>INVESTAR HOLDING CORP</td></tr>
    <tr><td>Symbol</td><td>ISTR</td></tr>
    <tr><td>Web</td><td><a href="https://www.investarbank.com">www.investarbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.07 |
| 2020 | 0.25 |
| 2019 | 0.228 |
| 2018 | 0.202 |
| 2017 | 0.072 |
| 2016 | 0.042 |
| 2015 | 0.032 |
| 2014 | 0.014 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
