---
title: "SUMMIT FINANCIAL GROUP (SMMF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SUMMIT FINANCIAL GROUP</td></tr>
    <tr><td>Symbol</td><td>SMMF</td></tr>
    <tr><td>Web</td><td><a href="https://www.summitfgi.com">www.summitfgi.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.17 |
| 2020 | 0.68 |
| 2019 | 0.59 |
| 2018 | 0.53 |
| 2017 | 0.44 |
| 2016 | 0.4 |
| 2015 | 0.32 |
| 2009 | 0.06 |
| 2008 | 0.36 |
| 2007 | 0.34 |
| 2006 | 0.32 |
| 2005 | 0.3 |
| 2004 | 0.52 |
| 2003 | 0.43 |
| 2002 | 0.75 |
| 2001 | 1.05 |
| 2000 | 1.2 |
| 1999 | 0.48 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
