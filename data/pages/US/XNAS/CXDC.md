---
title: "CHINA XD PLASTICS COMPANY LIMITED (CXDC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CHINA XD PLASTICS COMPANY LIMITED</td></tr>
    <tr><td>Symbol</td><td>CXDC</td></tr>
    <tr><td>Web</td><td><a href="https://www.chinaxd.net">www.chinaxd.net</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
