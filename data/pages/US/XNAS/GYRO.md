---
title: "GYRODYNE LLC (GYRO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GYRODYNE LLC</td></tr>
    <tr><td>Symbol</td><td>GYRO</td></tr>
    <tr><td>Web</td><td><a href="https://www.gyrodyne.com">www.gyrodyne.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 1.0 |
| 2016 | 10.75 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
