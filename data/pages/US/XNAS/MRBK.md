---
title: "MERIDIAN CORPORATION (MRBK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MERIDIAN CORPORATION</td></tr>
    <tr><td>Symbol</td><td>MRBK</td></tr>
    <tr><td>Web</td><td><a href="https://www.meridianbanker.com">www.meridianbanker.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.25 |
| 2020 | 0.25 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
