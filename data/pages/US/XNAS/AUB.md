---
title: "ATLANTIC UNION BANKSHARES CORP (AUB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ATLANTIC UNION BANKSHARES CORP</td></tr>
    <tr><td>Symbol</td><td>AUB</td></tr>
    <tr><td>Web</td><td><a href="https://www.atlanticunionbank.com">www.atlanticunionbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.53 |
| 2020 | 1.0 |
| 2019 | 0.96 |
| 2018 | 0.88 |
| 2017 | 0.81 |
| 2016 | 0.77 |
| 2015 | 0.68 |
| 2014 | 0.58 |
| 2013 | 0.54 |
| 2012 | 0.37 |
| 2011 | 0.28 |
| 2010 | 0.25 |
| 2009 | 0.3 |
| 2008 | 0.74 |
| 2007 | 0.725 |
| 2006 | 0.86 |
| 2005 | 0.77 |
| 2004 | 0.68 |
| 2003 | 0.6 |
| 2002 | 0.52 |
| 2001 | 0.46 |
| 2000 | 0.4 |
| 1999 | 0.4 |
| 1998 | 0.57 |
| 1997 | 0.74 |
| 1996 | 0.64 |
| 1995 | 0.56 |
| 1994 | 0.526 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
