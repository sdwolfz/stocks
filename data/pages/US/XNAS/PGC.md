---
title: "PEAPACK GLADSTONE FINANCIAL CORP (PGC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PEAPACK GLADSTONE FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>PGC</td></tr>
    <tr><td>Web</td><td><a href="https://www.pgbank.com">www.pgbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.2 |
| 2019 | 0.2 |
| 2018 | 0.2 |
| 2017 | 0.2 |
| 2016 | 0.2 |
| 2015 | 0.2 |
| 2014 | 0.2 |
| 2013 | 0.2 |
| 2012 | 0.2 |
| 2011 | 0.2 |
| 2010 | 0.2 |
| 2009 | 0.37 |
| 2008 | 0.64 |
| 2007 | 0.46 |
| 2006 | 0.58 |
| 2005 | 0.5 |
| 2004 | 0.31 |
| 2003 | 0.28 |
| 2002 | 0.48 |
| 2001 | 0.57 |
| 2000 | 0.194 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
