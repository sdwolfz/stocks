---
title: "JOHNSON OUTDOORS INC (JOUT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>JOHNSON OUTDOORS INC</td></tr>
    <tr><td>Symbol</td><td>JOUT</td></tr>
    <tr><td>Web</td><td><a href="https://www.johnsonoutdoors.com">www.johnsonoutdoors.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.42 |
| 2020 | 0.72 |
| 2019 | 0.59 |
| 2018 | 0.48 |
| 2017 | 0.37 |
| 2016 | 0.33 |
| 2015 | 0.305 |
| 2014 | 0.3 |
| 2013 | 0.075 |
| 2008 | 0.22 |
| 2007 | 0.11 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
