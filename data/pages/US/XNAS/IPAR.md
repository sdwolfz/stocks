---
title: "INTER PARFUMS INC (IPAR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>INTER PARFUMS INC</td></tr>
    <tr><td>Symbol</td><td>IPAR</td></tr>
    <tr><td>Web</td><td><a href="https://www.interparfumsinc.com">www.interparfumsinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.25 |
| 2020 | 0.33 |
| 2019 | 1.155 |
| 2018 | 0.905 |
| 2017 | 0.72 |
| 2016 | 0.62 |
| 2015 | 0.52 |
| 2014 | 0.48 |
| 2013 | 0.96 |
| 2012 | 0.32 |
| 2011 | 0.32 |
| 2010 | 0.26 |
| 2009 | 0.132 |
| 2008 | 0.149 |
| 2007 | 0.2 |
| 2006 | 0.16 |
| 2005 | 0.16 |
| 2004 | 0.12 |
| 2003 | 0.08 |
| 2002 | 0.06 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
