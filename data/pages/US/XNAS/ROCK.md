---
title: "GIBRALTAR INDUSTRIES INC (ROCK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GIBRALTAR INDUSTRIES INC</td></tr>
    <tr><td>Symbol</td><td>ROCK</td></tr>
    <tr><td>Web</td><td><a href="https://www.gibraltar1.com">www.gibraltar1.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2008 | 0.2 |
| 2007 | 0.25 |
| 2006 | 0.15 |
| 2005 | 0.2 |
| 2004 | 0.195 |
| 2003 | 0.175 |
| 2002 | 0.155 |
| 2001 | 0.135 |
| 2000 | 0.115 |
| 1999 | 0.125 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
