---
title: " (ACGLO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ACGLO</td></tr>
    <tr><td>Web</td><td><a href="https://www.archcapgroup.com">www.archcapgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.341 |
| 2020 | 1.363 |
| 2019 | 1.363 |
| 2018 | 1.363 |
| 2017 | 0.507 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
