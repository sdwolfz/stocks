---
title: "SILICOM (SILC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SILICOM</td></tr>
    <tr><td>Symbol</td><td>SILC</td></tr>
    <tr><td>Web</td><td><a href="https://www.silicom.co.il">www.silicom.co.il</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 1.0 |
| 2016 | 1.0 |
| 2015 | 1.0 |
| 2014 | 1.0 |
| 2013 | 0.55 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
