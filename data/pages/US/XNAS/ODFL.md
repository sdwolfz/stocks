---
title: "OLD DOMINION FREIGHT LINE INC (ODFL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>OLD DOMINION FREIGHT LINE INC</td></tr>
    <tr><td>Symbol</td><td>ODFL</td></tr>
    <tr><td>Web</td><td><a href="https://www.odfl.com">www.odfl.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.68 |
| 2019 | 0.68 |
| 2018 | 0.52 |
| 2017 | 0.4 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
