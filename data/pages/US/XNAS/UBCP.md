---
title: "UNITED BANCORP INC OHIO (UBCP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>UNITED BANCORP INC OHIO</td></tr>
    <tr><td>Symbol</td><td>UBCP</td></tr>
    <tr><td>Web</td><td><a href="https://www.unitedbancorp.com">www.unitedbancorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.387 |
| 2020 | 0.57 |
| 2019 | 0.545 |
| 2018 | 0.57 |
| 2017 | 0.51 |
| 2016 | 0.47 |
| 2015 | 0.42 |
| 2014 | 0.33 |
| 2013 | 0.29 |
| 2012 | 0.42 |
| 2011 | 0.56 |
| 2010 | 0.56 |
| 2009 | 0.56 |
| 2008 | 0.54 |
| 2007 | 0.52 |
| 2006 | 0.39 |
| 2005 | 0.39 |
| 2004 | 0.39 |
| 2003 | 0.39 |
| 2002 | 0.39 |
| 2001 | 0.39 |
| 2000 | 0.39 |
| 1999 | 0.39 |
| 1998 | 0.36 |
| 1997 | 0.34 |
| 1996 | 0.33 |
| 1995 | 0.42 |
| 1994 | 0.24 |
| 1993 | 0.38 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
