---
title: "HOMETRUST BANCSHARES INC (HTBI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HOMETRUST BANCSHARES INC</td></tr>
    <tr><td>Symbol</td><td>HTBI</td></tr>
    <tr><td>Web</td><td><a href="https://www.htb.com">www.htb.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.16 |
| 2020 | 0.29 |
| 2019 | 0.25 |
| 2018 | 0.06 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
