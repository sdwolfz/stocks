---
title: "INGLES MARKET INC (IMKTA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>INGLES MARKET INC</td></tr>
    <tr><td>Symbol</td><td>IMKTA</td></tr>
    <tr><td>Web</td><td><a href="https://www.ingles-markets.com">www.ingles-markets.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.33 |
| 2020 | 0.66 |
| 2019 | 0.66 |
| 2018 | 0.66 |
| 2017 | 0.66 |
| 2016 | 0.66 |
| 2015 | 0.66 |
| 2014 | 0.66 |
| 2013 | 0.495 |
| 2012 | 1.485 |
| 2011 | 0.66 |
| 2010 | 0.66 |
| 2009 | 0.66 |
| 2008 | 0.66 |
| 2007 | 0.66 |
| 2006 | 0.66 |
| 2005 | 0.495 |
| 2004 | 0.66 |
| 2003 | 0.825 |
| 2002 | 0.66 |
| 2001 | 0.66 |
| 2000 | 0.66 |
| 1999 | 0.66 |
| 1998 | 0.495 |
| 1997 | 0.495 |
| 1996 | 0.825 |
| 1995 | 0.495 |
| 1994 | 0.66 |
| 1993 | 0.385 |
| 1992 | 0.22 |
| 1991 | 0.22 |
| 1990 | 0.275 |
| 1989 | 0.165 |
| 1988 | 0.055 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
