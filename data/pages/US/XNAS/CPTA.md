---
title: " (CPTA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CPTA</td></tr>
    <tr><td>Web</td><td><a href="https://www.capitalagroup.com">www.capitalagroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.25 |
| 2019 | 1.0 |
| 2018 | 1.0 |
| 2017 | 1.42 |
| 2016 | 1.8 |
| 2015 | 2.38 |
| 2014 | 1.88 |
| 2013 | 0.47 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
