---
title: "ITURAN LOCATION & CONTROL (ITRN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ITURAN LOCATION & CONTROL</td></tr>
    <tr><td>Symbol</td><td>ITRN</td></tr>
    <tr><td>Web</td><td><a href="https://www.ituran.com">www.ituran.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.48 |
| 2020 | 0.24 |
| 2019 | 0.95 |
| 2018 | 0.95 |
| 2017 | 1.12 |
| 2016 | 0.86 |
| 2015 | 0.78 |
| 2014 | 0.98 |
| 2013 | 0.81 |
| 2012 | 1.71 |
| 2011 | 1.0 |
| 2010 | 1.5 |
| 2009 | 0.17 |
| 2008 | 1.34 |
| 2007 | 0.205 |
| 2006 | 0.16 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
