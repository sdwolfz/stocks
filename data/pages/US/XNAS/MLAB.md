---
title: "MESA LABORATORIES INC (MLAB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MESA LABORATORIES INC</td></tr>
    <tr><td>Symbol</td><td>MLAB</td></tr>
    <tr><td>Web</td><td><a href="https://www.mesalabs.com">www.mesalabs.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.32 |
| 2020 | 0.64 |
| 2019 | 0.64 |
| 2018 | 0.64 |
| 2017 | 0.64 |
| 2016 | 0.64 |
| 2015 | 0.64 |
| 2014 | 0.61 |
| 2013 | 0.57 |
| 2012 | 0.53 |
| 2011 | 0.49 |
| 2010 | 0.45 |
| 2009 | 0.41 |
| 2008 | 0.4 |
| 2007 | 0.34 |
| 2006 | 0.39 |
| 2005 | 0.5 |
| 2004 | 0.41 |
| 2003 | 0.2 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
