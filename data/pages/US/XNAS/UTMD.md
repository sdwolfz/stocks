---
title: "UTAH MEDICAL PRODUCTS (UTMD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>UTAH MEDICAL PRODUCTS</td></tr>
    <tr><td>Symbol</td><td>UTMD</td></tr>
    <tr><td>Web</td><td><a href="https://www.utahmed.com">www.utahmed.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.285 |
| 2020 | 1.125 |
| 2019 | 1.105 |
| 2018 | 1.085 |
| 2017 | 1.065 |
| 2016 | 1.045 |
| 2015 | 1.025 |
| 2014 | 1.005 |
| 2013 | 0.985 |
| 2012 | 0.965 |
| 2011 | 0.945 |
| 2010 | 1.665 |
| 2009 | 0.925 |
| 2008 | 0.905 |
| 2007 | 0.885 |
| 2006 | 0.78 |
| 2005 | 0.63 |
| 2004 | 0.45 |
| 1993 | 0.06 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
