---
title: "LIFEWAY FOODS INC (LWAY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>LIFEWAY FOODS INC</td></tr>
    <tr><td>Symbol</td><td>LWAY</td></tr>
    <tr><td>Web</td><td><a href="https://www.lifewayfoods.com">www.lifewayfoods.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2013 | 0.08 |
| 2012 | 0.07 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
