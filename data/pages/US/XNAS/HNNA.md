---
title: "HENNESSY ADVISORS INC (HNNA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HENNESSY ADVISORS INC</td></tr>
    <tr><td>Symbol</td><td>HNNA</td></tr>
    <tr><td>Web</td><td><a href="https://www.hennessyadvisors.com">www.hennessyadvisors.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.138 |
| 2020 | 0.55 |
| 2019 | 0.495 |
| 2018 | 0.41 |
| 2017 | 0.337 |
| 2016 | 0.34 |
| 2015 | 0.24 |
| 2014 | 0.17 |
| 2013 | 0.125 |
| 2012 | 0.094 |
| 2011 | 0.1 |
| 2010 | 0.18 |
| 2009 | 0.09 |
| 2008 | 0.09 |
| 2007 | 0.12 |
| 2006 | 0.142 |
| 2005 | 0.167 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
