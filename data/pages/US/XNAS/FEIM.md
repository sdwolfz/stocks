---
title: "FREQUENCY ELECTRONICS INC (FEIM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FREQUENCY ELECTRONICS INC</td></tr>
    <tr><td>Symbol</td><td>FEIM</td></tr>
    <tr><td>Web</td><td><a href="https://www.frequencyelectronics.com">www.frequencyelectronics.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2012 | 0.2 |
| 2007 | 0.2 |
| 2006 | 0.2 |
| 2005 | 0.2 |
| 2004 | 0.2 |
| 2003 | 0.2 |
| 2002 | 0.2 |
| 2001 | 0.2 |
| 2000 | 0.2 |
| 1999 | 0.1 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
