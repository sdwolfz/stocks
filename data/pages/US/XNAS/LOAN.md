---
title: " (LOAN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>LOAN</td></tr>
    <tr><td>Web</td><td><a href="https://www.manhattanbridgecapital.com">www.manhattanbridgecapital.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.235 |
| 2020 | 0.42 |
| 2019 | 0.48 |
| 2018 | 0.48 |
| 2017 | 0.415 |
| 2016 | 0.375 |
| 2015 | 0.405 |
| 2014 | 0.17 |
| 2013 | 0.03 |
| 2005 | 0.4 |
| 2004 | 0.28 |
| 2003 | 0.25 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
