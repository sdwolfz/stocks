---
title: "SIYATA MOBILE INC (SYTA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SIYATA MOBILE INC</td></tr>
    <tr><td>Symbol</td><td>SYTA</td></tr>
    <tr><td>Web</td><td><a href="https://www.siyatamobile.com">www.siyatamobile.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
