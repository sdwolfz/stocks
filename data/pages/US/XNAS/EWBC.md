---
title: "EAST WEST BANCORP INC (EWBC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>EAST WEST BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>EWBC</td></tr>
    <tr><td>Web</td><td><a href="https://www.eastwestbank.com">www.eastwestbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.66 |
| 2020 | 1.1 |
| 2019 | 1.055 |
| 2018 | 0.86 |
| 2017 | 0.8 |
| 2016 | 0.8 |
| 2015 | 0.8 |
| 2014 | 0.72 |
| 2013 | 0.6 |
| 2012 | 0.4 |
| 2011 | 0.16 |
| 2010 | 0.04 |
| 2009 | 0.05 |
| 2008 | 0.4 |
| 2007 | 0.4 |
| 2006 | 0.2 |
| 2005 | 0.2 |
| 2004 | 0.3 |
| 2003 | 0.4 |
| 2002 | 0.27 |
| 2001 | 0.12 |
| 2000 | 0.12 |
| 1999 | 0.12 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
