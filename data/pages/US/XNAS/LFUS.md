---
title: "LITTELFUSE INC (LFUS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>LITTELFUSE INC</td></tr>
    <tr><td>Symbol</td><td>LFUS</td></tr>
    <tr><td>Web</td><td><a href="https://www.littelfuse.com">www.littelfuse.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.96 |
| 2020 | 1.92 |
| 2019 | 1.82 |
| 2018 | 1.6 |
| 2017 | 1.4 |
| 2016 | 1.24 |
| 2015 | 1.08 |
| 2014 | 0.94 |
| 2013 | 0.84 |
| 2012 | 0.76 |
| 2011 | 0.66 |
| 2010 | 0.15 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
