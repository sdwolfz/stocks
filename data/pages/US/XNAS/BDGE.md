---
title: "BRIDGE BANCORP INC (BDGE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BRIDGE BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>BDGE</td></tr>
    <tr><td>Web</td><td><a href="http://www.bnbbank.com">www.bnbbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.96 |
| 2019 | 0.92 |
| 2018 | 0.92 |
| 2017 | 0.92 |
| 2016 | 0.92 |
| 2015 | 0.92 |
| 2014 | 0.92 |
| 2013 | 0.69 |
| 2012 | 1.15 |
| 2011 | 0.69 |
| 2010 | 0.92 |
| 2009 | 0.92 |
| 2008 | 0.92 |
| 2007 | 0.92 |
| 2006 | 0.92 |
| 2005 | 0.91 |
| 2004 | 0.88 |
| 2003 | 1.18 |
| 2002 | 0.61 |
| 2001 | 0.55 |
| 2000 | 0.49 |
| 1999 | 0.42 |
| 1998 | 0.25 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
