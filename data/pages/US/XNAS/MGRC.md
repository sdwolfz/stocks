---
title: "MCGRATH RENTCORP (MGRC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MCGRATH RENTCORP</td></tr>
    <tr><td>Symbol</td><td>MGRC</td></tr>
    <tr><td>Web</td><td><a href="https://www.mgrc.com">www.mgrc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.855 |
| 2020 | 1.635 |
| 2019 | 1.465 |
| 2018 | 1.28 |
| 2017 | 1.035 |
| 2016 | 1.015 |
| 2015 | 0.995 |
| 2014 | 0.975 |
| 2013 | 0.955 |
| 2012 | 0.935 |
| 2011 | 0.915 |
| 2010 | 0.895 |
| 2009 | 0.86 |
| 2008 | 0.78 |
| 2007 | 0.7 |
| 2006 | 0.62 |
| 2005 | 0.64 |
| 2004 | 0.86 |
| 2003 | 0.78 |
| 2002 | 0.68 |
| 2001 | 0.62 |
| 2000 | 0.54 |
| 1999 | 0.46 |
| 1998 | 0.38 |
| 1997 | 0.46 |
| 1996 | 0.54 |
| 1995 | 0.47 |
| 1994 | 0.43 |
| 1993 | 0.39 |
| 1992 | 0.35 |
| 1991 | 0.4 |
| 1990 | 0.48 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
