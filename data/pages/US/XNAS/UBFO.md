---
title: "UNITED SECURITY BANCSHARES(CAL) (UBFO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>UNITED SECURITY BANCSHARES(CAL)</td></tr>
    <tr><td>Symbol</td><td>UBFO</td></tr>
    <tr><td>Web</td><td><a href="https://www.unitedsecuritybank.com">www.unitedsecuritybank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.11 |
| 2020 | 0.44 |
| 2019 | 0.55 |
| 2018 | 0.35 |
| 2017 | 0.17 |
| 2008 | 0.385 |
| 2007 | 0.5 |
| 2006 | 0.64 |
| 2005 | 0.7 |
| 2004 | 0.625 |
| 2003 | 0.565 |
| 2002 | 0.505 |
| 2001 | 0.445 |
| 2000 | 0.36 |
| 1999 | 0.68 |
| 1998 | 0.18 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
