---
title: "ANTELOPE ENTERPRISE HOLDINGS LTD (AEHL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ANTELOPE ENTERPRISE HOLDINGS LTD</td></tr>
    <tr><td>Symbol</td><td>AEHL</td></tr>
    <tr><td>Web</td><td><a href="https://www.aehltd.com">www.aehltd.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2014 | 0.025 |
| 2013 | 0.2 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
