---
title: "MACKINAC FINANCIAL CORP (MFNC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MACKINAC FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>MFNC</td></tr>
    <tr><td>Web</td><td><a href="https://www.bankmbank.com">www.bankmbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.14 |
| 2020 | 0.56 |
| 2019 | 0.52 |
| 2018 | 0.48 |
| 2017 | 0.48 |
| 2016 | 0.4 |
| 2015 | 0.35 |
| 2014 | 0.225 |
| 2013 | 0.17 |
| 2012 | 0.04 |
| 2002 | 0.25 |
| 2001 | 0.4 |
| 2000 | 0.191 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
