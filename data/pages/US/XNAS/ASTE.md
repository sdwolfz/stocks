---
title: "ASTEC INDUSTRIES INC (ASTE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ASTEC INDUSTRIES INC</td></tr>
    <tr><td>Symbol</td><td>ASTE</td></tr>
    <tr><td>Web</td><td><a href="https://www.astecindustries.com">www.astecindustries.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.22 |
| 2020 | 0.44 |
| 2019 | 0.44 |
| 2018 | 0.42 |
| 2017 | 0.4 |
| 2016 | 0.4 |
| 2015 | 0.4 |
| 2014 | 0.4 |
| 2013 | 0.3 |
| 2012 | 1.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
