---
title: "USA TECHNOLOGIES INC (USAT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>USA TECHNOLOGIES INC</td></tr>
    <tr><td>Symbol</td><td>USAT</td></tr>
    <tr><td>Web</td><td><a href="http://www.usatech.com">www.usatech.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
