---
title: "GOOSEHEAD INS INC (GSHD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GOOSEHEAD INS INC</td></tr>
    <tr><td>Symbol</td><td>GSHD</td></tr>
    <tr><td>Web</td><td><a href="https://www.gooseheadinsurance.com">www.gooseheadinsurance.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.149 |
| 2019 | 0.413 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
