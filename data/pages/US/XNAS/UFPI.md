---
title: "UFP INDUSTRIES INC (UFPI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>UFP INDUSTRIES INC</td></tr>
    <tr><td>Symbol</td><td>UFPI</td></tr>
    <tr><td>Web</td><td><a href="https://www.ufpi.com">www.ufpi.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |
| 2020 | 0.5 |
| 2019 | 0.4 |
| 2018 | 0.36 |
| 2017 | 0.62 |
| 2016 | 0.87 |
| 2015 | 0.82 |
| 2014 | 0.61 |
| 2013 | 0.41 |
| 2012 | 0.4 |
| 2011 | 0.4 |
| 2010 | 0.4 |
| 2009 | 0.26 |
| 2008 | 0.12 |
| 2007 | 0.115 |
| 2006 | 0.11 |
| 2005 | 0.105 |
| 2004 | 0.1 |
| 2003 | 0.095 |
| 2002 | 0.09 |
| 2001 | 0.085 |
| 2000 | 0.08 |
| 1999 | 0.075 |
| 1998 | 0.07 |
| 1997 | 0.065 |
| 1996 | 0.06 |
| 1995 | 0.105 |
| 1994 | 0.05 |
| 1993 | 0.025 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
