---
title: "YRC WORLDWIDE INC (YRCW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>YRC WORLDWIDE INC</td></tr>
    <tr><td>Symbol</td><td>YRCW</td></tr>
    <tr><td>Web</td><td><a href="http://www.yrcw.com">www.yrcw.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2002 | 3.975 |
| 1995 | 0.47 |
| 1994 | 0.94 |
| 1993 | 0.94 |
| 1992 | 0.94 |
| 1991 | 0.94 |
| 1990 | 0.82 |
| 1989 | 0.73 |
| 1988 | 0.175 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
