---
title: "PREFERRED BK LOS ANGELES CALIFORNIA (PFBC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PREFERRED BK LOS ANGELES CALIFORNIA</td></tr>
    <tr><td>Symbol</td><td>PFBC</td></tr>
    <tr><td>Web</td><td><a href="https://www.preferredbank.com">www.preferredbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.68 |
| 2020 | 1.2 |
| 2019 | 1.2 |
| 2018 | 0.94 |
| 2017 | 0.76 |
| 2016 | 0.6 |
| 2015 | 0.46 |
| 2014 | 0.1 |
| 2009 | 0.08 |
| 2008 | 0.47 |
| 2007 | 0.765 |
| 2006 | 0.8 |
| 2005 | 0.64 |
| 2004 | 0.6 |
| 2003 | 0.24 |
| 2002 | 0.38 |
| 2001 | 0.38 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
