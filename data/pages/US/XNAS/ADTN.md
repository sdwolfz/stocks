---
title: "ADTRAN INC (ADTN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ADTRAN INC</td></tr>
    <tr><td>Symbol</td><td>ADTN</td></tr>
    <tr><td>Web</td><td><a href="https://www.adtran.com">www.adtran.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.18 |
| 2020 | 0.36 |
| 2019 | 0.36 |
| 2018 | 0.36 |
| 2017 | 0.36 |
| 2016 | 0.36 |
| 2015 | 0.36 |
| 2014 | 0.36 |
| 2013 | 0.36 |
| 2012 | 0.36 |
| 2011 | 0.36 |
| 2010 | 0.36 |
| 2009 | 0.36 |
| 2008 | 0.36 |
| 2007 | 0.36 |
| 2006 | 0.36 |
| 2005 | 0.34 |
| 2004 | 0.32 |
| 2003 | 2.3 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
