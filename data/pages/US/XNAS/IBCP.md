---
title: "INDEPENDENT BANK CORP MICHIGAN (IBCP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>INDEPENDENT BANK CORP MICHIGAN</td></tr>
    <tr><td>Symbol</td><td>IBCP</td></tr>
    <tr><td>Web</td><td><a href="https://www.independentbank.com">www.independentbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.42 |
| 2020 | 0.8 |
| 2019 | 0.72 |
| 2018 | 0.6 |
| 2017 | 0.42 |
| 2016 | 0.34 |
| 2015 | 0.26 |
| 2014 | 0.18 |
| 2009 | 0.03 |
| 2008 | 0.35 |
| 2007 | 0.83 |
| 2006 | 0.79 |
| 2005 | 0.74 |
| 2004 | 0.49 |
| 2003 | 0.46 |
| 2002 | 0.54 |
| 2001 | 0.47 |
| 2000 | 0.44 |
| 1999 | 0.28 |
| 1998 | 0.455 |
| 1997 | 0.89 |
| 1996 | 0.76 |
| 1995 | 0.48 |
| 1994 | 0.8 |
| 1993 | 0.6 |
| 1992 | 0.52 |
| 1991 | 0.46 |
| 1990 | 0.44 |
| 1989 | 1.815 |
| 1988 | 0.788 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
