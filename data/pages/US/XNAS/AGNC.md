---
title: " (AGNC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>AGNC</td></tr>
    <tr><td>Web</td><td><a href="https://www.agnc.com">www.agnc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.48 |
| 2020 | 1.56 |
| 2019 | 2.0 |
| 2018 | 2.16 |
| 2017 | 2.16 |
| 2016 | 2.3 |
| 2015 | 2.48 |
| 2014 | 2.61 |
| 2013 | 3.75 |
| 2012 | 5.0 |
| 2011 | 5.6 |
| 2010 | 5.6 |
| 2009 | 5.15 |
| 2008 | 2.51 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
