---
title: "WD-40 CO (WDFC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>WD-40 CO</td></tr>
    <tr><td>Symbol</td><td>WDFC</td></tr>
    <tr><td>Web</td><td><a href="https://www.wd40company.com">www.wd40company.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.39 |
| 2020 | 2.68 |
| 2019 | 2.44 |
| 2018 | 2.16 |
| 2017 | 1.96 |
| 2016 | 1.68 |
| 2015 | 1.14 |
| 2014 | 1.74 |
| 2013 | 1.24 |
| 2012 | 1.16 |
| 2011 | 1.08 |
| 2010 | 1.02 |
| 2009 | 1.0 |
| 2008 | 1.0 |
| 2007 | 1.0 |
| 2006 | 0.88 |
| 2005 | 0.86 |
| 2004 | 1.0 |
| 2003 | 0.8 |
| 2002 | 0.94 |
| 2001 | 1.13 |
| 2000 | 1.28 |
| 1999 | 1.28 |
| 1998 | 1.28 |
| 1997 | 2.18 |
| 1996 | 2.48 |
| 1995 | 2.44 |
| 1994 | 2.4 |
| 1993 | 2.2 |
| 1992 | 2.03 |
| 1991 | 2.02 |
| 1990 | 2.02 |
| 1989 | 1.93 |
| 1988 | 0.4 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
