---
title: "SURO CAPITAL CORP (SSSS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SURO CAPITAL CORP</td></tr>
    <tr><td>Symbol</td><td>SSSS</td></tr>
    <tr><td>Web</td><td><a href="https://www.surocap.com">www.surocap.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 3.0 |
| 2020 | 0.87 |
| 2019 | 0.32 |
| 2016 | 2.8 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
