---
title: "PACWEST BANCORP (PACW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PACWEST BANCORP</td></tr>
    <tr><td>Symbol</td><td>PACW</td></tr>
    <tr><td>Web</td><td><a href="https://www.pacwest.com">www.pacwest.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.5 |
| 2020 | 1.35 |
| 2019 | 2.4 |
| 2018 | 2.3 |
| 2017 | 2.0 |
| 2016 | 2.0 |
| 2015 | 2.0 |
| 2014 | 1.25 |
| 2013 | 1.0 |
| 2012 | 0.79 |
| 2011 | 0.21 |
| 2010 | 0.04 |
| 2009 | 0.35 |
| 2008 | 1.28 |
| 2007 | 1.28 |
| 2006 | 1.21 |
| 2005 | 0.97 |
| 2004 | 0.848 |
| 2003 | 0.675 |
| 2002 | 0.54 |
| 2001 | 0.36 |
| 2000 | 0.09 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
