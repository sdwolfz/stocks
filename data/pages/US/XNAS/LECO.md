---
title: "LINCOLN ELECTRIC HLDGS INC (LECO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>LINCOLN ELECTRIC HLDGS INC</td></tr>
    <tr><td>Symbol</td><td>LECO</td></tr>
    <tr><td>Web</td><td><a href="https://www.lincolnelectric.com">www.lincolnelectric.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.02 |
| 2020 | 1.98 |
| 2019 | 1.9 |
| 2018 | 1.64 |
| 2017 | 1.44 |
| 2016 | 1.31 |
| 2015 | 1.19 |
| 2014 | 0.98 |
| 2013 | 0.83 |
| 2012 | 0.71 |
| 2011 | 0.79 |
| 2010 | 1.15 |
| 2009 | 1.09 |
| 2008 | 1.02 |
| 2007 | 0.91 |
| 2006 | 0.79 |
| 2005 | 0.73 |
| 2004 | 0.69 |
| 2003 | 0.64 |
| 2002 | 0.61 |
| 2001 | 0.6 |
| 2000 | 0.57 |
| 1999 | 0.5 |
| 1998 | 0.52 |
| 1997 | 0.65 |
| 1996 | 0.48 |
| 1995 | 0.32 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
