---
title: "CONNECTONE BANCORP INC (CNOB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CONNECTONE BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>CNOB</td></tr>
    <tr><td>Web</td><td><a href="https://www.connectonebank.com">www.connectonebank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.36 |
| 2019 | 0.345 |
| 2018 | 0.3 |
| 2017 | 0.3 |
| 2016 | 0.3 |
| 2015 | 0.3 |
| 2014 | 0.3 |
| 2013 | 0.26 |
| 2012 | 0.17 |
| 2011 | 0.12 |
| 2010 | 0.12 |
| 2009 | 0.24 |
| 2008 | 0.36 |
| 2007 | 0.36 |
| 2006 | 0.36 |
| 2005 | 0.36 |
| 2004 | 0.27 |
| 2003 | 0.44 |
| 2002 | 0.66 |
| 2001 | 0.6 |
| 2000 | 0.75 |
| 1999 | 0.443 |
| 1998 | 0.633 |
| 1997 | 1.0 |
| 1996 | 0.4 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
