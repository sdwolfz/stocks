---
title: "PREMIER FINANCIAL BANCORP INC (PFBI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PREMIER FINANCIAL BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>PFBI</td></tr>
    <tr><td>Web</td><td><a href="https://www.premierbankinc.com">www.premierbankinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.15 |
| 2020 | 0.6 |
| 2019 | 0.6 |
| 2018 | 0.6 |
| 2017 | 0.6 |
| 2016 | 0.6 |
| 2015 | 0.56 |
| 2014 | 0.6 |
| 2013 | 0.44 |
| 2012 | 0.22 |
| 2010 | 0.22 |
| 2009 | 0.44 |
| 2008 | 0.43 |
| 2007 | 0.4 |
| 2006 | 0.1 |
| 2000 | 0.15 |
| 1999 | 0.6 |
| 1998 | 0.45 |
| 1997 | 0.55 |
| 1996 | 0.375 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
