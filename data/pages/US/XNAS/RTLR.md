---
title: "RATTLER MIDSTREAM LP (RTLR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>RATTLER MIDSTREAM LP</td></tr>
    <tr><td>Symbol</td><td>RTLR</td></tr>
    <tr><td>Web</td><td><a href="https://www.rattlermidstream.com">www.rattlermidstream.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.4 |
| 2020 | 1.07 |
| 2019 | 0.34 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
