---
title: "NATIONAL SECURITY GROUP INC (NSEC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NATIONAL SECURITY GROUP INC</td></tr>
    <tr><td>Symbol</td><td>NSEC</td></tr>
    <tr><td>Web</td><td><a href="https://www.nationalsecuritygroup.com">www.nationalsecuritygroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.12 |
| 2020 | 0.24 |
| 2019 | 0.21 |
| 2018 | 0.2 |
| 2017 | 0.2 |
| 2016 | 0.18 |
| 2015 | 0.16 |
| 2014 | 0.12 |
| 2013 | 0.1 |
| 2012 | 0.325 |
| 2011 | 0.55 |
| 2010 | 0.6 |
| 2009 | 0.6 |
| 2008 | 0.9 |
| 2007 | 0.9 |
| 2006 | 0.885 |
| 2005 | 0.865 |
| 2004 | 0.845 |
| 2003 | 0.825 |
| 2002 | 0.805 |
| 2001 | 0.61 |
| 2000 | 0.85 |
| 1999 | 0.81 |
| 1998 | 0.77 |
| 1997 | 0.7 |
| 1996 | 0.65 |
| 1995 | 0.61 |
| 1994 | 0.64 |
| 1993 | 0.78 |
| 1992 | 0.7 |
| 1991 | 0.67 |
| 1990 | 1.12 |
| 1989 | 1.01 |
| 1988 | 0.25 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
