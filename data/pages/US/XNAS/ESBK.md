---
title: "ELMIRA SVGS BK ELMIRA NY (ESBK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ELMIRA SVGS BK ELMIRA NY</td></tr>
    <tr><td>Symbol</td><td>ESBK</td></tr>
    <tr><td>Web</td><td><a href="https://www.elmirasavingsbank.com">www.elmirasavingsbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.15 |
| 2020 | 0.68 |
| 2019 | 0.92 |
| 2018 | 0.69 |
| 2017 | 0.92 |
| 2016 | 0.92 |
| 2015 | 0.92 |
| 2014 | 0.92 |
| 2013 | 0.69 |
| 2012 | 0.67 |
| 2011 | 0.6 |
| 2010 | 0.8 |
| 2009 | 0.8 |
| 2008 | 0.62 |
| 2007 | 0.65 |
| 2006 | 0.62 |
| 2005 | 0.59 |
| 2004 | 0.57 |
| 2003 | 0.57 |
| 2002 | 0.54 |
| 2001 | 0.52 |
| 2000 | 0.48 |
| 1999 | 0.48 |
| 1998 | 0.64 |
| 1997 | 0.48 |
| 1996 | 0.64 |
| 1995 | 0.64 |
| 1994 | 0.64 |
| 1993 | 0.44 |
| 1992 | 0.4 |
| 1991 | 0.4 |
| 1990 | 0.4 |
| 1989 | 0.4 |
| 1988 | 0.1 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
