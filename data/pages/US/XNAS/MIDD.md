---
title: "MIDDLEBY CORP (MIDD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MIDDLEBY CORP</td></tr>
    <tr><td>Symbol</td><td>MIDD</td></tr>
    <tr><td>Web</td><td><a href="https://www.middleby.com">www.middleby.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2004 | 0.4 |
| 2003 | 0.25 |
| 2000 | 0.1 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
