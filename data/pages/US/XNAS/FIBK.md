---
title: "FIRST INTERSTATE BANCSYSTEM INC (FIBK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIRST INTERSTATE BANCSYSTEM INC</td></tr>
    <tr><td>Symbol</td><td>FIBK</td></tr>
    <tr><td>Web</td><td><a href="https://www.fibk.com">www.fibk.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.82 |
| 2020 | 2.0 |
| 2019 | 1.24 |
| 2018 | 1.12 |
| 2017 | 0.96 |
| 2016 | 0.88 |
| 2015 | 0.8 |
| 2014 | 0.64 |
| 2013 | 0.41 |
| 2012 | 0.49 |
| 2011 | 0.457 |
| 2010 | 0.45 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
