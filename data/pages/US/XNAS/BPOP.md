---
title: "POPULAR INC (BPOP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>POPULAR INC</td></tr>
    <tr><td>Symbol</td><td>BPOP</td></tr>
    <tr><td>Web</td><td><a href="https://www.popular.com">www.popular.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.4 |
| 2020 | 1.6 |
| 2019 | 1.2 |
| 2018 | 1.0 |
| 2017 | 1.0 |
| 2016 | 0.6 |
| 2015 | 0.3 |
| 2009 | 0.02 |
| 2008 | 0.48 |
| 2007 | 0.64 |
| 2006 | 0.64 |
| 2005 | 0.64 |
| 2004 | 0.91 |
| 2003 | 1.01 |
| 2002 | 0.8 |
| 2001 | 0.76 |
| 2000 | 0.64 |
| 1999 | 0.6 |
| 1998 | 0.72 |
| 1997 | 0.8 |
| 1996 | 0.84 |
| 1995 | 1.15 |
| 1994 | 1.0 |
| 1993 | 0.9 |
| 1992 | 0.8 |
| 1991 | 0.8 |
| 1990 | 1.54 |
| 1989 | 1.55 |
| 1988 | 0.375 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
