---
title: "CAESARSTONE LTD (CSTE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CAESARSTONE LTD</td></tr>
    <tr><td>Symbol</td><td>CSTE</td></tr>
    <tr><td>Web</td><td><a href="https://www.caesarstone.com">www.caesarstone.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.14 |
| 2019 | 0.15 |
| 2018 | 0.59 |
| 2014 | 0.57 |
| 2013 | 0.58 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
