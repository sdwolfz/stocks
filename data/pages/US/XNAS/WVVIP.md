---
title: " (WVVIP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>WVVIP</td></tr>
    <tr><td>Web</td><td><a href="https://www.wvv.com">www.wvv.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.22 |
| 2019 | 0.22 |
| 2018 | 0.22 |
| 2017 | 0.22 |
| 2016 | 0.22 |
| 2015 | 0.055 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
