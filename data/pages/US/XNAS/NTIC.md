---
title: "NORTHERN TECHNOLOGIES INTL CORP (NTIC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NORTHERN TECHNOLOGIES INTL CORP</td></tr>
    <tr><td>Symbol</td><td>NTIC</td></tr>
    <tr><td>Web</td><td><a href="https://www.ntic.com">www.ntic.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.13 |
| 2020 | 0.065 |
| 2019 | 0.365 |
| 2018 | 0.42 |
| 2017 | 0.1 |
| 2004 | 0.07 |
| 2002 | 0.085 |
| 2000 | 0.17 |
| 1999 | 0.16 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
