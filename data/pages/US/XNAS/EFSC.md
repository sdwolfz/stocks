---
title: "ENTERPRISE FINANCIAL SERVICES CORP (EFSC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ENTERPRISE FINANCIAL SERVICES CORP</td></tr>
    <tr><td>Symbol</td><td>EFSC</td></tr>
    <tr><td>Web</td><td><a href="https://www.enterprisebank.com">www.enterprisebank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.36 |
| 2020 | 0.72 |
| 2019 | 0.62 |
| 2018 | 0.47 |
| 2017 | 0.44 |
| 2016 | 0.41 |
| 2015 | 0.263 |
| 2014 | 0.21 |
| 2013 | 0.21 |
| 2012 | 0.21 |
| 2011 | 0.21 |
| 2010 | 0.21 |
| 2009 | 0.21 |
| 2008 | 0.21 |
| 2007 | 0.21 |
| 2006 | 0.18 |
| 2005 | 0.14 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
