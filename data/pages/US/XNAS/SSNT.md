---
title: "SILVERSUN TECHNOLOGIES INC (SSNT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SILVERSUN TECHNOLOGIES INC</td></tr>
    <tr><td>Symbol</td><td>SSNT</td></tr>
    <tr><td>Web</td><td><a href="https://www.silversuntech.com">www.silversuntech.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.9 |
| 2019 | 0.05 |
| 2017 | 0.06 |
| 2016 | 0.057 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
