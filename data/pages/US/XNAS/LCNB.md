---
title: "LCNB CORP (LCNB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>LCNB CORP</td></tr>
    <tr><td>Symbol</td><td>LCNB</td></tr>
    <tr><td>Web</td><td><a href="https://www.lcnb.com">www.lcnb.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.19 |
| 2020 | 0.73 |
| 2019 | 0.69 |
| 2018 | 0.65 |
| 2017 | 0.64 |
| 2016 | 0.64 |
| 2015 | 0.64 |
| 2014 | 0.64 |
| 2013 | 0.64 |
| 2012 | 0.64 |
| 2011 | 0.64 |
| 2010 | 0.64 |
| 2009 | 0.64 |
| 2008 | 0.64 |
| 2007 | 0.775 |
| 2006 | 1.2 |
| 2005 | 1.16 |
| 2004 | 1.39 |
| 2003 | 2.125 |
| 2002 | 2.025 |
| 2001 | 1.85 |
| 2000 | 0.9 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
