---
title: "SEANERGY MARITIME HOLDINGS CORP (SHIP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SEANERGY MARITIME HOLDINGS CORP</td></tr>
    <tr><td>Symbol</td><td>SHIP</td></tr>
    <tr><td>Web</td><td><a href="https://www.seanergymaritime.com">www.seanergymaritime.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2008 | 0.184 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
