---
title: "STONECASTLE FINL CORP (BANX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>STONECASTLE FINL CORP</td></tr>
    <tr><td>Symbol</td><td>BANX</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.38 |
| 2020 | 1.57 |
| 2019 | 1.52 |
| 2018 | 1.66 |
| 2017 | 1.5 |
| 2016 | 1.46 |
| 2015 | 1.51 |
| 2014 | 2.0 |
| 2013 | 0.28 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
