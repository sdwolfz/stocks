---
title: "COUNTERPATH CORPORATION (CPAH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>COUNTERPATH CORPORATION</td></tr>
    <tr><td>Symbol</td><td>CPAH</td></tr>
    <tr><td>Web</td><td><a href="http://www.counterpath.com">www.counterpath.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
