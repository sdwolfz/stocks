---
title: "DIXIE GROUP INC (DXYN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>DIXIE GROUP INC</td></tr>
    <tr><td>Symbol</td><td>DXYN</td></tr>
    <tr><td>Web</td><td><a href="https://www.thedixiegroup.com">www.thedixiegroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 1998 | 0.15 |
| 1994 | 0.2 |
| 1993 | 0.2 |
| 1992 | 0.2 |
| 1991 | 0.42 |
| 1990 | 0.68 |
| 1989 | 0.68 |
| 1988 | 0.15 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
