---
title: " (CRESY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CRESY</td></tr>
    <tr><td>Web</td><td><a href="https://www.cresud.com.ar">www.cresud.com.ar</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 0.45 |
| 2013 | 0.31 |
| 2012 | 0.465 |
| 2011 | 0.652 |
| 2009 | 0.297 |
| 2008 | 0.104 |
| 2007 | 0.073 |
| 2006 | 0.071 |
| 2005 | 0.177 |
| 2004 | 0.057 |
| 2003 | 0.037 |
| 2001 | 0.304 |
| 2000 | 0.096 |
| 1999 | 1.153 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
