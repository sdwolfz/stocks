---
title: "YUNHONG CTI LTD (CTIB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>YUNHONG CTI LTD</td></tr>
    <tr><td>Symbol</td><td>CTIB</td></tr>
    <tr><td>Web</td><td><a href="https://www.ctiindustries.com">www.ctiindustries.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2011 | 0.05 |
| 2010 | 0.1 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
