---
title: "AXOGEN INC (AXGN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AXOGEN INC</td></tr>
    <tr><td>Symbol</td><td>AXGN</td></tr>
    <tr><td>Web</td><td><a href="https://www.axogeninc.com">www.axogeninc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2010 | 1.0 |
| 2005 | 0.06 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
