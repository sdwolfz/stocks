---
title: "FIFTH THIRD BANCORP (FITB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIFTH THIRD BANCORP</td></tr>
    <tr><td>Symbol</td><td>FITB</td></tr>
    <tr><td>Web</td><td><a href="https://www.53.com">www.53.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.27 |
| 2020 | 1.08 |
| 2019 | 0.94 |
| 2018 | 0.74 |
| 2017 | 0.6 |
| 2016 | 0.53 |
| 2015 | 0.52 |
| 2014 | 0.51 |
| 2013 | 0.47 |
| 2012 | 0.36 |
| 2011 | 0.28 |
| 2010 | 0.04 |
| 2009 | 0.04 |
| 2008 | 0.75 |
| 2007 | 1.7 |
| 2006 | 1.58 |
| 2005 | 1.46 |
| 2004 | 1.31 |
| 2003 | 1.13 |
| 2002 | 0.98 |
| 2001 | 0.83 |
| 2000 | 0.87 |
| 1999 | 0.88 |
| 1998 | 0.795 |
| 1997 | 1.06 |
| 1996 | 1.1 |
| 1995 | 1.44 |
| 1994 | 1.2 |
| 1993 | 1.02 |
| 1992 | 1.01 |
| 1991 | 1.17 |
| 1990 | 1.02 |
| 1989 | 1.35 |
| 1988 | 0.3 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
