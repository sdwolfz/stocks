---
title: "CAMTEK (CAMT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CAMTEK</td></tr>
    <tr><td>Symbol</td><td>CAMT</td></tr>
    <tr><td>Web</td><td><a href="https://www.camtek.com">www.camtek.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.17 |
| 2018 | 0.14 |
| 2017 | 0.14 |
| 2002 | 0.01 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
