---
title: "AMERIPRISE FINANCIAL INC (AMP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AMERIPRISE FINANCIAL INC</td></tr>
    <tr><td>Symbol</td><td>AMP</td></tr>
    <tr><td>Web</td><td><a href="https://www.ameriprise.com">www.ameriprise.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.17 |
| 2020 | 4.09 |
| 2019 | 3.81 |
| 2018 | 3.53 |
| 2017 | 3.24 |
| 2016 | 2.92 |
| 2015 | 2.59 |
| 2014 | 2.26 |
| 2013 | 1.04 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
