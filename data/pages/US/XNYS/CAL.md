---
title: "CALERES INC (CAL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CALERES INC</td></tr>
    <tr><td>Symbol</td><td>CAL</td></tr>
    <tr><td>Web</td><td><a href="https://www.caleres.com">www.caleres.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.07 |
| 2020 | 0.28 |
| 2019 | 0.28 |
| 2018 | 0.28 |
| 2017 | 0.35 |
| 2016 | 0.28 |
| 2015 | 0.28 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
