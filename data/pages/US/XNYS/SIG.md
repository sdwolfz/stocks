---
title: "SIGNET JEWELERS LTD (SIG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SIGNET JEWELERS LTD</td></tr>
    <tr><td>Symbol</td><td>SIG</td></tr>
    <tr><td>Web</td><td><a href="https://www.signetjewelers.com">www.signetjewelers.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.37 |
| 2019 | 1.48 |
| 2018 | 1.42 |
| 2017 | 1.19 |
| 2016 | 1.0 |
| 2015 | 1.24 |
| 2014 | 0.69 |
| 2013 | 0.3 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
