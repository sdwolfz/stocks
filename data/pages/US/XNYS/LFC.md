---
title: " (LFC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>LFC</td></tr>
    <tr><td>Web</td><td><a href="https://www.e-chinalife.com">www.e-chinalife.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.444 |
| 2019 | 0.084 |
| 2018 | 0.261 |
| 2017 | 0.137 |
| 2016 | 0.269 |
| 2015 | 0.637 |
| 2014 | 0.637 |
| 2013 | 0.306 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
