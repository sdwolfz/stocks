---
title: " (EPD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>EPD</td></tr>
    <tr><td>Web</td><td><a href="https://www.enterpriseproducts.com">www.enterpriseproducts.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.9 |
| 2020 | 1.78 |
| 2019 | 1.755 |
| 2018 | 1.716 |
| 2017 | 1.668 |
| 2016 | 1.59 |
| 2015 | 1.51 |
| 2014 | 2.135 |
| 2013 | 1.37 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
