---
title: "BLACK HILLS CORP (BKH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BLACK HILLS CORP</td></tr>
    <tr><td>Symbol</td><td>BKH</td></tr>
    <tr><td>Web</td><td><a href="https://www.blackhillscorp.com">www.blackhillscorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.13 |
| 2020 | 2.17 |
| 2019 | 2.05 |
| 2018 | 1.93 |
| 2017 | 1.81 |
| 2016 | 1.68 |
| 2015 | 1.62 |
| 2014 | 1.56 |
| 2013 | 0.76 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
