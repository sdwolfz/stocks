---
title: "GREENBRIER COMPANIES INC. (GBX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GREENBRIER COMPANIES INC.</td></tr>
    <tr><td>Symbol</td><td>GBX</td></tr>
    <tr><td>Web</td><td><a href="https://www.gbrx.com">www.gbrx.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.54 |
| 2020 | 1.08 |
| 2019 | 1.0 |
| 2018 | 0.98 |
| 2017 | 0.88 |
| 2016 | 1.02 |
| 2015 | 0.65 |
| 2014 | 0.3 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
