---
title: "CONAGRA BRANDS INC (CAG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CONAGRA BRANDS INC</td></tr>
    <tr><td>Symbol</td><td>CAG</td></tr>
    <tr><td>Web</td><td><a href="https://www.conagrabrands.com">www.conagrabrands.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.55 |
| 2020 | 0.914 |
| 2019 | 0.852 |
| 2018 | 0.852 |
| 2017 | 0.826 |
| 2016 | 1.0 |
| 2015 | 1.0 |
| 2014 | 1.0 |
| 2013 | 0.5 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
