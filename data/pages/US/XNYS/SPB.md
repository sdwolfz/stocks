---
title: "SPECTRUM BRANDS HOLDINGS INC (SPB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SPECTRUM BRANDS HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>SPB</td></tr>
    <tr><td>Web</td><td><a href="https://www.spectrumbrands.com">www.spectrumbrands.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.84 |
| 2020 | 1.68 |
| 2019 | 1.68 |
| 2018 | 1.68 |
| 2017 | 1.68 |
| 2016 | 1.52 |
| 2015 | 1.32 |
| 2014 | 1.2 |
| 2013 | 0.5 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
