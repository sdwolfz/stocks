---
title: "BUCKLE INC (BKE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BUCKLE INC</td></tr>
    <tr><td>Symbol</td><td>BKE</td></tr>
    <tr><td>Web</td><td><a href="https://www.buckle.com">www.buckle.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.33 |
| 2020 | 4.15 |
| 2019 | 2.0 |
| 2018 | 2.75 |
| 2017 | 2.0 |
| 2016 | 2.0 |
| 2015 | 0.92 |
| 2014 | 0.88 |
| 2013 | 0.4 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
