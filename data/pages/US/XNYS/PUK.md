---
title: " (PUK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>PUK</td></tr>
    <tr><td>Web</td><td><a href="https://www.prudentialplc.com">www.prudentialplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.215 |
| 2020 | 0.519 |
| 2019 | 6.472 |
| 2018 | 1.283 |
| 2017 | 1.184 |
| 2016 | 1.393 |
| 2015 | 1.18 |
| 2014 | 1.168 |
| 2013 | 0.311 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
