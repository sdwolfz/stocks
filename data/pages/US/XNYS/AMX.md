---
title: " (AMX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>AMX</td></tr>
    <tr><td>Web</td><td><a href="https://www.americamovil.com">www.americamovil.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.355 |
| 2019 | 0.368 |
| 2018 | 0.327 |
| 2017 | 0.17 |
| 2016 | 0.285 |
| 2015 | 0.67 |
| 2014 | 0.361 |
| 2013 | 0.345 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
