---
title: "BANK OF HAWAII CORPORATION (BOH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BANK OF HAWAII CORPORATION</td></tr>
    <tr><td>Symbol</td><td>BOH</td></tr>
    <tr><td>Web</td><td><a href="https://www.boh.com">www.boh.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.34 |
| 2020 | 2.68 |
| 2019 | 2.59 |
| 2018 | 2.34 |
| 2017 | 2.04 |
| 2016 | 1.89 |
| 2015 | 1.8 |
| 2014 | 1.8 |
| 2013 | 0.9 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
