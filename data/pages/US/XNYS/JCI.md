---
title: "JOHNSON CONTROLS INTERNATIONAL PLC (JCI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>JOHNSON CONTROLS INTERNATIONAL PLC</td></tr>
    <tr><td>Symbol</td><td>JCI</td></tr>
    <tr><td>Web</td><td><a href="https://www.johnsoncontrols.com">www.johnsoncontrols.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.27 |
| 2020 | 1.04 |
| 2019 | 1.04 |
| 2018 | 1.04 |
| 2017 | 1.01 |
| 2016 | 1.312 |
| 2015 | 1.07 |
| 2014 | 0.92 |
| 2013 | 0.41 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
