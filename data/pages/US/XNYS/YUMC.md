---
title: "YUM CHINA HOLDINGS INC (YUMC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>YUM CHINA HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>YUMC</td></tr>
    <tr><td>Web</td><td><a href="https://www.yumchina.com">www.yumchina.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.24 |
| 2020 | 0.24 |
| 2019 | 0.48 |
| 2018 | 0.42 |
| 2017 | 0.1 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
