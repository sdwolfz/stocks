---
title: " (KKR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>KKR</td></tr>
    <tr><td>Web</td><td><a href="https://www.kkr.com">www.kkr.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.28 |
| 2020 | 0.53 |
| 2019 | 0.5 |
| 2018 | 0.635 |
| 2017 | 0.67 |
| 2016 | 0.64 |
| 2015 | 1.58 |
| 2014 | 2.03 |
| 2013 | 0.65 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
