---
title: "SCHWEITZER-MAUDUIT INTL INC (SWM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SCHWEITZER-MAUDUIT INTL INC</td></tr>
    <tr><td>Symbol</td><td>SWM</td></tr>
    <tr><td>Web</td><td><a href="https://www.swmintl.com">www.swmintl.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.88 |
| 2020 | 1.76 |
| 2019 | 1.76 |
| 2018 | 1.73 |
| 2017 | 2.11 |
| 2016 | 1.62 |
| 2015 | 1.54 |
| 2014 | 1.46 |
| 2013 | 0.66 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
