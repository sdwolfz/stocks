---
title: "DELEK US HOLDINGS INC (DK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DELEK US HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>DK</td></tr>
    <tr><td>Web</td><td><a href="https://www.delekus.com">www.delekus.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.93 |
| 2019 | 1.14 |
| 2018 | 0.96 |
| 2017 | 0.6 |
| 2016 | 0.6 |
| 2015 | 0.6 |
| 2014 | 0.6 |
| 2013 | 0.3 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
