---
title: "FACTSET RESEARCH SYSTEMS (FDS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FACTSET RESEARCH SYSTEMS</td></tr>
    <tr><td>Symbol</td><td>FDS</td></tr>
    <tr><td>Web</td><td><a href="https://www.factset.com">www.factset.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.59 |
| 2020 | 3.03 |
| 2019 | 2.8 |
| 2018 | 2.48 |
| 2017 | 2.18 |
| 2016 | 1.94 |
| 2015 | 1.71 |
| 2014 | 1.52 |
| 2013 | 0.7 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
