---
title: "HAMILTON BEACH BRANDS HLDG CO (HBB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HAMILTON BEACH BRANDS HLDG CO</td></tr>
    <tr><td>Symbol</td><td>HBB</td></tr>
    <tr><td>Web</td><td><a href="https://www.hamiltonbeachbrands.com">www.hamiltonbeachbrands.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.095 |
| 2020 | 0.37 |
| 2019 | 0.355 |
| 2018 | 0.34 |
| 2017 | 0.085 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
