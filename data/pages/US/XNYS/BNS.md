---
title: "BANK OF NOVA SCOTIA (BNS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BANK OF NOVA SCOTIA</td></tr>
    <tr><td>Symbol</td><td>BNS</td></tr>
    <tr><td>Web</td><td><a href="https://www.scotiabank.com">www.scotiabank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.707 |
| 2020 | 2.674 |
| 2019 | 2.64 |
| 2018 | 2.541 |
| 2017 | 2.391 |
| 2016 | 2.226 |
| 2015 | 2.919 |
| 2014 | 3.2 |
| 2013 | 1.22 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
