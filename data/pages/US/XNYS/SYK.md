---
title: "STRYKER CORP (SYK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>STRYKER CORP</td></tr>
    <tr><td>Symbol</td><td>SYK</td></tr>
    <tr><td>Web</td><td><a href="https://www.stryker.com">www.stryker.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.26 |
| 2020 | 2.355 |
| 2019 | 2.135 |
| 2018 | 1.93 |
| 2017 | 1.745 |
| 2016 | 1.565 |
| 2015 | 1.415 |
| 2014 | 1.26 |
| 2013 | 0.57 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
