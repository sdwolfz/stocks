---
title: " (SNP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SNP</td></tr>
    <tr><td>Web</td><td><a href="https://www.sinopec.com">www.sinopec.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.766 |
| 2020 | 4.209 |
| 2019 | 4.821 |
| 2018 | 7.717 |
| 2017 | 3.553 |
| 2016 | 1.861 |
| 2015 | 4.09 |
| 2014 | 3.467 |
| 2013 | 3.266 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
