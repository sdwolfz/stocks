---
title: "POSTAL REALTY TRUST INC (PSTL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>POSTAL REALTY TRUST INC</td></tr>
    <tr><td>Symbol</td><td>PSTL</td></tr>
    <tr><td>Web</td><td><a href="https://www.postalrealtytrust.com">www.postalrealtytrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.438 |
| 2020 | 0.79 |
| 2019 | 0.203 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
