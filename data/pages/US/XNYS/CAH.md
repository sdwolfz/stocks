---
title: "CARDINAL HEALTH INC (CAH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CARDINAL HEALTH INC</td></tr>
    <tr><td>Symbol</td><td>CAH</td></tr>
    <tr><td>Web</td><td><a href="https://www.cardinalhealth.com">www.cardinalhealth.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.977 |
| 2020 | 1.939 |
| 2019 | 1.919 |
| 2018 | 1.89 |
| 2017 | 1.835 |
| 2016 | 1.734 |
| 2015 | 1.504 |
| 2014 | 1.332 |
| 2013 | 0.606 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
