---
title: "PRUDENTIAL FINANCIAL INC (PRU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PRUDENTIAL FINANCIAL INC</td></tr>
    <tr><td>Symbol</td><td>PRU</td></tr>
    <tr><td>Web</td><td><a href="https://www.investor.prudential.com">www.investor.prudential.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.15 |
| 2020 | 4.4 |
| 2019 | 4.0 |
| 2018 | 3.6 |
| 2017 | 3.0 |
| 2016 | 2.8 |
| 2015 | 2.44 |
| 2014 | 2.17 |
| 2013 | 0.93 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
