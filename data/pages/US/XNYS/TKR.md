---
title: "TIMKEN CO (TKR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TIMKEN CO</td></tr>
    <tr><td>Symbol</td><td>TKR</td></tr>
    <tr><td>Web</td><td><a href="https://www.timken.com">www.timken.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.29 |
| 2020 | 1.13 |
| 2019 | 1.12 |
| 2018 | 1.11 |
| 2017 | 1.07 |
| 2016 | 1.04 |
| 2015 | 1.03 |
| 2014 | 1.0 |
| 2013 | 0.46 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
