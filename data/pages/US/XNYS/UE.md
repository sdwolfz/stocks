---
title: " (UE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>UE</td></tr>
    <tr><td>Web</td><td><a href="https://www.uedge.com">www.uedge.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |
| 2020 | 0.68 |
| 2019 | 0.88 |
| 2018 | 0.88 |
| 2017 | 0.88 |
| 2016 | 0.82 |
| 2015 | 0.8 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
