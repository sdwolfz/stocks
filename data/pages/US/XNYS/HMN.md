---
title: "HORACE MANN EDUCATORS CORP (HMN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HORACE MANN EDUCATORS CORP</td></tr>
    <tr><td>Symbol</td><td>HMN</td></tr>
    <tr><td>Web</td><td><a href="https://www.horacemann.com">www.horacemann.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.31 |
| 2020 | 1.2 |
| 2019 | 1.148 |
| 2018 | 1.14 |
| 2017 | 1.1 |
| 2016 | 1.06 |
| 2015 | 1.0 |
| 2014 | 0.92 |
| 2013 | 0.585 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
