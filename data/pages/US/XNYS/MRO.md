---
title: "MARATHON OIL CORPORATION (MRO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MARATHON OIL CORPORATION</td></tr>
    <tr><td>Symbol</td><td>MRO</td></tr>
    <tr><td>Web</td><td><a href="https://www.marathonoil.com">www.marathonoil.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.07 |
| 2020 | 0.08 |
| 2019 | 0.2 |
| 2018 | 0.2 |
| 2017 | 0.2 |
| 2016 | 0.2 |
| 2015 | 0.68 |
| 2014 | 0.8 |
| 2013 | 0.38 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
