---
title: " (TLK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>TLK</td></tr>
    <tr><td>Web</td><td><a href="https://www.telkom.co.id">www.telkom.co.id</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.798 |
| 2019 | 0.945 |
| 2018 | 0.978 |
| 2017 | 0.701 |
| 2016 | 0.67 |
| 2015 | 1.107 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
