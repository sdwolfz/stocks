---
title: "FIRST REPUBLIC BANK SAN FRANCISCO (FRC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FIRST REPUBLIC BANK SAN FRANCISCO</td></tr>
    <tr><td>Symbol</td><td>FRC</td></tr>
    <tr><td>Web</td><td><a href="https://www.firstrepublic.com">www.firstrepublic.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.42 |
| 2020 | 0.79 |
| 2019 | 0.75 |
| 2018 | 0.71 |
| 2017 | 0.67 |
| 2016 | 0.63 |
| 2015 | 0.59 |
| 2014 | 0.54 |
| 2013 | 0.24 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
