---
title: "ORMAT TECHNOLOGIES INC (ORA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ORMAT TECHNOLOGIES INC</td></tr>
    <tr><td>Symbol</td><td>ORA</td></tr>
    <tr><td>Web</td><td><a href="https://www.ormat.com">www.ormat.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.24 |
| 2020 | 0.44 |
| 2019 | 0.44 |
| 2018 | 0.53 |
| 2017 | 0.41 |
| 2016 | 0.52 |
| 2015 | 0.26 |
| 2014 | 0.21 |
| 2013 | 0.08 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
