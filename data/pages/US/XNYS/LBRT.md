---
title: "LIBERTY OILFIELD SERVICES INC (LBRT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>LIBERTY OILFIELD SERVICES INC</td></tr>
    <tr><td>Symbol</td><td>LBRT</td></tr>
    <tr><td>Web</td><td><a href="https://www.libertyfrac.com">www.libertyfrac.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.05 |
| 2019 | 0.2 |
| 2018 | 0.1 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
