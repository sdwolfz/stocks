---
title: "BAIN CAPITAL SPECIALTY FINANCE INC (BCSF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BAIN CAPITAL SPECIALTY FINANCE INC</td></tr>
    <tr><td>Symbol</td><td>BCSF</td></tr>
    <tr><td>Web</td><td><a href="https://www.baincapitalbdc.com">www.baincapitalbdc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.68 |
| 2020 | 1.43 |
| 2019 | 1.64 |
| 2018 | 0.41 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
