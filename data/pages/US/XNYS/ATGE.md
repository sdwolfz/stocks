---
title: "ADTALEM GLOBAL EDUCATION INC (ATGE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ADTALEM GLOBAL EDUCATION INC</td></tr>
    <tr><td>Symbol</td><td>ATGE</td></tr>
    <tr><td>Web</td><td><a href="https://www.adtalem.com">www.adtalem.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.18 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
