---
title: "LOCKHEED MARTIN CORP (LMT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>LOCKHEED MARTIN CORP</td></tr>
    <tr><td>Symbol</td><td>LMT</td></tr>
    <tr><td>Web</td><td><a href="https://www.lockheedmartin.com">www.lockheedmartin.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 5.2 |
| 2020 | 9.8 |
| 2019 | 9.0 |
| 2018 | 8.2 |
| 2017 | 9.46 |
| 2016 | 6.77 |
| 2015 | 6.15 |
| 2014 | 5.49 |
| 2013 | 2.48 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
