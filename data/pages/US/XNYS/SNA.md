---
title: "SNAP-ON INC (SNA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SNAP-ON INC</td></tr>
    <tr><td>Symbol</td><td>SNA</td></tr>
    <tr><td>Web</td><td><a href="https://www.snapon.com">www.snapon.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.46 |
| 2020 | 4.47 |
| 2019 | 3.93 |
| 2018 | 3.41 |
| 2017 | 2.95 |
| 2016 | 2.54 |
| 2015 | 2.2 |
| 2014 | 1.85 |
| 2013 | 0.82 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
