---
title: " (EC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>EC</td></tr>
    <tr><td>Web</td><td><a href="https://www.ecopetrol.com.co">www.ecopetrol.com.co</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.799 |
| 2019 | 1.782 |
| 2018 | 0.612 |
| 2017 | 0.156 |
| 2015 | 1.04 |
| 2014 | 0.33 |
| 2013 | 0.397 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
