---
title: "ARMSTRONG WORLD INDUSTRIES INC (AWI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ARMSTRONG WORLD INDUSTRIES INC</td></tr>
    <tr><td>Symbol</td><td>AWI</td></tr>
    <tr><td>Web</td><td><a href="https://www.armstrongceilings.com">www.armstrongceilings.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.42 |
| 2020 | 0.81 |
| 2019 | 0.725 |
| 2018 | 0.175 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
