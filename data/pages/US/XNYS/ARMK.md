---
title: "ARAMARK (ARMK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ARAMARK</td></tr>
    <tr><td>Symbol</td><td>ARMK</td></tr>
    <tr><td>Web</td><td><a href="https://www.aramark.com">www.aramark.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.11 |
| 2020 | 0.44 |
| 2019 | 0.44 |
| 2018 | 0.425 |
| 2017 | 0.414 |
| 2016 | 0.388 |
| 2015 | 0.353 |
| 2014 | 0.311 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
