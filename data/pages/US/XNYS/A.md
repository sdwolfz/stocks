---
title: "AGILENT TECHNOLOGIES INC (A)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AGILENT TECHNOLOGIES INC</td></tr>
    <tr><td>Symbol</td><td>A</td></tr>
    <tr><td>Web</td><td><a href="https://www.agilent.com">www.agilent.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.388 |
| 2020 | 0.54 |
| 2019 | 0.672 |
| 2018 | 0.611 |
| 2017 | 0.545 |
| 2016 | 0.477 |
| 2015 | 0.515 |
| 2014 | 0.396 |
| 2013 | 0.372 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
