---
title: "PITNEY BOWES INC (PBI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PITNEY BOWES INC</td></tr>
    <tr><td>Symbol</td><td>PBI</td></tr>
    <tr><td>Web</td><td><a href="https://www.pitneybowes.com">www.pitneybowes.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.2 |
| 2019 | 0.2 |
| 2018 | 0.752 |
| 2017 | 0.752 |
| 2016 | 0.752 |
| 2015 | 0.752 |
| 2014 | 0.752 |
| 2013 | 0.376 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
