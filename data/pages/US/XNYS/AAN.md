---
title: "THE AARONS COMPANY INC (AAN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>THE AARONS COMPANY INC</td></tr>
    <tr><td>Symbol</td><td>AAN</td></tr>
    <tr><td>Web</td><td><a href="https://www.aarons.com">www.aarons.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.165 |
| 2019 | 0.145 |
| 2018 | 0.125 |
| 2017 | 0.111 |
| 2016 | 0.102 |
| 2015 | 0.094 |
| 2014 | 0.086 |
| 2013 | 0.038 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
