---
title: "KEMPER CORP (KMPR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>KEMPER CORP</td></tr>
    <tr><td>Symbol</td><td>KMPR</td></tr>
    <tr><td>Web</td><td><a href="https://www.kemper.com">www.kemper.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.62 |
| 2020 | 1.2 |
| 2019 | 1.03 |
| 2018 | 0.96 |
| 2017 | 0.96 |
| 2016 | 0.96 |
| 2015 | 0.96 |
| 2014 | 0.96 |
| 2013 | 0.48 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
