---
title: "GEOPARK LIMITED (GPRK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GEOPARK LIMITED</td></tr>
    <tr><td>Symbol</td><td>GPRK</td></tr>
    <tr><td>Web</td><td><a href="https://www.geo-park.com">www.geo-park.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.042 |
| 2020 | 0.086 |
| 2019 | 0.041 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
