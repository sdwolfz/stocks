---
title: "CRAWFORD & CO (CRD.B)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CRAWFORD & CO</td></tr>
    <tr><td>Symbol</td><td>CRD.B</td></tr>
    <tr><td>Web</td><td><a href="https://www.crawco.com">www.crawco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.06 |
| 2020 | 0.17 |
| 2019 | 0.2 |
| 2018 | 0.2 |
| 2017 | 0.2 |
| 2016 | 0.2 |
| 2015 | 0.2 |
| 2014 | 0.18 |
| 2013 | 0.08 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
