---
title: "CIGNA CORPORATION (CI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CIGNA CORPORATION</td></tr>
    <tr><td>Symbol</td><td>CI</td></tr>
    <tr><td>Web</td><td><a href="https://www.cigna.com">www.cigna.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.0 |
| 2020 | 0.04 |
| 2019 | 0.04 |
| 2018 | 0.04 |
| 2017 | 0.04 |
| 2016 | 0.04 |
| 2015 | 0.04 |
| 2014 | 0.04 |
| 2013 | 0.04 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
