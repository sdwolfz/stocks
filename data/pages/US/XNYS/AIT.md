---
title: "APPLIED INDUSTRIAL TECHNOLOGIES INC (AIT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>APPLIED INDUSTRIAL TECHNOLOGIES INC</td></tr>
    <tr><td>Symbol</td><td>AIT</td></tr>
    <tr><td>Web</td><td><a href="https://www.applied.com">www.applied.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.66 |
| 2020 | 1.28 |
| 2019 | 1.24 |
| 2018 | 1.2 |
| 2017 | 1.16 |
| 2016 | 1.12 |
| 2015 | 1.08 |
| 2014 | 1.0 |
| 2013 | 0.46 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
