---
title: "KELLOGG COMPANY (K)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>KELLOGG COMPANY</td></tr>
    <tr><td>Symbol</td><td>K</td></tr>
    <tr><td>Web</td><td><a href="https://www.kelloggcompany.com">www.kelloggcompany.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.15 |
| 2020 | 2.28 |
| 2019 | 2.26 |
| 2018 | 2.2 |
| 2017 | 2.12 |
| 2016 | 2.04 |
| 2015 | 1.98 |
| 2014 | 1.9 |
| 2013 | 0.92 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
