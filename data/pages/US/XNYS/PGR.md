---
title: "PROGRESSIVE CORP(OHIO) (PGR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PROGRESSIVE CORP(OHIO)</td></tr>
    <tr><td>Symbol</td><td>PGR</td></tr>
    <tr><td>Web</td><td><a href="https://www.progressive.com">www.progressive.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 4.7 |
| 2020 | 2.65 |
| 2019 | 2.814 |
| 2018 | 1.125 |
| 2017 | 0.681 |
| 2016 | 0.888 |
| 2015 | 0.686 |
| 2013 | 0.285 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
