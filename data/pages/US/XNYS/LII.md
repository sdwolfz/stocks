---
title: "LENNOX INTERNATIONAL INC (LII)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>LENNOX INTERNATIONAL INC</td></tr>
    <tr><td>Symbol</td><td>LII</td></tr>
    <tr><td>Web</td><td><a href="https://www.lennoxinternational.com">www.lennoxinternational.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.77 |
| 2020 | 3.08 |
| 2019 | 2.95 |
| 2018 | 2.43 |
| 2017 | 1.96 |
| 2016 | 1.65 |
| 2015 | 1.38 |
| 2014 | 1.14 |
| 2013 | 0.72 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
