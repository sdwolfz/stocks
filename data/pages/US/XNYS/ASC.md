---
title: "ARDMORE SHIPPING CORP (ASC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ARDMORE SHIPPING CORP</td></tr>
    <tr><td>Symbol</td><td>ASC</td></tr>
    <tr><td>Web</td><td><a href="https://www.ardmoreshipping.com">www.ardmoreshipping.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.05 |
| 2016 | 0.53 |
| 2015 | 0.61 |
| 2014 | 0.4 |
| 2013 | 0.066 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
