---
title: " (MMP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>MMP</td></tr>
    <tr><td>Web</td><td><a href="https://www.magellanlp.com">www.magellanlp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.056 |
| 2020 | 4.112 |
| 2019 | 4.036 |
| 2018 | 3.793 |
| 2017 | 3.523 |
| 2016 | 3.245 |
| 2015 | 2.914 |
| 2014 | 2.506 |
| 2013 | 1.091 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
