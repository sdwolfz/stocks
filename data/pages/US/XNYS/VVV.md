---
title: "VALVOLINE INC (VVV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>VALVOLINE INC</td></tr>
    <tr><td>Symbol</td><td>VVV</td></tr>
    <tr><td>Web</td><td><a href="https://www.valvoline.com">www.valvoline.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.25 |
| 2020 | 0.464 |
| 2019 | 0.431 |
| 2018 | 0.331 |
| 2017 | 0.222 |
| 2016 | 0.049 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
