---
title: "TECK RESOURCES LIMITED (TECK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TECK RESOURCES LIMITED</td></tr>
    <tr><td>Symbol</td><td>TECK</td></tr>
    <tr><td>Web</td><td><a href="https://www.teck.com">www.teck.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.149 |
| 2019 | 0.151 |
| 2018 | 0.23 |
| 2017 | 0.463 |
| 2016 | 0.037 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
