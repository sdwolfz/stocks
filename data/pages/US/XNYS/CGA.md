---
title: "CHINA GREEN AGRICULTURE INC (CGA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CHINA GREEN AGRICULTURE INC</td></tr>
    <tr><td>Symbol</td><td>CGA</td></tr>
    <tr><td>Web</td><td><a href="https://www.cgagri.com">www.cgagri.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2014 | 0.1 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
