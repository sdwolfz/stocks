---
title: "V F CORP (VFC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>V F CORP</td></tr>
    <tr><td>Symbol</td><td>VFC</td></tr>
    <tr><td>Web</td><td><a href="https://www.vfc.com">www.vfc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.49 |
| 2020 | 1.93 |
| 2019 | 7.237 |
| 2018 | 1.89 |
| 2017 | 1.72 |
| 2016 | 1.53 |
| 2015 | 1.33 |
| 2014 | 1.106 |
| 2013 | 1.132 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
