---
title: "GRANITE CONSTRUCTION (GVA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GRANITE CONSTRUCTION</td></tr>
    <tr><td>Symbol</td><td>GVA</td></tr>
    <tr><td>Web</td><td><a href="https://www.graniteconstruction.com">www.graniteconstruction.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.13 |
| 2020 | 0.52 |
| 2019 | 0.52 |
| 2018 | 0.52 |
| 2017 | 0.52 |
| 2016 | 0.52 |
| 2015 | 0.52 |
| 2014 | 0.52 |
| 2013 | 0.39 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
