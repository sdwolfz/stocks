---
title: "MEREDITH CORP (MDP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MEREDITH CORP</td></tr>
    <tr><td>Symbol</td><td>MDP</td></tr>
    <tr><td>Web</td><td><a href="https://www.meredith.com">www.meredith.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.595 |
| 2019 | 2.3 |
| 2018 | 2.18 |
| 2017 | 2.08 |
| 2016 | 1.98 |
| 2015 | 1.832 |
| 2014 | 1.706 |
| 2013 | 0.814 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
