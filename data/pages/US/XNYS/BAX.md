---
title: "BAXTER INTERNATIONAL INC (BAX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BAXTER INTERNATIONAL INC</td></tr>
    <tr><td>Symbol</td><td>BAX</td></tr>
    <tr><td>Web</td><td><a href="https://www.baxter.com">www.baxter.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.525 |
| 2020 | 0.955 |
| 2019 | 0.85 |
| 2018 | 0.73 |
| 2017 | 0.61 |
| 2016 | 0.505 |
| 2015 | 1.27 |
| 2014 | 2.05 |
| 2013 | 0.98 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
