---
title: " (WBK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>WBK</td></tr>
    <tr><td>Web</td><td><a href="https://www.westpac.com.au">www.westpac.com.au</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.44 |
| 2020 | 0.225 |
| 2019 | 1.193 |
| 2018 | 1.351 |
| 2017 | 1.427 |
| 2016 | 2.08 |
| 2015 | 1.37 |
| 2014 | 1.587 |
| 2013 | 0.971 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
