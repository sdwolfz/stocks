---
title: " (BVN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BVN</td></tr>
    <tr><td>Web</td><td><a href="https://www.buenaventura.com">www.buenaventura.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.084 |
| 2018 | 0.087 |
| 2017 | 0.084 |
| 2016 | 0.029 |
| 2014 | 0.097 |
| 2013 | 0.298 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
