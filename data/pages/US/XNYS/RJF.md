---
title: "RAYMOND JAMES FINANCIAL INC (RJF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>RAYMOND JAMES FINANCIAL INC</td></tr>
    <tr><td>Symbol</td><td>RJF</td></tr>
    <tr><td>Web</td><td><a href="https://www.raymondjames.com">www.raymondjames.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.78 |
| 2020 | 1.11 |
| 2019 | 1.73 |
| 2018 | 1.1 |
| 2017 | 0.66 |
| 2016 | 0.82 |
| 2015 | 0.74 |
| 2014 | 0.66 |
| 2013 | 0.44 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
