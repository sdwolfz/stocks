---
title: " (PLD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>PLD</td></tr>
    <tr><td>Web</td><td><a href="https://www.prologis.com">www.prologis.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.26 |
| 2020 | 2.32 |
| 2019 | 2.12 |
| 2018 | 1.92 |
| 2017 | 1.76 |
| 2016 | 1.68 |
| 2015 | 1.52 |
| 2014 | 1.32 |
| 2013 | 0.84 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
