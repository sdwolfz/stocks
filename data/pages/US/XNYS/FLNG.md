---
title: "FLEX LNG LTD (BM) (FLNG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FLEX LNG LTD (BM)</td></tr>
    <tr><td>Symbol</td><td>FLNG</td></tr>
    <tr><td>Web</td><td><a href="https://www.flexlng.com">www.flexlng.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
