---
title: "FIRST COMM'WTH FINL CORP PA (FCF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FIRST COMM'WTH FINL CORP PA</td></tr>
    <tr><td>Symbol</td><td>FCF</td></tr>
    <tr><td>Web</td><td><a href="https://www.fcbanking.com">www.fcbanking.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.225 |
| 2020 | 0.44 |
| 2019 | 0.4 |
| 2018 | 0.35 |
| 2017 | 0.32 |
| 2016 | 0.28 |
| 2015 | 0.28 |
| 2014 | 0.28 |
| 2013 | 0.12 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
