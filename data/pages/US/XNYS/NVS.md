---
title: " (NVS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>NVS</td></tr>
    <tr><td>Web</td><td><a href="https://www.novartis.com">www.novartis.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.079 |
| 2020 | 2.007 |
| 2019 | 13.432 |
| 2018 | 1.909 |
| 2017 | 2.303 |
| 2016 | 2.303 |
| 2015 | 2.259 |
| 2014 | 1.794 |
| 2013 | 2.057 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
