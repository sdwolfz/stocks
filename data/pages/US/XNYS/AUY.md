---
title: "YAMANA GOLD INC (AUY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>YAMANA GOLD INC</td></tr>
    <tr><td>Symbol</td><td>AUY</td></tr>
    <tr><td>Web</td><td><a href="https://www.yamana.com">www.yamana.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.099 |
| 2019 | 0.03 |
| 2018 | 0.02 |
| 2017 | 0.02 |
| 2016 | 0.02 |
| 2015 | 0.06 |
| 2014 | 0.129 |
| 2013 | 0.13 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
