---
title: "BITE ACQUISITION CORP (BITE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BITE ACQUISITION CORP</td></tr>
    <tr><td>Symbol</td><td>BITE</td></tr>
    <tr><td>Web</td><td><a href="https://www.biteacquisitioncorp.com">www.biteacquisitioncorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.354 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
