---
title: "UNIFIRST CORP (UNF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>UNIFIRST CORP</td></tr>
    <tr><td>Symbol</td><td>UNF</td></tr>
    <tr><td>Web</td><td><a href="https://www.unifirst.com">www.unifirst.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.5 |
| 2020 | 1.0 |
| 2019 | 0.586 |
| 2018 | 0.374 |
| 2017 | 0.152 |
| 2016 | 0.152 |
| 2015 | 0.152 |
| 2014 | 0.152 |
| 2013 | 0.076 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
