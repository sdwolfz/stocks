---
title: "TEMPUR SEALY INTERNATIONAL INC (TPX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TEMPUR SEALY INTERNATIONAL INC</td></tr>
    <tr><td>Symbol</td><td>TPX</td></tr>
    <tr><td>Web</td><td><a href="https://www.tempursealy.com">www.tempursealy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.14 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
