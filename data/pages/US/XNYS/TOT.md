---
title: " (TOT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>TOT</td></tr>
    <tr><td>Web</td><td><a href="https://www.total.com">www.total.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.558 |
| 2020 | 3.072 |
| 2019 | 1.781 |
| 2018 | 2.592 |
| 2017 | 1.127 |
| 2016 | 2.272 |
| 2015 | 2.927 |
| 2014 | 2.668 |
| 2013 | 1.348 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
