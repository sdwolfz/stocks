---
title: "TRANSOCEAN LIMITED (RIG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TRANSOCEAN LIMITED</td></tr>
    <tr><td>Symbol</td><td>RIG</td></tr>
    <tr><td>Web</td><td><a href="https://www.deepwater.com">www.deepwater.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 1.05 |
| 2014 | 2.81 |
| 2013 | 1.12 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
