---
title: "FERRARI N V (RACE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FERRARI N V</td></tr>
    <tr><td>Symbol</td><td>RACE</td></tr>
    <tr><td>Web</td><td><a href="https://www.ferrari.com">www.ferrari.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 0.682 |
| 2016 | 0.515 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
