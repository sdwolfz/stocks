---
title: "TELUS CORP (TU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TELUS CORP</td></tr>
    <tr><td>Symbol</td><td>TU</td></tr>
    <tr><td>Web</td><td><a href="https://www.telus.com">www.telus.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.891 |
| 2019 | 1.692 |
| 2018 | 1.62 |
| 2017 | 1.518 |
| 2016 | 1.384 |
| 2015 | 1.47 |
| 2014 | 1.52 |
| 2013 | 0.7 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
