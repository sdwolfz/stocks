---
title: "REGIONAL MGMT CORP (RM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>REGIONAL MGMT CORP</td></tr>
    <tr><td>Symbol</td><td>RM</td></tr>
    <tr><td>Web</td><td><a href="https://www.regionalmanagement.com">www.regionalmanagement.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.45 |
| 2020 | 0.2 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
