---
title: "HAVERTY FURNITURE COS INC (HVT.A)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HAVERTY FURNITURE COS INC</td></tr>
    <tr><td>Symbol</td><td>HVT.A</td></tr>
    <tr><td>Web</td><td><a href="https://www.havertys.com">www.havertys.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 2.62 |
| 2019 | 0.18 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
