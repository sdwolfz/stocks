---
title: "METLIFE INC (MET)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>METLIFE INC</td></tr>
    <tr><td>Symbol</td><td>MET</td></tr>
    <tr><td>Web</td><td><a href="https://www.metlife.com">www.metlife.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.94 |
| 2020 | 1.82 |
| 2019 | 1.74 |
| 2018 | 1.66 |
| 2017 | 1.6 |
| 2016 | 1.575 |
| 2015 | 1.475 |
| 2014 | 1.325 |
| 2013 | 0.55 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
