---
title: "ABERCROMBIE & FITCH CO (ANF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ABERCROMBIE & FITCH CO</td></tr>
    <tr><td>Symbol</td><td>ANF</td></tr>
    <tr><td>Web</td><td><a href="https://corporate.abercrombie.com">corporate.abercrombie.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.2 |
| 2019 | 0.8 |
| 2018 | 0.8 |
| 2017 | 0.8 |
| 2016 | 0.8 |
| 2015 | 0.8 |
| 2014 | 0.8 |
| 2013 | 0.6 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
