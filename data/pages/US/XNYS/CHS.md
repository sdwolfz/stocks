---
title: "CHICO'S FAS INC (CHS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CHICO'S FAS INC</td></tr>
    <tr><td>Symbol</td><td>CHS</td></tr>
    <tr><td>Web</td><td><a href="https://www.chicosfas.com">www.chicosfas.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.09 |
| 2019 | 0.348 |
| 2018 | 0.34 |
| 2017 | 0.332 |
| 2016 | 0.32 |
| 2015 | 0.312 |
| 2014 | 0.3 |
| 2013 | 0.13 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
