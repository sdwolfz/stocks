---
title: "SYSTEMAX INC (SYX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SYSTEMAX INC</td></tr>
    <tr><td>Symbol</td><td>SYX</td></tr>
    <tr><td>Web</td><td><a href="https://www.systemax.com">www.systemax.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.32 |
| 2020 | 3.56 |
| 2019 | 0.48 |
| 2018 | 9.44 |
| 2017 | 0.35 |
| 2016 | 0.15 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
