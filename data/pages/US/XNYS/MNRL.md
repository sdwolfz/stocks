---
title: "BRIGHAM MINERALS INC (MNRL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BRIGHAM MINERALS INC</td></tr>
    <tr><td>Symbol</td><td>MNRL</td></tr>
    <tr><td>Web</td><td><a href="https://www.brighamminerals.com">www.brighamminerals.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.26 |
| 2020 | 1.13 |
| 2019 | 0.66 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
