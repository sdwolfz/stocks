---
title: "FBL FINANCIAL GROUP INC (FFG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FBL FINANCIAL GROUP INC</td></tr>
    <tr><td>Symbol</td><td>FFG</td></tr>
    <tr><td>Web</td><td><a href="https://www.fblfinancial.com">www.fblfinancial.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.52 |
| 2020 | 3.5 |
| 2019 | 3.42 |
| 2018 | 3.34 |
| 2017 | 3.26 |
| 2016 | 3.68 |
| 2015 | 1.6 |
| 2014 | 1.4 |
| 2013 | 0.41 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
