---
title: "MOODYS CORP (MCO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MOODYS CORP</td></tr>
    <tr><td>Symbol</td><td>MCO</td></tr>
    <tr><td>Web</td><td><a href="https://www.moodys.com">www.moodys.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.24 |
| 2020 | 2.24 |
| 2019 | 2.0 |
| 2018 | 1.76 |
| 2017 | 1.52 |
| 2016 | 1.48 |
| 2015 | 1.36 |
| 2014 | 1.12 |
| 2013 | 0.5 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
