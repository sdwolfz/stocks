---
title: " (KOF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>KOF</td></tr>
    <tr><td>Web</td><td><a href="https://www.coca-colafemsa.com">www.coca-colafemsa.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.258 |
| 2020 | 2.138 |
| 2019 | 1.837 |
| 2018 | 1.703 |
| 2017 | 1.753 |
| 2016 | 1.804 |
| 2015 | 1.942 |
| 2014 | 2.174 |
| 2013 | 2.298 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
