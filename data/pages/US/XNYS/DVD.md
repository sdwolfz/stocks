---
title: "DOVER MOTORSPORTS INC (DVD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DOVER MOTORSPORTS INC</td></tr>
    <tr><td>Symbol</td><td>DVD</td></tr>
    <tr><td>Web</td><td><a href="https://www.doverspeedway.com">www.doverspeedway.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.04 |
| 2020 | 0.07 |
| 2019 | 0.1 |
| 2018 | 0.08 |
| 2017 | 0.08 |
| 2016 | 0.05 |
| 2015 | 0.05 |
| 2014 | 0.05 |
| 2013 | 0.05 |
| 2012 | 0.04 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
