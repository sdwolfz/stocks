---
title: "ENNIS INC (EBF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ENNIS INC</td></tr>
    <tr><td>Symbol</td><td>EBF</td></tr>
    <tr><td>Web</td><td><a href="https://www.ennis.com">www.ennis.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.7 |
| 2020 | 0.9 |
| 2019 | 0.9 |
| 2018 | 0.95 |
| 2017 | 0.75 |
| 2016 | 2.2 |
| 2015 | 0.7 |
| 2014 | 0.7 |
| 2013 | 0.35 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
