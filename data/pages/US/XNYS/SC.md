---
title: "SANTANDER CONSUMER USA HLDGS INC (SC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SANTANDER CONSUMER USA HLDGS INC</td></tr>
    <tr><td>Symbol</td><td>SC</td></tr>
    <tr><td>Web</td><td><a href="https://www.santanderconsumerusa.com">www.santanderconsumerusa.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.66 |
| 2020 | 0.66 |
| 2019 | 0.84 |
| 2018 | 0.5 |
| 2017 | 0.03 |
| 2014 | 0.15 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
