---
title: "TOOTSIE ROLL INDUSTRIES INC (TR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TOOTSIE ROLL INDUSTRIES INC</td></tr>
    <tr><td>Symbol</td><td>TR</td></tr>
    <tr><td>Web</td><td><a href="https://www.tootsie.com">www.tootsie.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.09 |
| 2020 | 0.36 |
| 2019 | 0.36 |
| 2018 | 0.36 |
| 2017 | 0.36 |
| 2016 | 0.36 |
| 2015 | 0.27 |
| 2014 | 0.318 |
| 2013 | 0.24 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
