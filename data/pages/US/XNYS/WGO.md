---
title: "WINNEBAGO INDUSTRIES INC (WGO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>WINNEBAGO INDUSTRIES INC</td></tr>
    <tr><td>Symbol</td><td>WGO</td></tr>
    <tr><td>Web</td><td><a href="https://www.winnebagoind.com">www.winnebagoind.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.24 |
| 2020 | 0.45 |
| 2019 | 0.44 |
| 2018 | 0.4 |
| 2017 | 0.4 |
| 2016 | 0.4 |
| 2015 | 0.37 |
| 2014 | 0.09 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
