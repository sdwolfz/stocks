---
title: "EXANTAS CAPITAL CORP (XAN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>EXANTAS CAPITAL CORP</td></tr>
    <tr><td>Symbol</td><td>XAN</td></tr>
    <tr><td>Web</td><td><a href="http://www.exantas.com">www.exantas.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.275 |
| 2019 | 0.95 |
| 2018 | 0.475 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
