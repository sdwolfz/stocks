---
title: "CONSOLIDATED EDISON INC (ED)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CONSOLIDATED EDISON INC</td></tr>
    <tr><td>Symbol</td><td>ED</td></tr>
    <tr><td>Web</td><td><a href="https://www.conedison.com">www.conedison.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.55 |
| 2020 | 3.06 |
| 2019 | 2.96 |
| 2018 | 2.86 |
| 2017 | 2.76 |
| 2016 | 2.68 |
| 2015 | 2.6 |
| 2014 | 2.52 |
| 2013 | 1.23 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
