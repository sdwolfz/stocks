---
title: "CANADIAN NATIONAL RAILWAYS CO (CNI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CANADIAN NATIONAL RAILWAYS CO</td></tr>
    <tr><td>Symbol</td><td>CNI</td></tr>
    <tr><td>Web</td><td><a href="https://www.cn.ca">www.cn.ca</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.715 |
| 2019 | 1.62 |
| 2018 | 1.391 |
| 2017 | 1.274 |
| 2016 | 1.128 |
| 2015 | 1.173 |
| 2014 | 1.0 |
| 2013 | 0.645 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
