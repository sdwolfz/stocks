---
title: "TE CONNECTIVITY LTD (TEL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TE CONNECTIVITY LTD</td></tr>
    <tr><td>Symbol</td><td>TEL</td></tr>
    <tr><td>Web</td><td><a href="https://www.te.com">www.te.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.98 |
| 2020 | 1.9 |
| 2019 | 1.82 |
| 2018 | 1.72 |
| 2017 | 1.57 |
| 2016 | 1.44 |
| 2015 | 1.28 |
| 2014 | 1.12 |
| 2013 | 0.5 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
