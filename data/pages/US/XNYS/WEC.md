---
title: "WEC ENERGY GROUP INC (WEC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>WEC ENERGY GROUP INC</td></tr>
    <tr><td>Symbol</td><td>WEC</td></tr>
    <tr><td>Web</td><td><a href="https://www.wecenergygroup.com">www.wecenergygroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.356 |
| 2020 | 2.528 |
| 2019 | 2.36 |
| 2018 | 2.212 |
| 2017 | 2.08 |
| 2016 | 1.98 |
| 2015 | 1.727 |
| 2014 | 1.56 |
| 2013 | 0.764 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
