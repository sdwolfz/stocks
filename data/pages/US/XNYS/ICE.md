---
title: "INTERCONTINENTAL EXCHANGE INC (ICE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>INTERCONTINENTAL EXCHANGE INC</td></tr>
    <tr><td>Symbol</td><td>ICE</td></tr>
    <tr><td>Web</td><td><a href="https://www.theice.com">www.theice.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.66 |
| 2020 | 1.2 |
| 2019 | 1.1 |
| 2018 | 0.96 |
| 2017 | 1.0 |
| 2016 | 2.72 |
| 2015 | 2.9 |
| 2014 | 2.6 |
| 2013 | 0.65 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
