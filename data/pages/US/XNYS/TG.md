---
title: "TREDEGAR CORP (TG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TREDEGAR CORP</td></tr>
    <tr><td>Symbol</td><td>TG</td></tr>
    <tr><td>Web</td><td><a href="https://www.tredegar.com">www.tredegar.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.24 |
| 2020 | 6.45 |
| 2019 | 0.46 |
| 2018 | 0.44 |
| 2017 | 0.44 |
| 2016 | 0.44 |
| 2015 | 0.42 |
| 2014 | 0.34 |
| 2013 | 0.14 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
