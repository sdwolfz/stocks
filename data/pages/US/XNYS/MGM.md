---
title: "MGM RESORTS INTERNATIONAL (MGM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MGM RESORTS INTERNATIONAL</td></tr>
    <tr><td>Symbol</td><td>MGM</td></tr>
    <tr><td>Web</td><td><a href="https://www.mgmresorts.com">www.mgmresorts.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.004 |
| 2020 | 0.156 |
| 2019 | 0.52 |
| 2018 | 0.48 |
| 2017 | 0.44 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
