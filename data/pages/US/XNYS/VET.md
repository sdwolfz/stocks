---
title: "VERMILION ENERGY INC (VET)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>VERMILION ENERGY INC</td></tr>
    <tr><td>Symbol</td><td>VET</td></tr>
    <tr><td>Web</td><td><a href="https://www.vermilionenergy.com">www.vermilionenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.437 |
| 2019 | 2.078 |
| 2018 | 2.101 |
| 2017 | 1.982 |
| 2016 | 1.746 |
| 2015 | 2.396 |
| 2014 | 2.555 |
| 2013 | 1.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
