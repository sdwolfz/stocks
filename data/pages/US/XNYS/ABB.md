---
title: " (ABB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ABB</td></tr>
    <tr><td>Web</td><td><a href="https://www.abb.com">www.abb.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.528 |
| 2020 | 0.516 |
| 2019 | 0.491 |
| 2018 | 0.504 |
| 2017 | 0.727 |
| 2016 | 0.731 |
| 2015 | 1.325 |
| 2014 | 0.769 |
| 2013 | 0.704 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
