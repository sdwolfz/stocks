---
title: "INTERNATIONAL FLAVORS & FRAGRANCES (IFF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>INTERNATIONAL FLAVORS & FRAGRANCES</td></tr>
    <tr><td>Symbol</td><td>IFF</td></tr>
    <tr><td>Web</td><td><a href="https://www.iff.com">www.iff.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.54 |
| 2020 | 3.04 |
| 2019 | 2.96 |
| 2018 | 2.84 |
| 2017 | 2.66 |
| 2016 | 2.4 |
| 2015 | 2.06 |
| 2014 | 1.72 |
| 2013 | 0.78 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
