---
title: "LOWE'S COMPANIES INC (LOW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>LOWE'S COMPANIES INC</td></tr>
    <tr><td>Symbol</td><td>LOW</td></tr>
    <tr><td>Web</td><td><a href="https://www.lowes.com">www.lowes.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.2 |
| 2020 | 2.25 |
| 2019 | 2.06 |
| 2018 | 1.78 |
| 2017 | 1.52 |
| 2016 | 1.26 |
| 2015 | 1.02 |
| 2014 | 0.82 |
| 2013 | 0.36 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
