---
title: "TEXTAINER GROUP HLDGS LTD (TGH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TEXTAINER GROUP HLDGS LTD</td></tr>
    <tr><td>Symbol</td><td>TGH</td></tr>
    <tr><td>Web</td><td><a href="https://www.textainer.com">www.textainer.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.51 |
| 2015 | 1.65 |
| 2014 | 1.88 |
| 2013 | 0.94 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
