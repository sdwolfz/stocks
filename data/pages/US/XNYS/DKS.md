---
title: "DICKS SPORTING GOODS INC (DKS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DICKS SPORTING GOODS INC</td></tr>
    <tr><td>Symbol</td><td>DKS</td></tr>
    <tr><td>Web</td><td><a href="https://www.dickssportinggoods.com">www.dickssportinggoods.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.363 |
| 2020 | 1.252 |
| 2019 | 1.1 |
| 2018 | 0.9 |
| 2017 | 0.68 |
| 2016 | 0.604 |
| 2015 | 0.552 |
| 2014 | 0.5 |
| 2013 | 0.375 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
