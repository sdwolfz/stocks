---
title: "HIGHPOINT RESOURCES CORPORATION (HPR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HIGHPOINT RESOURCES CORPORATION</td></tr>
    <tr><td>Symbol</td><td>HPR</td></tr>
    <tr><td>Web</td><td><a href="http://www.hpres.com">www.hpres.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
