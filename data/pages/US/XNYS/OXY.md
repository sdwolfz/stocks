---
title: "OCCIDENTAL PETROLEUM CORP (OXY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>OCCIDENTAL PETROLEUM CORP</td></tr>
    <tr><td>Symbol</td><td>OXY</td></tr>
    <tr><td>Web</td><td><a href="https://www.oxy.com">www.oxy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.01 |
| 2020 | 0.82 |
| 2019 | 3.14 |
| 2018 | 3.1 |
| 2017 | 3.06 |
| 2016 | 3.02 |
| 2015 | 2.97 |
| 2014 | 2.88 |
| 2013 | 1.28 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
