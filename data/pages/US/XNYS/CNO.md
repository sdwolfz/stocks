---
title: "CNO FINANCIAL GROUP INC (CNO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CNO FINANCIAL GROUP INC</td></tr>
    <tr><td>Symbol</td><td>CNO</td></tr>
    <tr><td>Web</td><td><a href="https://www.cnoinc.com">www.cnoinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.12 |
| 2020 | 0.47 |
| 2019 | 0.43 |
| 2018 | 0.39 |
| 2017 | 0.35 |
| 2016 | 0.31 |
| 2015 | 0.27 |
| 2014 | 0.24 |
| 2013 | 0.06 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
