---
title: "ARCHROCK INC (AROC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ARCHROCK INC</td></tr>
    <tr><td>Symbol</td><td>AROC</td></tr>
    <tr><td>Web</td><td><a href="https://www.archrock.com">www.archrock.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.29 |
| 2020 | 0.58 |
| 2019 | 0.554 |
| 2018 | 0.504 |
| 2017 | 0.48 |
| 2016 | 0.498 |
| 2015 | 0.15 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
