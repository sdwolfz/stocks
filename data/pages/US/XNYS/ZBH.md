---
title: "ZIMMER BIOMET HOLDINGS INC (ZBH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ZIMMER BIOMET HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>ZBH</td></tr>
    <tr><td>Web</td><td><a href="https://www.zimmerbiomet.com">www.zimmerbiomet.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.24 |
| 2020 | 0.96 |
| 2019 | 0.96 |
| 2018 | 0.96 |
| 2017 | 0.96 |
| 2016 | 0.96 |
| 2015 | 0.66 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
