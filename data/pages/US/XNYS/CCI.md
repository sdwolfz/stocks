---
title: "CROWN CASTLE INTERNATIONAL CORP (CCI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CROWN CASTLE INTERNATIONAL CORP</td></tr>
    <tr><td>Symbol</td><td>CCI</td></tr>
    <tr><td>Web</td><td><a href="https://www.crowncastle.com">www.crowncastle.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.33 |
| 2020 | 4.93 |
| 2019 | 4.575 |
| 2018 | 4.275 |
| 2017 | 3.9 |
| 2016 | 3.605 |
| 2015 | 3.345 |
| 2014 | 1.87 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
