---
title: "NATURAL GROCERS BY VITA COTTAGE (NGVC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NATURAL GROCERS BY VITA COTTAGE</td></tr>
    <tr><td>Symbol</td><td>NGVC</td></tr>
    <tr><td>Web</td><td><a href="https://www.naturalgrocers.com">www.naturalgrocers.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.07 |
| 2020 | 2.28 |
| 2019 | 0.07 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
