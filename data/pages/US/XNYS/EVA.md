---
title: " (EVA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>EVA</td></tr>
    <tr><td>Web</td><td><a href="https://www.envivabiomass.com">www.envivabiomass.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.565 |
| 2020 | 2.895 |
| 2019 | 2.615 |
| 2018 | 2.51 |
| 2017 | 2.275 |
| 2016 | 2.025 |
| 2015 | 0.703 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
