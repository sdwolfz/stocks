---
title: " (KB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>KB</td></tr>
    <tr><td>Web</td><td><a href="https://www.kbfg.com">www.kbfg.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.31 |
| 2019 | 1.492 |
| 2018 | 1.375 |
| 2017 | 1.471 |
| 2016 | 0.833 |
| 2015 | 0.684 |
| 2014 | 0.565 |
| 2013 | 0.377 |
| 2012 | 0.39 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
