---
title: "POLARIS INC (PII)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>POLARIS INC</td></tr>
    <tr><td>Symbol</td><td>PII</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.26 |
| 2020 | 2.48 |
| 2019 | 2.44 |
| 2018 | 2.4 |
| 2017 | 2.32 |
| 2016 | 2.2 |
| 2015 | 2.12 |
| 2014 | 1.92 |
| 2013 | 0.84 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
