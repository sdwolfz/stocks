---
title: "NEW YORK TIMES CO (NYT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NEW YORK TIMES CO</td></tr>
    <tr><td>Symbol</td><td>NYT</td></tr>
    <tr><td>Web</td><td><a href="https://www.nytco.com">www.nytco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.13 |
| 2020 | 0.23 |
| 2019 | 0.19 |
| 2018 | 0.16 |
| 2017 | 0.16 |
| 2016 | 0.16 |
| 2015 | 0.16 |
| 2014 | 0.16 |
| 2013 | 0.04 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
