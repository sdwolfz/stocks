---
title: "INTERPUBLIC GROUP COS INC (IPG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>INTERPUBLIC GROUP COS INC</td></tr>
    <tr><td>Symbol</td><td>IPG</td></tr>
    <tr><td>Web</td><td><a href="https://www.interpublic.com">www.interpublic.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.27 |
| 2020 | 1.02 |
| 2019 | 0.94 |
| 2018 | 0.84 |
| 2017 | 0.72 |
| 2016 | 0.6 |
| 2015 | 0.48 |
| 2014 | 0.38 |
| 2013 | 0.15 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
