---
title: "MEDTRONIC PLC (MDT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MEDTRONIC PLC</td></tr>
    <tr><td>Symbol</td><td>MDT</td></tr>
    <tr><td>Web</td><td><a href="https://www.medtronic.com">www.medtronic.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.58 |
| 2020 | 2.28 |
| 2019 | 2.12 |
| 2018 | 1.96 |
| 2017 | 1.81 |
| 2016 | 1.67 |
| 2015 | 1.445 |
| 2014 | 1.195 |
| 2013 | 0.84 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
