---
title: "PBF ENERGY INC (PBF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PBF ENERGY INC</td></tr>
    <tr><td>Symbol</td><td>PBF</td></tr>
    <tr><td>Web</td><td><a href="https://www.pbfenergy.com">www.pbfenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.3 |
| 2019 | 1.2 |
| 2018 | 1.2 |
| 2017 | 1.2 |
| 2016 | 1.2 |
| 2015 | 1.2 |
| 2014 | 1.2 |
| 2013 | 0.6 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
