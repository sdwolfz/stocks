---
title: "EDGEWELL PERSONAL CARE CO (EPC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>EDGEWELL PERSONAL CARE CO</td></tr>
    <tr><td>Symbol</td><td>EPC</td></tr>
    <tr><td>Web</td><td><a href="https://www.edgewell.com">www.edgewell.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |
| 2020 | 0.15 |
| 2015 | 0.5 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
