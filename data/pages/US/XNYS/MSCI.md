---
title: "MSCI INC (MSCI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MSCI INC</td></tr>
    <tr><td>Symbol</td><td>MSCI</td></tr>
    <tr><td>Web</td><td><a href="https://www.msci.com">www.msci.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.56 |
| 2020 | 2.92 |
| 2019 | 2.52 |
| 2018 | 1.92 |
| 2017 | 1.32 |
| 2016 | 1.0 |
| 2015 | 0.8 |
| 2014 | 0.18 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
