---
title: "APOLLO GLOBAL MANAGEMENT INC (APO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>APOLLO GLOBAL MANAGEMENT INC</td></tr>
    <tr><td>Symbol</td><td>APO</td></tr>
    <tr><td>Web</td><td><a href="https://www.apollo.com">www.apollo.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.1 |
| 2020 | 2.31 |
| 2019 | 2.02 |
| 2018 | 1.93 |
| 2017 | 1.85 |
| 2016 | 1.25 |
| 2015 | 1.96 |
| 2014 | 3.11 |
| 2013 | 2.33 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
