---
title: "LUXFER HOLDINGS PLC (LXFR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>LUXFER HOLDINGS PLC</td></tr>
    <tr><td>Symbol</td><td>LXFR</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.25 |
| 2020 | 0.5 |
| 2019 | 0.5 |
| 2018 | 0.5 |
| 2017 | 0.5 |
| 2016 | 0.5 |
| 2015 | 0.4 |
| 2014 | 0.4 |
| 2013 | 0.4 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
