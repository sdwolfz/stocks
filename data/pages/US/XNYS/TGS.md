---
title: " (TGS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>TGS</td></tr>
    <tr><td>Web</td><td><a href="https://www.tgs.com.ar">www.tgs.com.ar</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 1.013 |
| 2018 | 0.931 |
| 2016 | 0.031 |
| 2014 | 0.14 |
| 2013 | 0.128 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
