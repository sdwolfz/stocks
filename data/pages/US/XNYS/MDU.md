---
title: "MDU RESOURCES GROUP INC (MDU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MDU RESOURCES GROUP INC</td></tr>
    <tr><td>Symbol</td><td>MDU</td></tr>
    <tr><td>Web</td><td><a href="https://www.mdu.com">www.mdu.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.213 |
| 2020 | 0.834 |
| 2019 | 0.816 |
| 2018 | 0.797 |
| 2017 | 0.774 |
| 2016 | 0.756 |
| 2015 | 0.737 |
| 2014 | 0.714 |
| 2013 | 0.349 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
