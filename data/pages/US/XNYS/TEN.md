---
title: "TENNECO INC (TEN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TENNECO INC</td></tr>
    <tr><td>Symbol</td><td>TEN</td></tr>
    <tr><td>Web</td><td><a href="https://www.tenneco.com">www.tenneco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.25 |
| 2018 | 1.0 |
| 2017 | 1.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
