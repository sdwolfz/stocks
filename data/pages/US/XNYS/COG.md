---
title: "CABOT OIL & GAS CORP (COG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CABOT OIL & GAS CORP</td></tr>
    <tr><td>Symbol</td><td>COG</td></tr>
    <tr><td>Web</td><td><a href="https://www.cabotog.com">www.cabotog.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.21 |
| 2020 | 0.4 |
| 2019 | 0.35 |
| 2018 | 0.25 |
| 2017 | 0.17 |
| 2016 | 0.06 |
| 2015 | 0.08 |
| 2014 | 0.08 |
| 2013 | 0.03 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
