---
title: "MERCURY GENERAL CORP (MCY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MERCURY GENERAL CORP</td></tr>
    <tr><td>Symbol</td><td>MCY</td></tr>
    <tr><td>Web</td><td><a href="https://www.mercuryinsurance.com">www.mercuryinsurance.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.264 |
| 2020 | 2.522 |
| 2019 | 2.511 |
| 2018 | 2.502 |
| 2017 | 2.494 |
| 2016 | 2.483 |
| 2015 | 2.474 |
| 2014 | 2.463 |
| 2013 | 1.228 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
