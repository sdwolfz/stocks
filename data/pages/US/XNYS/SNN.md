---
title: " (SNN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SNN</td></tr>
    <tr><td>Web</td><td><a href="https://www.smith-nephew.com">www.smith-nephew.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.452 |
| 2020 | 0.73 |
| 2019 | 0.708 |
| 2018 | 0.714 |
| 2017 | 0.606 |
| 2016 | 0.606 |
| 2015 | 0.598 |
| 2014 | 1.05 |
| 2013 | 1.3 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
