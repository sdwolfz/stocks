---
title: " (WY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>WY</td></tr>
    <tr><td>Web</td><td><a href="https://www.weyerhaeuser.com">www.weyerhaeuser.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.17 |
| 2020 | 0.51 |
| 2019 | 1.36 |
| 2018 | 1.32 |
| 2017 | 1.25 |
| 2016 | 1.24 |
| 2015 | 1.2 |
| 2014 | 1.02 |
| 2013 | 0.44 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
