---
title: "ZOETIS INC (ZTS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ZOETIS INC</td></tr>
    <tr><td>Symbol</td><td>ZTS</td></tr>
    <tr><td>Web</td><td><a href="https://www.zoetis.com">www.zoetis.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.5 |
| 2020 | 0.8 |
| 2019 | 0.656 |
| 2018 | 0.504 |
| 2017 | 0.42 |
| 2016 | 0.38 |
| 2015 | 0.332 |
| 2014 | 0.288 |
| 2013 | 0.13 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
