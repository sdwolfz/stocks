---
title: "GENERAL DYNAMICS CORP (GD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GENERAL DYNAMICS CORP</td></tr>
    <tr><td>Symbol</td><td>GD</td></tr>
    <tr><td>Web</td><td><a href="https://www.gd.com">www.gd.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.29 |
| 2020 | 4.32 |
| 2019 | 3.99 |
| 2018 | 3.63 |
| 2017 | 3.28 |
| 2016 | 2.97 |
| 2015 | 2.69 |
| 2014 | 2.42 |
| 2013 | 0.56 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
