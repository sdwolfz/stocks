---
title: "MARINE PRODUCTS CORP (MPX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MARINE PRODUCTS CORP</td></tr>
    <tr><td>Symbol</td><td>MPX</td></tr>
    <tr><td>Web</td><td><a href="https://www.marineproductscorp.com">www.marineproductscorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.22 |
| 2020 | 0.4 |
| 2019 | 0.58 |
| 2018 | 0.5 |
| 2017 | 0.33 |
| 2016 | 0.24 |
| 2015 | 0.24 |
| 2014 | 0.12 |
| 2013 | 0.06 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
