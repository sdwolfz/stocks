---
title: "SCORPIO TANKERS INC (STNG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SCORPIO TANKERS INC</td></tr>
    <tr><td>Symbol</td><td>STNG</td></tr>
    <tr><td>Web</td><td><a href="https://www.scorpiotankers.com">www.scorpiotankers.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.4 |
| 2019 | 0.5 |
| 2018 | 0.13 |
| 2017 | 0.04 |
| 2016 | 0.375 |
| 2015 | 0.495 |
| 2014 | 0.39 |
| 2013 | 0.105 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
