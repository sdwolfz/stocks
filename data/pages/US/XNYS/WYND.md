---
title: "WYNDHAM DESTINATIONS INC (WYND)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>WYNDHAM DESTINATIONS INC</td></tr>
    <tr><td>Symbol</td><td>WYND</td></tr>
    <tr><td>Web</td><td><a href="http://www.wyndhamdestinations.com">www.wyndhamdestinations.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.6 |
| 2019 | 1.8 |
| 2018 | 1.23 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
