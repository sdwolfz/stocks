---
title: "NATIONAL PRESTO INDUSTRIES INC (NPK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NATIONAL PRESTO INDUSTRIES INC</td></tr>
    <tr><td>Symbol</td><td>NPK</td></tr>
    <tr><td>Web</td><td><a href="https://www.gopresto.com">www.gopresto.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 6.25 |
| 2020 | 6.0 |
| 2019 | 6.0 |
| 2017 | 5.5 |
| 2016 | 5.05 |
| 2015 | 4.05 |
| 2014 | 1.0 |
| 2012 | 1.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
