---
title: " (RWT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>RWT</td></tr>
    <tr><td>Web</td><td><a href="https://www.redwoodtrust.com">www.redwoodtrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.16 |
| 2020 | 0.725 |
| 2019 | 1.2 |
| 2018 | 1.18 |
| 2017 | 1.12 |
| 2016 | 1.12 |
| 2015 | 1.12 |
| 2014 | 1.12 |
| 2013 | 0.56 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
