---
title: "BLACKROCK INC (BLK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BLACKROCK INC</td></tr>
    <tr><td>Symbol</td><td>BLK</td></tr>
    <tr><td>Web</td><td><a href="https://www.blackrock.com">www.blackrock.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 4.13 |
| 2020 | 14.52 |
| 2019 | 13.2 |
| 2018 | 12.093 |
| 2017 | 10.0 |
| 2016 | 9.16 |
| 2015 | 8.72 |
| 2014 | 7.72 |
| 2013 | 3.36 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
