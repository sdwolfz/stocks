---
title: "RE/MAX HOLDINGS (RMAX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>RE/MAX HOLDINGS</td></tr>
    <tr><td>Symbol</td><td>RMAX</td></tr>
    <tr><td>Web</td><td><a href="https://www.remax.com">www.remax.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.46 |
| 2020 | 0.88 |
| 2019 | 0.84 |
| 2018 | 0.8 |
| 2017 | 0.72 |
| 2016 | 0.6 |
| 2015 | 0.5 |
| 2014 | 0.252 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
