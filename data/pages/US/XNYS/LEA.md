---
title: "LEAR CORP (LEA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>LEAR CORP</td></tr>
    <tr><td>Symbol</td><td>LEA</td></tr>
    <tr><td>Web</td><td><a href="https://www.lear.com">www.lear.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.25 |
| 2020 | 1.02 |
| 2019 | 3.0 |
| 2018 | 2.8 |
| 2017 | 2.0 |
| 2016 | 1.2 |
| 2015 | 1.0 |
| 2014 | 0.8 |
| 2013 | 0.34 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
