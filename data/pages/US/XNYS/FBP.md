---
title: "FIRST BANCORP PUERTO RICO (FBP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FIRST BANCORP PUERTO RICO</td></tr>
    <tr><td>Symbol</td><td>FBP</td></tr>
    <tr><td>Web</td><td><a href="https://www.1firstbank.com">www.1firstbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.14 |
| 2020 | 0.2 |
| 2019 | 0.14 |
| 2018 | 0.03 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
