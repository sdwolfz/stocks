---
title: "ENTRAVISION COMMUNICATIONS CORP (EVC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ENTRAVISION COMMUNICATIONS CORP</td></tr>
    <tr><td>Symbol</td><td>EVC</td></tr>
    <tr><td>Web</td><td><a href="https://www.entravision.com">www.entravision.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.025 |
| 2020 | 0.125 |
| 2019 | 0.2 |
| 2018 | 0.2 |
| 2017 | 0.162 |
| 2016 | 0.124 |
| 2015 | 0.106 |
| 2014 | 0.1 |
| 2013 | 0.025 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
