---
title: "DELL TECHNOLOGIES INC (DELL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DELL TECHNOLOGIES INC</td></tr>
    <tr><td>Symbol</td><td>DELL</td></tr>
    <tr><td>Web</td><td><a href="https://www.delltechnologies.com">www.delltechnologies.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2013 | 0.16 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
