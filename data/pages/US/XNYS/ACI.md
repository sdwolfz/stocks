---
title: "ALBERTSONS COMPANIES INC (ACI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ALBERTSONS COMPANIES INC</td></tr>
    <tr><td>Symbol</td><td>ACI</td></tr>
    <tr><td>Web</td><td><a href="https://www.albertsonscompanies.com">www.albertsonscompanies.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.1 |
| 2014 | 0.01 |
| 2013 | 0.06 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
