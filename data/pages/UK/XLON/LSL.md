---
title: "Lsl Prop (LSL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Lsl Prop</td></tr>
    <tr><td>Symbol</td><td>LSL</td></tr>
    <tr><td>Web</td><td><a href="https://www.lslps.co.uk">www.lslps.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 11.2 |
| 2018 | 10.9 |
| 2017 | 11.3 |
| 2016 | 10.3 |
| 2015 | 12.6 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
