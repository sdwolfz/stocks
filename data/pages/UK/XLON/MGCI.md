---
title: "M&g Credit Inc. (MGCI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>M&g Credit Inc.</td></tr>
    <tr><td>Symbol</td><td>MGCI</td></tr>
    <tr><td>Web</td><td><a href="https://www.mandg.co.uk">www.mandg.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
