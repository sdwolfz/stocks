---
title: "Majedie Inv. (MAJE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Majedie Inv.</td></tr>
    <tr><td>Symbol</td><td>MAJE</td></tr>
    <tr><td>Web</td><td><a href="https://www.majedie.co.uk">www.majedie.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 11.4 |
| 2019 | 11.4 |
| 2018 | 11.0 |
| 2017 | 9.75 |
| 2016 | 8.75 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
