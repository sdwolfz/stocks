---
title: "Hend.eur.focus (HEFT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Hend.eur.focus</td></tr>
    <tr><td>Symbol</td><td>HEFT</td></tr>
    <tr><td>Web</td><td><a href="https://www.hendersoneuropeanfocus.com">www.hendersoneuropeanfocus.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 31.3 |
| 2019 | 31.3 |
| 2018 | 31.0 |
| 2017 | 29.5 |
| 2016 | 26.4 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
