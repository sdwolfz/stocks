---
title: "Stagecoach (SGC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Stagecoach</td></tr>
    <tr><td>Symbol</td><td>SGC</td></tr>
    <tr><td>Web</td><td><a href="https://www.stagecoachplc.com">www.stagecoachplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 3.8 |
| 2019 | 7.7 |
| 2018 | 7.7 |
| 2016 | 11.4 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
