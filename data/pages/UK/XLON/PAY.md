---
title: "Paypoint (PAY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Paypoint</td></tr>
    <tr><td>Symbol</td><td>PAY</td></tr>
    <tr><td>Web</td><td><a href="https://www.paypoint.com">www.paypoint.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 39.2 |
| 2019 | 39.2 |
| 2018 | 45.9 |
| 2017 | 45.0 |
| 2016 | 42.4 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
