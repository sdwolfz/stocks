---
title: "Glaxosmithkline (GSK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Glaxosmithkline</td></tr>
    <tr><td>Symbol</td><td>GSK</td></tr>
    <tr><td>Web</td><td><a href="https://www.gsk.com">www.gsk.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 80.0 |
| 2019 | 80.0 |
| 2018 | 80.0 |
| 2017 | 80.0 |
| 2016 | 80.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
