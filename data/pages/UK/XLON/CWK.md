---
title: "Cranswick (CWK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Cranswick</td></tr>
    <tr><td>Symbol</td><td>CWK</td></tr>
    <tr><td>Web</td><td><a href="https://www.cranswick.co.uk">www.cranswick.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 60.4 |
| 2019 | 55.9 |
| 2018 | 53.7 |
| 2017 | 44.1 |
| 2016 | 37.5 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
