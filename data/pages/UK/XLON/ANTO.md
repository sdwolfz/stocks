---
title: "Antofagasta (ANTO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Antofagasta</td></tr>
    <tr><td>Symbol</td><td>ANTO</td></tr>
    <tr><td>Web</td><td><a href="https://www.antofagasta.co.uk">www.antofagasta.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 34.1 |
| 2018 | 43.8 |
| 2017 | 50.9 |
| 2016 | 18.4 |
| 2015 | 3.1 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
