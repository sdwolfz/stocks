---
title: "Aviva (AV.)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Aviva</td></tr>
    <tr><td>Symbol</td><td>AV.</td></tr>
    <tr><td>Web</td><td><a href="https://www.aviva.com">www.aviva.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 21.0 |
| 2019 | 30.9 |
| 2018 | 30.0 |
| 2017 | 27.4 |
| 2016 | 23.3 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
