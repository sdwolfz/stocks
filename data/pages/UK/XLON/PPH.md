---
title: "Pphe Hotel (PPH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Pphe Hotel</td></tr>
    <tr><td>Symbol</td><td>PPH</td></tr>
    <tr><td>Web</td><td><a href="https://www.pphe.com">www.pphe.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 37.0 |
| 2018 | 35.0 |
| 2017 | 24.0 |
| 2016 | 21.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
