---
title: "BHP Group (BHP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>BHP Group</td></tr>
    <tr><td>Symbol</td><td>BHP</td></tr>
    <tr><td>Web</td><td><a href="https://www.bhp.com">www.bhp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 120.0 |
| 2019 | 235.0 |
| 2018 | 118.0 |
| 2017 | 83.0 |
| 2016 | 30.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
