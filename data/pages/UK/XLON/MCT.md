---
title: "Middlefield Prf (MCT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Middlefield Prf</td></tr>
    <tr><td>Symbol</td><td>MCT</td></tr>
    <tr><td>Web</td><td><a href="https://www.middlefield.co.uk">www.middlefield.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 5.1 |
| 2018 | 5.1 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
