---
title: "NewRiver (NRR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>NewRiver</td></tr>
    <tr><td>Symbol</td><td>NRR</td></tr>
    <tr><td>Web</td><td><a href="https://www.nrr.co.uk">www.nrr.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 16.2 |
| 2019 | 21.6 |
| 2018 | 21.0 |
| 2017 | 23.0 |
| 2016 | 18.5 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
