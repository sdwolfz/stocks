---
title: "JPMORGAN JAPAN SMALLER CO TST PLC ORD 10P (JPS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/lse/">LSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/lse/">XLON</a></td></tr>
    <tr><td>Name</td><td>JPMORGAN JAPAN SMALLER CO TST PLC ORD 10P</td></tr>
    <tr><td>Symbol</td><td>JPS</td></tr>
    <tr><td>Web</td><td><a href="http://www.jpmfjapanesesmaller.com">www.jpmfjapanesesmaller.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 17.7 |
| 2019 | 18.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
