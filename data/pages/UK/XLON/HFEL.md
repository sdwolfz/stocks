---
title: "Hend.far East (HFEL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Hend.far East</td></tr>
    <tr><td>Symbol</td><td>HFEL</td></tr>
    <tr><td>Web</td><td><a href="https://www.janushenderson.com">www.janushenderson.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 23.0 |
| 2019 | 22.4 |
| 2018 | 21.6 |
| 2017 | 20.8 |
| 2016 | 20.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
