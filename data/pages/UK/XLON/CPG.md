---
title: "Compass Group (CPG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Compass Group</td></tr>
    <tr><td>Symbol</td><td>CPG</td></tr>
    <tr><td>Web</td><td><a href="https://www.compass-group.com">www.compass-group.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 16.36 |
| 2019 | 40.0 |
| 2018 | 37.7 |
| 2017 | 33.5 |
| 2016 | 31.7 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
