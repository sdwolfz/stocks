---
title: "Aberdeen Di&g (ADIG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Aberdeen Di&g</td></tr>
    <tr><td>Symbol</td><td>ADIG</td></tr>
    <tr><td>Web</td><td><a href="https://www.aberdeendiversified.co.uk">www.aberdeendiversified.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 5.44 |
| 2019 | 5.36 |
| 2018 | 5.24 |
| 2017 | 5.89 |
| 2016 | 6.54 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
