---
title: "Rm (RM.)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Rm</td></tr>
    <tr><td>Symbol</td><td>RM.</td></tr>
    <tr><td>Web</td><td><a href="https://www.rmplc.com">www.rmplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 3.0 |
| 2019 | 8.0 |
| 2018 | 7.6 |
| 2017 | 6.6 |
| 2016 | 6.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
