---
title: "Bankers Investment Trust (BNKR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Bankers Investment Trust</td></tr>
    <tr><td>Symbol</td><td>BNKR</td></tr>
    <tr><td>Web</td><td><a href="https://www.bankersinvestmenttrust.com">www.bankersinvestmenttrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 21.54 |
| 2019 | 20.9 |
| 2017 | 18.6 |
| 2016 | 17.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
