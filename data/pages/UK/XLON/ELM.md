---
title: "Elementis (ELM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Elementis</td></tr>
    <tr><td>Symbol</td><td>ELM</td></tr>
    <tr><td>Web</td><td><a href="https://www.elementis.com">www.elementis.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 8.55 |
| 2016 | 16.8 |
| 2015 | 16.45 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
