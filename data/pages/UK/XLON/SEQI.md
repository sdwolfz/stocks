---
title: "Sequoia Economic Infrastructure Fund (SEQI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Sequoia Economic Infrastructure Fund</td></tr>
    <tr><td>Symbol</td><td>SEQI</td></tr>
    <tr><td>Web</td><td><a href="https://www.seqifund.com">www.seqifund.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 6.25 |
| 2019 | 6.0 |
| 2018 | 6.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
