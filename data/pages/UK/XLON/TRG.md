---
title: "Tr Euro.growth (TRG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Tr Euro.growth</td></tr>
    <tr><td>Symbol</td><td>TRG</td></tr>
    <tr><td>Web</td><td><a href="https://www.treuropeangrowthtrust.com">www.treuropeangrowthtrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 22.0 |
| 2019 | 22.0 |
| 2018 | 19.0 |
| 2017 | 14.5 |
| 2016 | 11.5 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
