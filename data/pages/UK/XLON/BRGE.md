---
title: "Blackrock Great (BRGE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Blackrock Great</td></tr>
    <tr><td>Symbol</td><td>BRGE</td></tr>
    <tr><td>Web</td><td><a href="https://www.blackrock.com">www.blackrock.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 6.15 |
| 2019 | 5.85 |
| 2018 | 5.75 |
| 2017 | 5.45 |
| 2016 | 5.3 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
