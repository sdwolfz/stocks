---
title: "ALCENTRA EUROPEAN FLTG RTE INC FD RED ORD NPV GBP (AEFS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/lse/">LSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/lse/">XLON</a></td></tr>
    <tr><td>Name</td><td>ALCENTRA EUROPEAN FLTG RTE INC FD RED ORD NPV GBP</td></tr>
    <tr><td>Symbol</td><td>AEFS</td></tr>
    <tr><td>Web</td><td><a href="http://www.aefrif.com">www.aefrif.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 4.46 |
| 2018 | 4.26 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
