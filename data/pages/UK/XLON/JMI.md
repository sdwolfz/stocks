---
title: "JPMorgan Smaller Companies (JMI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>JPMorgan Smaller Companies</td></tr>
    <tr><td>Symbol</td><td>JMI</td></tr>
    <tr><td>Web</td><td><a href="https://www.jpmfsmallercompanies.com">www.jpmfsmallercompanies.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 5.5 |
| 2019 | 5.5 |
| 2018 | 5.4 |
| 2017 | 23.0 |
| 2016 | 18.3 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
