---
title: "Rank (RNK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Rank</td></tr>
    <tr><td>Symbol</td><td>RNK</td></tr>
    <tr><td>Web</td><td><a href="https://www.rank.com">www.rank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 2.8 |
| 2019 | 7.65 |
| 2018 | 7.45 |
| 2017 | 7.3 |
| 2016 | 6.5 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
