---
title: "Jp Morg.chin (JCGI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Jp Morg.chin</td></tr>
    <tr><td>Symbol</td><td>JCGI</td></tr>
    <tr><td>Web</td><td><a href="https://www.jpmfchinese.com">www.jpmfchinese.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 7.4 |
| 2019 | 2.5 |
| 2018 | 3.5 |
| 2017 | 1.6 |
| 2016 | 1.6 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
