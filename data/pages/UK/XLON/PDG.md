---
title: "Pendragon (PDG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Pendragon</td></tr>
    <tr><td>Symbol</td><td>PDG</td></tr>
    <tr><td>Web</td><td><a href="https://www.pendragonplc.com">www.pendragonplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 1.55 |
| 2016 | 1.45 |
| 2015 | 1.3 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
