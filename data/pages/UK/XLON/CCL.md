---
title: "Carnival (CCL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Carnival</td></tr>
    <tr><td>Symbol</td><td>CCL</td></tr>
    <tr><td>Web</td><td><a href="https://www.carnivalcorp.com">www.carnivalcorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 50.0 |
| 2019 | 200.0 |
| 2018 | 195.0 |
| 2017 | 160.0 |
| 2016 | 135.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
