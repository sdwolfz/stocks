---
title: "Smithson Invest (SSON)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Smithson Invest</td></tr>
    <tr><td>Symbol</td><td>SSON</td></tr>
    <tr><td>Web</td><td><a href="https://www.smithson.co.uk">www.smithson.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
