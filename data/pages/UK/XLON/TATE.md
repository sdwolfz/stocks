---
title: "Tate & Lyle (TATE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Tate & Lyle</td></tr>
    <tr><td>Symbol</td><td>TATE</td></tr>
    <tr><td>Web</td><td><a href="https://www.tateandlyle.com">www.tateandlyle.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 29.6 |
| 2019 | 29.4 |
| 2018 | 28.7 |
| 2017 | 28.0 |
| 2016 | 28.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
