---
title: "Rightmove (RMV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Rightmove</td></tr>
    <tr><td>Symbol</td><td>RMV</td></tr>
    <tr><td>Web</td><td><a href="https://plc.rightmove.co.uk">plc.rightmove.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 7.2 |
| 2018 | 6.5 |
| 2017 | 5.8 |
| 2016 | 5.1 |
| 2015 | 4.3 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
