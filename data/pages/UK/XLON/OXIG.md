---
title: "Oxford Instruments (OXIG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Oxford Instruments</td></tr>
    <tr><td>Symbol</td><td>OXIG</td></tr>
    <tr><td>Web</td><td><a href="https://www.oxford-instruments.com">www.oxford-instruments.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 14.4 |
| 2019 | 14.4 |
| 2018 | 13.3 |
| 2017 | 13.0 |
| 2016 | 13.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
