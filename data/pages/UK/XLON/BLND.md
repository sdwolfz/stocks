---
title: "British Land (BLND)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>British Land</td></tr>
    <tr><td>Symbol</td><td>BLND</td></tr>
    <tr><td>Web</td><td><a href="https://www.britishland.com">www.britishland.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 18.04 |
| 2019 | 31.0 |
| 2018 | 30.08 |
| 2017 | 29.2 |
| 2016 | 28.4 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
