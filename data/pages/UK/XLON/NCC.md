---
title: "Ncc (NCC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Ncc</td></tr>
    <tr><td>Symbol</td><td>NCC</td></tr>
    <tr><td>Web</td><td><a href="https://www.nccgroup.com">www.nccgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 4.65 |
| 2019 | 7.8 |
| 2018 | 4.65 |
| 2017 | 4.65 |
| 2016 | 4.65 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
