---
title: "Anglo American (AAL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Anglo American</td></tr>
    <tr><td>Symbol</td><td>AAL</td></tr>
    <tr><td>Web</td><td><a href="https://www.angloamerican.com">www.angloamerican.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 100.0 |
| 2019 | 109.0 |
| 2018 | 100.0 |
| 2017 | 102.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
