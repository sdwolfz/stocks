---
title: "Diageo (DGE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Diageo</td></tr>
    <tr><td>Symbol</td><td>DGE</td></tr>
    <tr><td>Web</td><td><a href="https://www.diageo.com">www.diageo.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 69.88 |
| 2019 | 68.57 |
| 2018 | 65.3 |
| 2017 | 62.2 |
| 2016 | 59.2 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
