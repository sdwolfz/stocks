---
title: "HgCapital Trust plc (HGT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>HgCapital Trust plc</td></tr>
    <tr><td>Symbol</td><td>HGT</td></tr>
    <tr><td>Web</td><td><a href="https://www.hgcapitaltrust.com">www.hgcapitaltrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 5.0 |
| 2019 | 4.8 |
| 2018 | 4.6 |
| 2017 | 4.6 |
| 2016 | 4.6 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
