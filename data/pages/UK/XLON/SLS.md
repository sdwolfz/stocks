---
title: "Stand Life Uk (SLS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Stand Life Uk</td></tr>
    <tr><td>Symbol</td><td>SLS</td></tr>
    <tr><td>Web</td><td><a href="https://www.sli.co.uk">www.sli.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 7.7 |
| 2019 | 7.7 |
| 2018 | 7.0 |
| 2017 | 6.7 |
| 2016 | 6.6 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
