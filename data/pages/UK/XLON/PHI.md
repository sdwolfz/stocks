---
title: "Pacific Horizon (PHI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Pacific Horizon</td></tr>
    <tr><td>Symbol</td><td>PHI</td></tr>
    <tr><td>Web</td><td><a href="https://www.bailliegifford.com">www.bailliegifford.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.25 |
| 2016 | 0.35 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
