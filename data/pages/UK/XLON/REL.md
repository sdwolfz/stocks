---
title: "Relx (REL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Relx</td></tr>
    <tr><td>Symbol</td><td>REL</td></tr>
    <tr><td>Web</td><td><a href="https://www.relx.com">www.relx.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 47.0 |
| 2019 | 45.7 |
| 2018 | 42.1 |
| 2017 | 37.4 |
| 2016 | 32.55 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
