---
title: "Lxi Reit (LXI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Lxi Reit</td></tr>
    <tr><td>Symbol</td><td>LXI</td></tr>
    <tr><td>Web</td><td><a href="https://www.lxireit.com">www.lxireit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 5.69 |
| 2019 | 6.13 |
| 2018 | 4.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
