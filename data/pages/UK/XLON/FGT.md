---
title: "Finsbury Growth (FGT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Finsbury Growth</td></tr>
    <tr><td>Symbol</td><td>FGT</td></tr>
    <tr><td>Web</td><td><a href="https://www.finsburygt.com">www.finsburygt.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 16.6 |
| 2019 | 16.6 |
| 2018 | 15.3 |
| 2017 | 14.2 |
| 2016 | 13.1 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
