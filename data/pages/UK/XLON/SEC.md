---
title: "Strategic Eqty (SEC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Strategic Eqty</td></tr>
    <tr><td>Symbol</td><td>SEC</td></tr>
    <tr><td>Web</td><td><a href="https://www.strategicequitycapital.com">www.strategicequitycapital.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.5 |
| 2019 | 1.0 |
| 2018 | 1.0 |
| 2017 | 0.78 |
| 2016 | 0.78 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
