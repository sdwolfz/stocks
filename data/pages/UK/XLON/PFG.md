---
title: "Provident Financial (PFG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Provident Financial</td></tr>
    <tr><td>Symbol</td><td>PFG</td></tr>
    <tr><td>Web</td><td><a href="https://www.providentfinancial.com">www.providentfinancial.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 25.0 |
| 2018 | 10.0 |
| 2017 | 87.93 |
| 2016 | 134.6 |
| 2015 | 120.1 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
