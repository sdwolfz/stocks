---
title: "Hays (HAS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Hays</td></tr>
    <tr><td>Symbol</td><td>HAS</td></tr>
    <tr><td>Web</td><td><a href="https://www.haysplc.com">www.haysplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 9.4 |
| 2018 | 8.81 |
| 2017 | 7.47 |
| 2016 | 2.9 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
