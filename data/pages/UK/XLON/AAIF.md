---
title: "Abdn.asn Inc (AAIF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Abdn.asn Inc</td></tr>
    <tr><td>Symbol</td><td>AAIF</td></tr>
    <tr><td>Web</td><td><a href="https://www.asian-income.co.uk">www.asian-income.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 9.25 |
| 2018 | 9.15 |
| 2017 | 9.0 |
| 2016 | 8.75 |
| 2015 | 8.5 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
