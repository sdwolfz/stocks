---
title: "Hyve Grp. (HYVE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Hyve Grp.</td></tr>
    <tr><td>Symbol</td><td>HYVE</td></tr>
    <tr><td>Web</td><td><a href="https://www.hyve.group">www.hyve.group</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 6.86 |
| 2019 | 19.0 |
| 2018 | 40.0 |
| 2017 | 40.0 |
| 2016 | 64.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
