---
title: "Taylor Wimpey (TW.)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Taylor Wimpey</td></tr>
    <tr><td>Symbol</td><td>TW.</td></tr>
    <tr><td>Web</td><td><a href="https://www.taylorwimpey.com">www.taylorwimpey.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 18.34 |
| 2018 | 16.64 |
| 2017 | 4.74 |
| 2016 | 2.82 |
| 2015 | 1.67 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
