---
title: "Impax Asset Management (IEM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Impax Asset Management</td></tr>
    <tr><td>Symbol</td><td>IEM</td></tr>
    <tr><td>Web</td><td><a href="https://www.impaxam.com">www.impaxam.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 3.0 |
| 2018 | 3.0 |
| 2017 | 2.5 |
| 2016 | 1.95 |
| 2015 | 1.45 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
