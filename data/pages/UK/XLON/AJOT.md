---
title: "Avi Japan Oppo. (AJOT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Avi Japan Oppo.</td></tr>
    <tr><td>Symbol</td><td>AJOT</td></tr>
    <tr><td>Web</td><td><a href="https://www.ajot.co.uk">www.ajot.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
