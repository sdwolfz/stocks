---
title: "Picton Prop (PCTN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Picton Prop</td></tr>
    <tr><td>Symbol</td><td>PCTN</td></tr>
    <tr><td>Web</td><td><a href="https://www.picton.co.uk">www.picton.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 3.5 |
| 2019 | 3.5 |
| 2018 | 3.4 |
| 2017 | 3.3 |
| 2016 | 3.3 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
