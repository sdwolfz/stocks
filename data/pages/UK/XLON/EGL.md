---
title: "Ecofin Global (EGL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Ecofin Global</td></tr>
    <tr><td>Symbol</td><td>EGL</td></tr>
    <tr><td>Web</td><td><a href="https://www.ecofininvest.com">www.ecofininvest.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 6.55 |
| 2019 | 6.4 |
| 2018 | 6.4 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
