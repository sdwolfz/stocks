---
title: "Artemis Alpha (ATS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Artemis Alpha</td></tr>
    <tr><td>Symbol</td><td>ATS</td></tr>
    <tr><td>Web</td><td><a href="https://www.artemisalphatrust.co.uk">www.artemisalphatrust.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 5.2 |
| 2019 | 5.5 |
| 2018 | 6.35 |
| 2017 | 6.3 |
| 2016 | 3.9 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
