---
title: "Games Workshop (GAW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Games Workshop</td></tr>
    <tr><td>Symbol</td><td>GAW</td></tr>
    <tr><td>Web</td><td><a href="https://www.games-workshop.com">www.games-workshop.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 145.0 |
| 2019 | 155.0 |
| 2018 | 126.0 |
| 2017 | 74.0 |
| 2016 | 40.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
