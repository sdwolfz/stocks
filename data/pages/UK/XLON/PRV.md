---
title: "Porvair (PRV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Porvair</td></tr>
    <tr><td>Symbol</td><td>PRV</td></tr>
    <tr><td>Web</td><td><a href="https://www.porvair.com">www.porvair.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 5.0 |
| 2019 | 4.9 |
| 2018 | 4.6 |
| 2017 | 4.2 |
| 2016 | 3.8 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
