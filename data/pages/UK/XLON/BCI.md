---
title: "BMO Capital & Income Investment Trust (BCI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>BMO Capital & Income Investment Trust</td></tr>
    <tr><td>Symbol</td><td>BCI</td></tr>
    <tr><td>Web</td><td><a href="https://www.bmocapitalandincome.com">www.bmocapitalandincome.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 11.5 |
| 2019 | 11.4 |
| 2018 | 10.95 |
| 2017 | 10.65 |
| 2016 | 10.3 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
