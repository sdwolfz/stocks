---
title: "Residential Sec (RESI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Residential Sec</td></tr>
    <tr><td>Symbol</td><td>RESI</td></tr>
    <tr><td>Web</td><td><a href="https://www.resi-reit.com">www.resi-reit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 5.0 |
| 2019 | 5.0 |
| 2018 | 3.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
