---
title: "Jlen Env (JLEN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Jlen Env</td></tr>
    <tr><td>Symbol</td><td>JLEN</td></tr>
    <tr><td>Web</td><td><a href="https://www.jlen.com">www.jlen.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 6.6 |
| 2019 | 6.51 |
| 2018 | 6.31 |
| 2017 | 6.14 |
| 2016 | 6.05 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
