---
title: "U And I Group (UAI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>U And I Group</td></tr>
    <tr><td>Symbol</td><td>UAI</td></tr>
    <tr><td>Web</td><td><a href="https://www.uandiplc.com">www.uandiplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 10.0 |
| 2019 | 10.0 |
| 2018 | 17.9 |
| 2017 | 8.7 |
| 2016 | 13.9 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
