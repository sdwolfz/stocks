---
title: "Int.biotech. (IBT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Int.biotech.</td></tr>
    <tr><td>Symbol</td><td>IBT</td></tr>
    <tr><td>Web</td><td><a href="https://www.internationalbiotrust.com">www.internationalbiotrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 24.8 |
| 2019 | 28.0 |
| 2018 | 27.0 |
| 2017 | 23.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
