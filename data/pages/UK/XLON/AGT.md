---
title: "Avi Global Tst (AGT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Avi Global Tst</td></tr>
    <tr><td>Symbol</td><td>AGT</td></tr>
    <tr><td>Web</td><td><a href="https://www.aviglobal.co.uk">www.aviglobal.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 16.5 |
| 2019 | 16.5 |
| 2018 | 13.0 |
| 2017 | 12.0 |
| 2016 | 14.5 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
