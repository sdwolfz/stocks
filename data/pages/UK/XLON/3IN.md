---
title: "3i Infrastructure (3IN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>3i Infrastructure</td></tr>
    <tr><td>Symbol</td><td>3IN</td></tr>
    <tr><td>Web</td><td><a href="https://www.3i-infrastructure.com">www.3i-infrastructure.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 9.2 |
| 2019 | 8.65 |
| 2018 | 7.85 |
| 2017 | 7.55 |
| 2016 | 7.25 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
