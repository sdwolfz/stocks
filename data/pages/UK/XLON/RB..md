---
title: "RECKITT BENCKISER GROUP PLC ORD 10P (RB.)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/lse/">LSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/lse/">XLON</a></td></tr>
    <tr><td>Name</td><td>RECKITT BENCKISER GROUP PLC ORD 10P</td></tr>
    <tr><td>Symbol</td><td>RB.</td></tr>
    <tr><td>Web</td><td><a href="http://www.reckittbenckiser.com">www.reckittbenckiser.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 174.6 |
| 2018 | 170.7 |
| 2017 | 164.3 |
| 2016 | 153.2 |
| 2015 | 139.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
