---
title: "Anglo-Eastern Plantations (AEP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Anglo-Eastern Plantations</td></tr>
    <tr><td>Symbol</td><td>AEP</td></tr>
    <tr><td>Web</td><td><a href="https://www.angloeastern.co.uk">www.angloeastern.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.5 |
| 2018 | 3.0 |
| 2017 | 4.0 |
| 2016 | 3.8 |
| 2015 | 2.5 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
