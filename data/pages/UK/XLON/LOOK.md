---
title: "LOOKERS PLC ORD  5P (LOOK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/lse/">LSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/lse/">XLON</a></td></tr>
    <tr><td>Name</td><td>LOOKERS PLC ORD  5P</td></tr>
    <tr><td>Symbol</td><td>LOOK</td></tr>
    <tr><td>Web</td><td><a href="http://www.lookersplc.com">www.lookersplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 4.08 |
| 2017 | 3.89 |
| 2016 | 3.64 |
| 2015 | 3.12 |
| 2014 | 2.84 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
