---
title: "Standard Chartered (STAN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Standard Chartered</td></tr>
    <tr><td>Symbol</td><td>STAN</td></tr>
    <tr><td>Web</td><td><a href="https://www.standardchartered.com">www.standardchartered.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 9.0 |
| 2019 | 22.0 |
| 2018 | 21.0 |
| 2017 | 11.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
