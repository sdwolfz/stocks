---
title: "Electrocomponents (ECM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Electrocomponents</td></tr>
    <tr><td>Symbol</td><td>ECM</td></tr>
    <tr><td>Web</td><td><a href="https://www.electrocomponents.com">www.electrocomponents.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 5.9 |
| 2019 | 14.8 |
| 2018 | 13.25 |
| 2017 | 12.3 |
| 2016 | 11.75 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
