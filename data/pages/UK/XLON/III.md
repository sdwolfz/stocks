---
title: "3i Group (III)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>3i Group</td></tr>
    <tr><td>Symbol</td><td>III</td></tr>
    <tr><td>Web</td><td><a href="https://www.3i.com">www.3i.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 37.5 |
| 2019 | 37.0 |
| 2018 | 26.5 |
| 2017 | 26.5 |
| 2016 | 22.0 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
