---
title: "Barr (A.G.) (BAG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Barr (A.G.)</td></tr>
    <tr><td>Symbol</td><td>BAG</td></tr>
    <tr><td>Web</td><td><a href="https://www.agbarr.co.uk">www.agbarr.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 16.64 |
| 2018 | 15.55 |
| 2017 | 14.4 |
| 2016 | 13.33 |
| 2015 | 12.12 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
