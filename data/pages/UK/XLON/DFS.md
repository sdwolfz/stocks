---
title: "Dfs Furn (DFS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Dfs Furn</td></tr>
    <tr><td>Symbol</td><td>DFS</td></tr>
    <tr><td>Web</td><td><a href="https://www.dfscorporate.co.uk">www.dfscorporate.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 11.2 |
| 2018 | 11.2 |

### Reports

Not avaiable yet! Click [here](/money/) to find out what YOU can do about it!

## Valuation

**Coming soon...**
