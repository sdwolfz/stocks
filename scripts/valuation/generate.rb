# frozen_string_literal: true

require 'fileutils'

file = File.join('scripts', 'valuation', 'templates', 'report.yml')
raw  = File.read(file)

years     = ['2016', '2017', '2018', '2019', '2020', '2021']
ymls      = ['Q1.yml', 'H2.yml', 'T3.yml', 'A4.yml']
exchanges = ['RO']
symbols   = ARGV

raise 'Need to provide an argument' if symbols.size.zero?

exchanges.each do |exchange|
  pp exchange

  years.each do |year|
    pp year

    symbols.each do |symbol|
      pp symbol

      ymls.each do |yml|
        path = File.join('./data/valuation', exchange, symbol, year, yml)

        unless File.exist?(path)
          FileUtils.mkdir_p(File.dirname(path))
          File.write(path, raw)
        end
      end
    end
  end
end
