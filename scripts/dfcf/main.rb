# frozen_string_literal: true

require 'erb'
require 'json'
require 'yaml'

require 'pry'

require './lib/data_joiner'

# ------------------------------------------------------------------------------
# Args

args = { symbol: nil, geo: nil }
ARGV.each do |arg|
  if (match = /^SYMBOL=(.*?)$/.match(arg))
    if match[1] != ''
      args[:symbol] = match[1].split(',')
    else
      args[:symbol] = []
    end
  end

  if (match = /^GEO=(.*?)$/.match(arg))
    if match[1] != ''
      args[:geo] = match[1].split(',')
    else
      args[:geo] = []
    end
  end
end

class Regression
  def linear(pairs)
    n = pairs.size

    sum_x = pairs.map { |x, _| x }.sum.to_f
    sum_y = pairs.map { |_, y| y }.sum.to_f

    sum_xx = pairs.map { |x, _| x * x }.sum.to_f
    sum_xy = pairs.map { |x, y| x * y }.sum.to_f

    base = (n * sum_xx - sum_x**2)
    a    = (n * sum_xy - sum_x * sum_y ) / base
    b    = (sum_y * sum_xx - sum_x * sum_xy) / base

    [a, b]
  end

  def predict_next(count, values)
    pairs = []
    values.each_with_index { |e, i| pairs << [i, e] }

    a, b = linear(pairs)
    n    = values.size

    result = []
    count.times { |i| result << a * (i + n) + b }
    result
  end
end

class DFCF
  LOGGING = false
  CONFIG  = {
    years:      7,
    discount:   0.12,
    perpetuity: 0.03,
    safety:     0.25
  }.freeze

  def call(geos, symbols)
    geos.each do |geo|
      puts "############################"
      puts "#{geo}"
      puts "############################"

      artifacts = [
        ['price', "#{geo}.json"],
      ]
      registry = DataJoiner.new.call(artifacts)

      symbols.each do |symbol|
        result = extract_fcf(geo, symbol, registry)

        fair    = result[:fair]
        current = result[:current]
        delta   = ((current - fair).to_f / fair * 100).round(2)

        delta = if delta < 0
                  "#{-delta}% DISCOUNT! <--------------------------------------"
                else
                  "#{delta}% premium"
                end

        puts "#{symbol}:\t#{fair}\tMarket:\t#{current}\t#{delta}"
      end
    end
  end

  def extract_fcf(geo, symbol, registry)
    fcf    = fcf_registry(geo, symbol)
    shares = shares_registry(geo, symbol, registry)
    price  = price_registry(geo, symbol, registry)

    fair_value = run_dfcf(fcf, shares)

    { fair: fair_value, current: price }
  end

  def fcf_registry(geo, symbol)
    files = Dir.glob("./data/valuation/#{geo}/#{symbol}/*/A4.yml")

    registry = {}
    files.each do |file|
      year    = file.split('/')[-2].to_i
      raw     = File.read(file)
      content = YAML.safe_load(raw)

      registry[year] = { file: file, content: content }
    end

    registry \
      .to_a \
      .sort_by { |e| e[0] } \
      .map { |e| e[1][:content]['cashflow']['fcf'] }
  end

  def shares_registry(geo, symbol, registry)
    registry[symbol]['shares']
  end

  def price_registry(geo, symbol, registry)
    registry[symbol]['price']
  end

  def run_dfcf(fcf, shares)
    @fcf = fcf

    @years      = CONFIG[:years]
    @discount   = CONFIG[:discount]
    @perpetuity = CONFIG[:perpetuity]
    @safety     = CONFIG[:safety]

    @shares = shares

    projected  = Regression.new.predict_next(@years, @fcf)
    discounted = discounted_fcf(projected, @discount)

    perpetuity_year = @fcf.size + @years + 1
    perpetuity = perpetuity_value(projected.last, @discount, @perpetuity, perpetuity_year)
    discounted << perpetuity

    if LOGGING
      pp fcf
      pp discounted
    end

    valuation = discounted.sum / @shares / (1 + @safety)
    valuation.round(2)
  end

  def discounted_fcf(projected, discount)
    result = []

    projected.each_with_index do |value, index|
      discounted = value / (1 + discount)**(index + 1)

      result << discounted
    end

    result
  end

  def perpetuity_value(value, discount, perpetuity, year)
    ((value * (1 + perpetuity)) / (discount - perpetuity)) / (1 + discount)**year
  end
end

DFCF.new.call(args[:geo], args[:symbol])
