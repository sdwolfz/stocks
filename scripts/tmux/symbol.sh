#!/usr/bin/env sh

# 3 panes
tmux split-window -h

tmux select-pane -t 0
tmux split-window -v

tmux select-pane -t 2
tmux split-window -v

# Symbol
tmux send-keys -t 1 'time make shell/run ONLY=symbol GEO=RO' Enter
tmux send-keys -t 2 'time make shell/run ONLY=symbol GEO=UK' Enter
tmux send-keys -t 3 'time make shell/run ONLY=symbol GEO=US' Enter

# Main
tmux select-pane -t 0
tmux send-keys -t 0 'reset' Enter
