# frozen_string_literal: true

require 'fileutils'
require 'pry'
require './lib/data_joiner'

geo = ARGV[0]

artifacts = [
  ['symbol', "#{geo}.json"],
]
registry = DataJoiner.new.call(artifacts)

relevant = []
registry.each_pair do |_, payload|
  symbol = payload['symbol']

  relevant << symbol
end

existing = Dir.glob("./data/registry/#{geo}/*")

discovered = existing.map { |path| path.split('/')[-1] }

junk = discovered - relevant
pp junk.size

junk.each do |symbol|
  path = File.join("./data/registry/#{geo}/#{symbol}")

  if File.exist?(path)
    puts path

    FileUtils.rm_rf(path)
  end
end
